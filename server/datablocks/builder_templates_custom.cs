/*
ovopen-server
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

if (!isObject(CustomTemplateSet))
{
	new SimSet(CustomTemplateSet);
}

function clearCustomTemplates()
{
	%count = CustomTemplateSet.getCount();
	for (%i=0; %i<%count; %i++)
	{
		%obj = CustomTemplateSet.getObject(0);
		%obj.delete();
	}
}

function setupCustomGroups()
{
	// Define groups here. Check out names of lists in builder_templates.cs (i.e. tpl_* SimGroups)
	%groupAdd = new SimGroup() {
		displayName = "custom group shapes";

		new ScriptObject() {
			displayName = "king_frosty";
			func = "createTSStatic";
			class = "BuilderTemplate";
			shapeFile = "~/data/live_assets/shapes/dynamic/king_frosty.dts";
		};
	};
	tpl_scene_objects.add(%groupAdd); // Add to scene objects list
}

clearCustomTemplates();
setupCustomGroups();

function setupToolTargetBuilderGroup()
{
  %count = DataBlockGroup.getCount();
  for (%i=0; %i<%count; %i++)
  {
    %obj = DataBlockGroup.getObject(%i);
    if (%obj.className $= ToolTarget)
    {
      %so = new ScriptObject() {
        displayName = %obj.displayName $= "" ? %obj.getDataBlock() : %obj.displayName;
        func = "createInteractiveObject";
        class = "BuilderTemplate";
        datablock = %obj;
      };
      tpl_tool_targets.add(%so);
    }
  }
}

