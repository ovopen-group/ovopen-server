/*
ovopen-server
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

function setDefaults()
{
	// List of master servers to query, each one is tried in order
	// until one responds
	$Pref::Server::IsBuilder = 1;
	$Pref::Server::LightingId = 51;

	// Information about the server
	$Pref::Server::Name = "ovopen Test";
	$Pref::Server::Info = "This is an ovopen test server.";

	// The connection error message is transmitted to the client immediatly
	// on connection, if any further error occures during the connection
	// process, such as network traffic mismatch, or missing files, this error
	// message is display. This message should be replaced with information
	// usefull to the client, such as the url or ftp address of where the
	// latest version of the game can be obtained.
	$Pref::Server::ConnectionError =
	   "You do not have the correct version of the FPS starter kit or "@
	   "the related art needed to play on this server, please contact "@
	   "the server operator for more information.";

	// The network port is also defined by the client, this value 
	// overrides pref::net::port for dedicated servers
	$Pref::Server::Port = 28000;

	// Misc server settings.
	$Pref::Server::MaxPlayers = 64;
	$Pref::Server::TimeLimit = 20;               // In minutes
	$Pref::Server::KickBanTime = 300;            // specified in seconds
	$Pref::Server::BanTime = 1800;               // specified in seconds
	$Pref::Server::FloodProtectionEnabled = 1;
}
