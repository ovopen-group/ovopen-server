/*
ovopen-server
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
// Builder specific commands

/*
    %this.addObject(%folder, "Water Block", 0, 10000);
    %this.addObject(%folder, "Sun", 0, 10001);
    %this.addObject(%folder, "Foliage Replicator", 0, 10002);
    %this.addObject(%folder, "Shape Replicator", 0, 10003);
    %this.addObject(%folder, "Audio Emitter", 0, 10005);
    %this.addObject(%folder, "Sun Light FX", 0, 10006);
    %this.addObject(%folder, "Precipitation", 0, 10011);
    %this.addObject(%folder, "Lightning", 0, 10012);
    %folder = %this.addRootType(0, "Onverse Objects");
    %this.removeAllChildren(%folder);
    %this.addObject(%folder, "PoiPoint", 0, 11005);
    %this.addObject(%folder, "HomeArea", 0, 11006);
    %this.addObject(%folder, "HomePoint", 0, 11007);
    %this.addObject(%folder, "StoreArea", 0, 11008);
    %this.addObject(%folder, "Trigger", 0, 11009);
    %this.addObject(%folder, "GameArea", 0, 11010);
    %this.addObject(%folder, "PlayerBlocker", 0, 11011);
    %this.addObject(%folder, "VehicleBlocker", 0, 11012);
    %this.addObject(%folder, "CameraBlocker", 0, 11013);
    %this.addObject(%folder, "GameScriptObject", 0, 11014);
*/

function serverCmdGetGhostObj(%client, %gid)
{
	echo("SERVER: obj id = " @ %client.resolveObjectFromGhostIndex(%gid));
}

function serverCmdSaveBuilder(%client) 
{
		if (!$Server::Builder)
		{
			error("Client " @ %client.getId() @ "tried saving in non-builder mode!");
			return;
		}

		%count = ClientGroup.getCount();

		echo("Terrain: " @ (LocationTerrain.commitsPending() ? "no commits" : "commits") @ "pending");
		// refresh
		// .terrainFile = ""
		// commitsPending
		// TODO: LocationTerrain save() - (string fileName) - saves the terrain block's terrain file to the specified file name.

		for(%i= 0; %i < %count; %i++)
		{
			%client = ClientGroup.getObject(%i);
			commandToClient(%client, 'StartSaveBuilder', %client.name);
		}

		for(%i= 0; %i < %count; %i++)
		{
			%client = ClientGroup.getObject(%i);
			LocationTerrain.saveForClient(%client);
		}

		// Save location group
		LocationGroup.save($Pref::Server::RootMission);
		echo("Saved location to " @ $Pref::Server::RootMission);

		for(%i= 0; %i < %count; %i++)
		{
			%client = ClientGroup.getObject(%i);
			commandToClient(%client, 'EndSaveBuilder', %client.name);
		}
}

function serverCmdDeleteObject(%client, %ghostId) 
{
	if (!%client.isAdmin)
		return;

	%obj = %client.resolveObjectFromGhostIndex(%ghostId);
	if (%obj != 0)
	{
		%obj.delete();
	}
}

function serverCmdResolveObjectByName(%client, %ghost, %datablock)
{
	echo("ResolvedObjectByName");
	if (!%client.isAdmin)
		return;

	echo("ResolvedObjectByName checking..." SPC %ghost SPC %datablock);

	if (%ghost != 0)
	{
		%gid = %client.resolveObjectFromGhostIndex(%ghost);
		echo(%ghost  @ " -> " @ %gid);
		commandToClient('ResolvedObjectByName', 0, %gid);
	}
	else if (%datablock !$= "")
	{
		commandToClient('ResolvedObjectByName', nameToId(%datablock));
	}
}

function serverCmdDuplicateObject(%client, %ghostId) 
{
	if (!%client.isAdmin)
		return;

	%obj = %client.resolveObjectFromGhostIndex(%ghostId);
	if (%obj != 0)
	{
		%newObj = %obj.duplicate();
		if (%newObj != 0)
		{
			%gid = %client.getGhostID(%newObj);
			if (%gid != 0)
			{
				commandToClient(%client, 'ReturnGhostId', %gid);
			}
		}
	}
}

function serverCmdRefreshObject(%client, %ghostId) 
{
	%obj = %client.resolveObjectFromGhostIndex(%ghostId);
	if (%obj != 0)
	{
		//echo("TODO: refreshObject" SPC %obj.getId());
		%obj.refresh();
	}
}

function serverCmdGetCatChildren(%client, %typeName, %folderID, %catID) 
{
	%category_name = $objects::reverseName[%typeName];

	echo("catID=" @ %catID);
	%type = getSubStr(%catID, 0, 1);
	%tpl_group = (%type $= "g") ? getSubStr(%catID, 1, strlen(%catID)).getId() : ("tpl_" @ %category_name).getId();
	echo("group=" @ %tpl_group);

	if (!isObject(%tpl_group))
	{
		return commandToClient(%client, 'ReturnCatChildren', 0, %folderID, "", "");
	}

	echo("Getting cat " @ %category_name);
	echo("typeName=" @ %typeName SPC "folderID=" @ %folderID SPC "catID=" @ %catID);

	%count = %tpl_group.getCount();
	%end = %count-1;
	%buildFolders = "";
	%buildObjects = "";
	for (%i=0; %i<=%count; %i++)
	{
		echo(%i);
		if (%i == %end)
		{
			%nl = "";
		}
		if (%i != %count)
		{
			%obj = %tpl_group.getObject(%i);
			if (%obj.getClassName() $= "SimGroup")
			{
				%buildFolders = %buildFolders @ "g" @ %obj.getId() TAB %obj.displayName @ "\n";
			}
			else
			{
				%buildObjects = %buildObjects @ "t" @ %obj.getId() TAB %obj.displayName @ "\n";
			}
		}

		if (%i % 4 == 3 || %i == %count)
		{

			%buildFolders = rtrim(%buildFolders);
			%buildObjects = rtrim(%buildObjects);

			echo("|" @ %buildObjects @ "|");
		  commandToClient(%client, %i > 3 ? 'ReturnAppendCatChildren' : 'ReturnCatChildren', 0, %folderID, %buildFolders, %buildObjects);
		  %buildFolders = "";
		  %buildObjects = "";
		}
	}
}

function BuilderTemplate::delayedGhostNotify(%this, %client, %newObj, %tryCount)
{
	%gid = %client.getGhostID(%newObj);
	if (%gid == -1)
	{
		if (%tryCount == 3)
		{
			echo("Aborted finding ghost for oid " @ %newObj);
			return;
		}

		%newObj.setTransform(getWords(%client.player.getTransform(), 0, 2));
		%tryCount += 1;
		%this.schedule(250, delayedGhostNotify, %client, %newObj, %tryCount);
		return;
	}
	echo("delayedGhostNotify to" SPC %client SPC "for " @ %gid SPC "oid=" @ %oid);
	commandToClient(%client, 'ReturnGhostId', %gid);
	%oid.clearScopeToClient(%client);
}

function serverCmdCreateObject(%client, %type, %oid) 
{
	if (!%client.isAdmin)
		return;

	echo("TODO: CreateObject" SPC %type SPC %oid);
	%type = getSubStr(%oid, 0, 1);
	if (%type $= "t")
	{
		%oid = getSubStr(%oid, 1, strlen(%oid));

		if (isObject(%oid))
		{
			if (%oid.class $= "BuilderTemplate")
			{
				echo("TODO: create from this tpl");
				%oid.dump();
				%newObj = %oid.create(%client.getControlObject().getTransform());
				LocationGroup.add(%newObj);
				%newObj.scopeToClient(%client);
				echo("Got " SPC %newObj SPC "returned");

				if (%newObj != 0)
				{
					%oid.schedule(250, delayedGhostNotify, %client, %newObj, 0);
				}


			}
		}
	}
	else
	{
		echo("TODO: create basic object " @ %oid @ " of type " @ %type);

		%isPOI = false;
		%isStoreArea = false;

		switch(%oid)
		{
			case 10000: // WaterBlock
			%newObj = new ClientEditWaterBlock() {};

			case 10001: // Sun
			%newObj = new ClientEditSun() {};

			case 10002: // FoliageReplicator
			%newObj = new ClientEditFoliageReplicator() {};

			case 10003: // ShapeReplicator
			%newObj = new ClientEditShapeReplicator() {};

			case 10005: // AudioEmitter
			%newObj = new ClientEditAudioEmitter() {};

			case 10006: // FXSun
			%newObj = new ClientEditSunLight() {};

			case 10011: // Precipitation
			%newObj = new ClientEditPrecipitation() { datablock = DB_25; };

			case 10012: // Lightning
			%newObj = new ClientEditLightning() { datablock = DB_36; };

			case 11005: // PoiPoint
			%newObj = new ClientEditPoiPoint() {};
			%isPOI = true;

			case 11006: // HomeArea
			%newObj = new ClientEditHomeArea() {};

			case 11007: // HomePoint
			%newObj = new ClientEditHomePoint() {
				datablock = DB_7042;
			};

			case 11008: // StoreArea
			%newObj = new ClientEditStoreArea() {};

			if (!isObject(StoreAreasGroup))
			{
				new SimGroup(StoreAreasGroup);
				LocationGroup.add(StoreAreasGroup);
			}

			%isStoreArea = true;

			case 11009: // Trigger
			%newObj = new ClientEditTrigger() { 
				datablock = DefaultTrigger; 
				polyhedron = "-0.5 0.5 -0.5 1.0 0.0 0.0 0.0 -1.0 0.0 0.0 0.0 1.0";
			};

			case 11010: // GameArea
			%newObj = new ClientEditGameArea() {
				datablock = DefaultTrigger; 
				polyhedron = "-0.5 0.5 -0.5 1.0 0.0 0.0 0.0 -1.0 0.0 0.0 0.0 1.0";
			};

			case 11011: // PlayerBlocker
			%newObj = new ClientEditPlayerBlocker() {};

			case 11012: // VehicleBlocker
			%newObj = new ClientEditVehicleBlocker() {};

			case 11013: // CameraBlocker
			%newObj = new ClientEditCameraBlocker() {};

			case 11014: // GameScriptObject
			%newObj = new ClientEditGameScriptObject() {};
		}


		if (%newObj != 0)
		{
				if (%newObj != 0)
				{
					%newObj.scopeToClient(%client);
					if (%isPOI)
						PoiPoints.add(%newObj);
					else if (%isStoreArea)
						StoreAreasGroup.add(%newObj);
					else
						LocationGroup.add(%newObj);
					%oid.schedule(250, delayedGhostNotify, %client, %newObj, 0);
				}
		}
	}
}


function serverCmdResolveRealDatablock(%client, %name, %id, %this) 
{
	if (!%client.isAdmin)
		return;

	// TODO: name?
	%id = nameToId(%db);
	commandToClient(%client, 'returnResolveRealDatablock', %id);
}


//TODO: serverCmdGetCatChildren 5 3 0
//TODO: serverCmdGetCatChildren 4 1 0
//TODO: serverCmdGetCatChildren 7 5 0
//TODO: serverCmdGetCatChildren 8 7 0
//TODO: serverCmdGetCatChildren 9 9 0
//TODO: serverCmdGetCatChildren 13 11 0




function BuilderTemplate::create(%this, %transform)
{
	$retobj = "";
	%this.transform = %transform $= "" ? "0 0 0" : %transform;
	%evalStr = "$retobj = " @ %this.getId() @ "." @ %this.func @ "();";
	echo(%evalStr);
	%obj = eval(%evalStr);
	return $retobj;
}

function BuilderTemplate::createInterior(%this, %transform)
{
   %obj = new ClientEditInteriorInstance() {
      position = getWords(%transform, 0, 2);
      rotation = "0 0 0";
      interiorFile = %this.interiorFile;
   };
   
   return(%obj);
}

function BuilderTemplate::createInteractiveObject(%this, %transform)
{
   %block = %this.dataBlock;
   %obj = new ClientEditInteractiveObject() {
      position = getWords(%transform, 0, 2);
      rotation = "0 0 0";
      dataBlock = %block;
   };
   %obj.setTransform(%transform);
   
   return(%obj);
}

function BuilderTemplate::createGameScript(%this, %transform)
{
   %block = %this.dataBlock;
   %obj = new ClientEditGameScriptObject() {
      position = getWords(%transform, 0, 2);
      rotation = "0 0 0";
      class = %this.klass;
   };
   %obj.setTransform(%transform);
   
   return(%obj);
}

function BuilderTemplate::createTrigger(%this, %transform)
{
	%obj = new ClientEditTrigger() { 
		datablock = DefaultTrigger; 
		polyhedron = "-0.5 0.5 -0.5 1.0 0.0 0.0 0.0 -1.0 0.0 0.0 0.0 1.0";
		position = getWords(%transform, 0, 2);
		rotation = "0 0 0";
		TargetClass = %this.klass;
	};
   %obj.setTransform(%transform);
   
   return(%obj);
}



function BuilderTemplate::createNPC(%this, %transform)
{
   %block = %this.dataBlock;
   %obj = new ClientEditAIPlayerObject() {
      position = getWords(%transform, 0, 2);
      rotation = "0 0 0";
      dataBlock = %block;
   };
   
   return(%obj);
}

function BuilderTemplate::createTravelMount(%this, %transform)
{
	echo("TODO: createTravelMount");
}

function BuilderTemplate::createTSStatic(%this, %transform)
{
   %obj = new ClientEditTSStatic() {
      position = getWords(%transform, 0, 2);
      shapeName = %this.shapeFile;
   };
   return(%obj);
}

function BuilderTemplate::createClothingDisplay(%this, %transform)
{
   %block = %this.dataBlock;
   %obj = new PurchaseClothingObject() {
      position = getWords(%transform, 0, 2);
      rotation = "0 0 0";
      toolDatablock = "";
      maleClothing = (%block.dualSex || %block.sex $= "Male") ? %block : 0;
      femaleClothing = (%block.dualSex || %block.sex $= "Female") ? %block : 0;
   };
   return(%obj);
}

function BuilderTemplate::createToolDisplay(%this, %transform)
{
   %block = %this.dataBlock;
   %obj = new PurchaseToolObject() {
      position = getWords(%transform, 0, 2);
      rotation = "0 0 0";
      toolDatablock = "";
   };
   return(%obj);
}

function BuilderTemplate::createParticle(%this, %transform)
{
   %block = %this.particleEmitterDB;
   %obj = new ClientEditParticleEmitterNode() {
      position = getWords(%transform, 0, 2);
      rotation = "0 0 0";
      dataBlock = DB_4470;
      emitter = %block;
   };
   return(%obj);
}

function BuilderTemplate::createAudioEmitter(%this, %transform)
{
   %block = %this.dataBlock;
   %obj = new ClientEditAudioEmitter() {
      position = getWords(%transform, 0, 2);
      rotation = "0 0 0";
      profile = %block;
   };
   return(%obj);
}

function BuilderTemplate::createLight(%this, %transform)
{
   %block = %this.dataBlock;
   %obj = new ClientEditLightObject() {
      position = getWords(%transform, 0, 2);
      rotation = "0 0 0";
      dataBlock = %block;
   };
   return(%obj);
}



