/*
ovopen-server
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// Connection request received. Patcher code verifies password.
function GameConnection::onConnectRequest( %client, %netAddress, %arg1, %arg2 )
{
   echo("Connect request from: " @ %netAddress SPC "uid=" @ %client.getUserAccID() SPC "username=" @ %client.getUserName());
   %client.connectName = %client.getUserName();
   if($Server::PlayerCount >= $pref::Server::MaxPlayers)
      return "CR_SERVERFULL";

   // CHR_SUBSCRIPTION

   return "";
}

$clientuid = 1;

//-----------------------------------------------------------------------------
// This script function is the first called on a client accept
//
function GameConnection::onConnect( %client, %name )
{  
   %client.isAdmin = $Pref::Server::IsBuilder || ClientGroup.getCount() == 1;
   %client.isSuperAdmin = false;
   %client.uid = $clientuid++;
   %client.setSimulatedNetParams(0,0);
   %client.setTurbo(1);
   %client.checkMaxRate();

   %client.cc = 0;
   %client.pp = 0;
   %client.ccDelta = 0;
   %client.ppDelta = 0;

   echo("DBG: connection " @ %this SPC "accID=" @ %client.getUserAccID());
   MasterNotifyUserPresence(%client.getUserAccId(), 1);
   MasterSetUserConnectionId(%client.getUserAccId(), %client);

   // info.permissions, info.username.c_str(), info.invID, info.isVIP, info.avatarSex, info.connectionID
   %userDetails = MasterGetUserLoginDetails(%client.getUserAccID());
   echo("USER DETAILS: " @ %userDetails);

   // Save client preferences on the connection object for later use.
   %client.sex = getField(%userDetails, 4) == 1 ? "Female" : "Male";
   %client.setPlayerName(%client.connectName);

   %client.setupInventory(getField(%userDetails, 2));
   MasterQueryBalance(%client.getUserAccID());
   %client.reloadInventoryLists(); // Needs to be done before stage 3 TODO: handle case with bad delay

   // 
   $instantGroup = ServerGroup;
   $instantGroup = MissionCleanup;
   echo("CADD: " @ %client @ " " @ %client.getAddress());

   // Inform the client of all the other clients
   %count = ClientGroup.getCount();
   for (%cl = 0; %cl < %count; %cl++) {
      %other = ClientGroup.getObject(%cl);
      if ((%other != %client)) {
         %recipient = ClientGroup.getObject(%cl);
         commandToClient(%recipient, 'LocalUserLogin', %client.uid, %client.name);
      }
   }

   // If the mission is running, go ahead download it to the client
   if ($missionRunning)
      %client.loadMission();
}

//-----------------------------------------------------------------------------
// A player's name could be obtained from the auth server, but for
// now we use the one passed from the client.
// %realName = getField( %authInfo, 0 );
//
function GameConnection::setPlayerName(%client,%name)
{
   %client.sendGuid = 0;

   // Minimum length requirements
   %name = stripTrailingSpaces( strToPlayerName( %name ) );
   if ( strlen( %name ) < 3 )
      %name = "Poser";

   // Make sure the alias is unique, we'll hit something eventually
   if (!isNameUnique(%name))
   {
      %isUnique = false;
      for (%suffix = 1; !%isUnique; %suffix++)  {
         %nameTry = %name @ "." @ %suffix;
         %isUnique = isNameUnique(%nameTry);
      }
      %name = %nameTry;
   }

   // Tag the name with the "smurf" color:
   %client.nameBase = %name;
   %client.name = addTaggedString("\cp\c8" @ %name @ "\co");
}

function isNameUnique(%name)
{
   %count = ClientGroup.getCount();
   for ( %i = 0; %i < %count; %i++ )
   {
      %test = ClientGroup.getObject( %i );
      %rawName = stripChars( detag( getTaggedString( %test.name ) ), "\cp\co\c6\c7\c8\c9" );
      if ( strcmp( %name, %rawName ) == 0 )
         return false;
   }
   return true;
}

//-----------------------------------------------------------------------------
// This function is called when a client drops for any reason
//
function GameConnection::onDrop(%client, %reason)
{
   echo("Notifying master ACCID " @ %client.getUserAccID() @ " disconnected");
   MasterNotifyUserPresence(%client.getUserAccID(), 0);
   %client.commitDeltaBalance();
   %client.syncInventory();
   %client.onClientLeaveGame();
   
   removeFromServerGuidList( %client.guid );


   %count = ClientGroup.getCount();
   for(%cl= 0; %cl < %count; %cl++)
   {
      %recipient = ClientGroup.getObject(%cl);
      commandToClient(%recipient, 'LocalUserLogout', %client.uid, %client.name);
   }

   removeTaggedString(%client.name);
   echo("CDROP: " @ %client @ " " @ %client.getAddress());
}


//-----------------------------------------------------------------------------

function GameConnection::startMission(%this)
{
}


function GameConnection::endMission(%this)
{
}

function GameConnection::setupUI(%this)
{
   %mode = ""; // paintball allowed
   commandToClient(%this, 'GameInventory_Added');
   commandToClient(%this, 'Inventory_Removed');
   commandToClient(%this, 'SwitchToGameUI', %mode);
}

function GameConnection::doEnterAreaTriggerEvent(%this, %arg1, %arg2)
{
   commandToClient(%this, 'onTriggerEvent', 1, %arg1, %arg2);
}

function GameConnection::doFadeInMusic(%this, %trackName)
{
   // 2, <track>
   // "menus/world-map"
   commandToClient(%this, 'onTriggerEvent', 2, %trackName);
}

function GameConnection::doFadeOutMusic(%this)
{
   commandToClient(%this, 'offTriggerEvent', 2);
}

function GameConnection::doEnterStoreArea(%this, %name, %description, %trackName)
{
   // 3
   commandToClient(%this, 'onTriggerEvent', 3, %name, %description, %trackName);
}

function GameConnection::doExitStoreArea(%this)
{
   commandToClient(%this, 'offTriggerEvent', 3);
}

function GameConnection::doShowTutorialMessage(%this, %messageFile)
{
   commandToClient(%this, 'onTriggerEvent', 4, %messageFile);
}

function GameConnection::updatePoints(%this)
{
   commandToClient(%this, 'OnUpdatePoints', %this.cc + %this.ccDelta, %this.pp + %this.ppDelta);
}

// Commits small balance changes (from tool targets, etc)
function GameConnection::commitDeltaBalance(%client)
{
   echo("commitDeltaBalance!!!!");
   if (%client.ccDelta == 0 && %client.ppDelta == 0)
      return;

   %client.performTransaction(CommitDeltaBalance, 0, %client.ccDelta, %client.ppDelta, 0);
}

function GameConnection::hasEnoughCC(%client, %points)
{
   return (%points >= (%client.cc + %client.ccDelta));
}

function GameConnection::hasEnoughPP(%client, %points)
{
   return (%points >= (%client.pp + %client.ppDelta));
}

