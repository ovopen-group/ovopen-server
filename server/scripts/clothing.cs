/*
ovopen-server
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// Clothing management
// Onverse has up to 13 mount points for clothing, the first 3 being the head. 
// The first 3 are  always present, while the others are usually optional in some form or another.
//
// Two important values, the "clothing mask" and "mesh mask" are set for each piece of clothing to determine what clothing is 
// compatible with which item. The last thing you wear will remove incompatible clothing types set by the "clothing mask", and 
// the mesh mask will hide meshes.

$clothingType[1] = "hair";
$clothingType[2] = "eyes"; 
$clothingType[3] = "lips";
$clothingType[4] = "chest";
$clothingType[5] = "hands";
$clothingType[6] = "legs";
$clothingType[7] = "feet";
$clothingType[8] = "over"; // back
$clothingType[9] = "hats";
$clothingType[10] = "face";
$clothingType[11] = "belt";
$clothingType[12] = "ear";
$clothingType[13] = "neck";

// Each avatar has a set of meshes which can be toggled off. For certain 
// clothing items, meshes need to be turned off. This is controlled by 
// the "maleClothingMask" and "femaleClothingMask" fields on each datablock.
// (e.g. bit 1 = 1skin off, bit 2 = 2skin off, etc)
//
// For ref, each skin mesh corresponds to...
// -- head -- 
// 1skin = head
// -- shirt --
// 2skin = base of neck
// 3skin = top torso
// 4skin = mid torso
// 5skin = lower torso
// -- pants --
// 6skin = crotch + ass
// -- shirt -- 
// 7skin = right top shoulder
// 8skin = left top shoulder
// -- long shirt --
// 9skin = right arm
// 10skin = left arm
// 11skin = right forearm
// 12skin = left forearm
// -- gloves --
// 13skin = right hand
// 14skin = left hand
// -- pants --
// 15skin = right thigh
// 16skin = left thigh
// 17skin = right knee
// 18skin = left knee
// 19skin = right lower leg
// 20skin = left lower leg
// -- shoes --
// 21skin = right foot
// 22skin = left foot

// exec("onverse/server/scripts/clothing.cs");
function setupClothingSystem(%force)
{
	%db = DataBlockGroup.getId();
	%size = %db.getCount();

	if (!isObject(MaleClothingList_1))
	{
		for (%i=0; %i<14; %i++)
		{
			%maleList[%i] = new SimSet("MaleClothingList_" @ %i) {};
			%femaleList[%i] = new SimSet("FemaleClothingList_" @ %i) {};
		}
	}
	else
	{
		for (%i=0; %i<14; %i++)
		{
			%maleList[%i] = ("MaleClothingList_" @ %i).getId();
			%femaleList[%i] = ("FemaleClothingList_" @ %i).getId();

			%maleList[%i].clear();
			%femaleList[%i].clear();
		}
	}

	// Generate the male and female clothing lists, adding in guesses for 
	// %maleMeshMask and %femaleMeshMask where appropriate.
	for (%i=0; %i<%size; %i++)
	{
		%obj = %db.getObject(%i);
		if (%obj.getClassName() $= "ClothingData")
		{
			// Double check sex
			if (%obj.sex $= "" && %obj.dualSex == 0)
			{
				%obj.sex = strstr(%obj.shapeFile, "female") >= 0 ? "Female" : "Male";
			}

			if (%obj.sex $= "Male" && strstr(%obj.shapeFile, "female") >= 0)
			{
				%obj.sex = "Female";
			}

			%male = %obj.dualSex || (%obj.sex $= "Male");
			%female = %obj.dualSex || (%obj.sex $= "Female");

			// Some stuff is marked as dualSex but uses female mesh
			if (%obj.dualSex)
			{
				if (strstr(%obj.shapeFile, "female") >= 0)
					%male = false;
				else if (strstr(%obj.shapeFile, "male") >= 0)
					%female = false;
			}

			//echo("Sorting clothing " @ %obj.getName() SPC %obj.sex SPC %male SPC %female SPC %obj.shapeFile);

			if (%male)
			{
				%maleList[%obj.type].add(%obj);
			}

			if (%female)
			{
				%femaleList[%obj.type].add(%obj);
			}

			// Set male mask
			if (%male && (%force || %obj.maleClothingMask == 0))
			{
				// 
				%bestGuess = 0x1;
				switch (%obj.type)
				{
					case 4: // shirt
						%bestGuess |= (0x1BC); // shoulders
						if (strstr(%obj.desc, "long")  >= 0 ||
							strstr(%obj.desc, "jacket")  >= 0)
						{
							%bestGuess |= (0x1E00); // long sleeve
						}
						if (strstr(%obj.shapeFile, "shortsleeve")  >= 0)
						{
							%bestGuess |= (0x600); // only upper arms
						}
					case 6: // pants
						if (strstr(%obj.desc, "shorts") >= 0)
						{
							%bestGuess |= (0x78040);
						}
						else
						{
							%bestGuess |= (0x1F8040);
						}
					case 7: // feet
						%bestGuess |= (0x600000);
				}
				%obj.maleClothingMask = ~%bestGuess;
			}

			// Set female mask
			if (%female && (%force || %obj.femaleClothingMask == 0))
			{
				%bestGuess = 0x1;
				switch (%obj.type)
				{
					case 4: // shirt
						%bestGuess |= (0x1BC);
						if (strstr(%obj.desc, "long")  >= 0 ||
							strstr(%obj.desc, "jacket")  >= 0)
						{
							%bestGuess |= (0x1E00); // long sleeve
						}
						if (strstr(%obj.shapeFile, "shortsleeve")  >= 0)
						{
							%bestGuess |= (0x600); // only upper arms
						}
					case 6: // pants
						if (strstr(%obj.desc, "shorts") >= 0)
						{
							%bestGuess |= (0x78040);
						}
						else if (strstr(%obj.desc, "skirt") >= 0)
						{
							%bestGuess |= (0x18040);
						}
						else
						{
							%bestGuess |= (0x1F8040);
						}
					case 7: // feet
						%bestGuess |= (0x600000);
				}
				%obj.femaleClothingMask = ~%bestGuess;
			}
		}
	}
}

// This function updates avatar mesh visibility based on the current 
// clothing items mounted.
function Player::updateClothing(%this)
{
	// Set based on masks
	%isMale = %this.sex $= "Male";
	%isFemale = %this.sex $= "Female";
	%meshMask = 0xFFFFFFFF;

	if (!(%isMale || %isFemale))
	{
		%isMale = true;
	}

	%clothingSetMask = %this.clothingSetMask;

	// Take off meshes from the mask based on clothing
	for (%i=0; %i<14; %i++)
	{
		// NOTE: we can't just use getClothingDatablock as that will
		//       crash if nothing is assigned.
		if ((%clothingSetMask & (1<<%i)) == 0)
			continue;

		%obj = %this.getClothingDatablock(%i);
		if (%obj != 0)
		{
			if (%isFemale)
			{
				%meshMask &= %obj.femaleClothingMask;
			}
			else
			{
				%meshMask &= %obj.maleClothingMask;
			}
		}
	}

	%this.setMeshMask(%meshMask);
}

function SimSet::randomSelect(%this)
{
	%idx = getRandom(0, %this.getCount()-1);
	return %this.getObject(%idx);
}

function SimSet::randomSelectUnique(%this, %num)
{
	%list = "";
	%count = %this.getCount();
	if (%num > %count)
	{
		%num = %count;
	}
	%count -= 1;
	if (%count < 0)
		return "";
	for (%i=0; %i<%num; %i++)
	{
		%idx = getRandom(0, %count);
		while(%selected[%idx])
		{
			%idx = getRandom(0, %count);
		}
		%selected[%idx] = true;
		%list = %list @ %this.getObject(%idx) @ "\n";
	}
	return %list;
}

// Mounts clothing items to slots while updating clothing mask
function Player::maskAndMountClothing(%this, %db, %idx)
{
	if (isObject(%db))
	{
		%this.clothingSetMask |= (1 << %idx);
		%this.mountClothing(%db, %idx);
	}
	else
	{
		%this.clothingSetMask &= ~(1 << %idx); 
		%this.unmountClothing(%idx);
	}
}

// Forces a sex change of the players avatar
function GameConnection::changeSex(%this, %newSex)
{
	%transform = %this.player.getTransform();
	%this.setControlObject(%this.camera);
	// NOTE: need to delay this otherwise game will crash
	%this.player.schedule(500, delete);

	if (%newSex $= "Female")
	{
		%this.sex = "Female";
	}
	else
	{
		%this.sex = "Male";
	}

	%this.createPlayer(%transform);
}

// ClientGroup.getObject(0).player.randomlyPickClothing();
// ClientGroup.getObject(0).player.updateClothing();

function Player::resetToNude(%this)
{
	%this.clothingSetMask = 0x0;
	for (%i=0; %i<13; %i++)
	{
		%this.unmountClothing(%i);
	}
}

function Player::updateFromReserved(%this, %reserved)
{
	echo("updateFromReserved");
	for (%i=0; %i<3; %i++)
	{
		%record = getRecord(%reserved, %i);
		%dbID = getField(%record, 1);
		echo("Record " SPC %record SPC "dbID" SPC %dbID);
		if (%dbID != 0) echo("DB_" @ %dbID SPC "EQ" SPC %dbID.getId());
		%this.maskAndMountClothing("DB_" @ %dbID, %i);
	}
}

function Player::updateFromEquip(%this, %equip)
{
	echo("Equip == " SPC %equip);
	for (%i=0; %i<10; %i++)
	{
		%record = getRecord(%equip, %i);
		%dbID = getField(%record, 2);
		if (%dbID != 0) echo("DB_" @ %dbID SPC "EQ" SPC %dbID.getId());
		%this.maskAndMountClothing("DB_" @ %dbID, %i+3);
	}
}

// Randomly selects a gharish outfit
function Player::randomlyPickClothing(%this)
{
	%this.resetToNude();

	%sex = %this.sex;
	%isMale = %this.sex $= "Male";
	%isFemale = %this.sex $= "Female";
	echo("Randomly selecting " @ %sex);

	// Randomly select stuff we need
	%randomHair = (%sex @ "ClothingList_1").randomSelect();
	%randomEyes = (%sex @ "ClothingList_2").randomSelect();
	%randomPants = (%sex @ "ClothingList_6").randomSelect();

	echo("Random hair = " SPC %randomHair);
	echo("Random pants = " SPC %randomPants);

	%this.maskAndMountClothing(%randomHair, 0);
	%this.maskAndMountClothing(%randomEyes, 1);
	%this.maskAndMountClothing(%randomPants, 2);
	%pos = 3;

	// Randomly select everything else

	if (%isFemale || getRandom(0,10) > 1)
	{
		%randomShirt = (%sex @ "ClothingList_4").randomSelect();
		echo("random shirt: " SPC %randomShirt);
		%this.maskAndMountClothing(%randomShirt, %pos++);
	}

	if (%isFemale || getRandom(0,10) > 5)
	{
		%randomFace = (%sex @ "ClothingList_3").randomSelect();
		%this.maskAndMountClothing(%randomFace, %pos++);
	}

	if (getRandom(0,10) > 5)
	{
		%randomHand = (%sex @ "ClothingList_5").randomSelect();
		%this.maskAndMountClothing(%randomHand, %pos++);
	}

	if (getRandom(0,10) > 8)
	{
		%randomFeet = (%sex @ "ClothingList_7").randomSelect();
		%this.maskAndMountClothing(%randomFeet, %pos++);
	}

	if (getRandom(0,10) > 8)
	{
		%randomBack = (%sex @ "ClothingList_8").randomSelect();
		%this.maskAndMountClothing(%randomBack, %pos++);
	}

	if (getRandom(0,10) > 8)
	{
		%randomHead = (%sex @ "ClothingList_9").randomSelect();
		%this.maskAndMountClothing(%randomHead, %pos++);
	}

	if (getRandom(0,10) > 8)
	{
		%randomGlasses = (%sex @ "ClothingList_10").randomSelect();
		%this.maskAndMountClothing(%randomGlasses, %pos++);
	}

	if (getRandom(0,10) > 8)
	{
		%randomHips = (%sex @ "ClothingList_11").randomSelect();
		%this.maskAndMountClothing(%randomHips, %pos++);
	}

	if (getRandom(0,10) > 8)
	{
		%randomPiercing = (%sex @ "ClothingList_12").randomSelect();
		%this.maskAndMountClothing(%randomPiercing, %pos++);
	}

	if (getRandom(0,10) > 8)
	{
		%randomNeck = (%sex @ "ClothingList_13").randomSelect();
		%this.maskAndMountClothing(%randomNeck, %pos++);
	}

	%this.updateClothing();
}

// Updates visible meshes based on mask (starts from 1, first bit ignored)
function Player::setMeshMask(%this, %mask)
{
	// Turn off clothing based on what bits aren't set
	%this.AllMeshOn();
	for (%i=1; %i<23; %i++)
	{
		if ((%mask & (1<<%i)) == 0)
		{
			%this.MeshOff(%i @ "skin");
		}
	}
	%this.meshMask = %mask;
}

function Player::setClothingMask(%this, %mask)
{
	// TODO
}

// Makes the player invisible
function Player::toggleInvisible(%this)
{
	if (!%this.invisible)
	{
		%this.updateClothing();
	}
	else
	{
		%this.AllMeshOff();
		%this.meshMask = 0;
	}
}


function serverCmdSetClothingMask(%client, %bits) 
{
	// Toggles clothing? To what?
	commandToClient(%client, "SetClothingMask not implemented");
}

function serverCmdSetMeshMask(%client, %bits) 
{
	// Toggles meshes?
	commandToClient(%client, "SetMeshMask not implemented");
}

