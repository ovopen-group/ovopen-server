/*
ovopen-server
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

function serverCmdGotoPOI(%client, %poiName) 
{
	%poi = PoiPoints.findPoi(%poiName);
	if (!isObject(%poi))
		return;

	%client.player.safeUnmount();
	%client.player.setTransform(%poi.getTransform());
}

function serverCmdToggleInvisible(%client) 
{
	if (!%client.isAdmin)
		return;
	
	%client.player.toggleInvisible();
}

function serverCmdForceMovePlayer(%client, %username, %message) 
{
	commandToClient(%client, "ForceMovePlayer not implemented");
}

function serverCmdToggleCamera(%client)
{
	if (!%client.isAdmin)
		return;

	%control = %client.getControlObject();
	if (%control == %client.player)
	{
		%control = %client.camera;
		%control.setTransform(%client.player.getTransform());
		%control.mode = toggleCameraFly;
		%client.camera.setFlyMode(false);
	}
	else
	{
		%control = %client.player;
		%control.mode = observerFly;
	}
	%client.setControlObject(%control);
}

function serverCmdDropCamAtAva(%client) 
{
	if (!%client.isAdmin)
		return;

	%client.camera.setTransform(%client.player.getEyeTransform());
	%client.camera.setVelocity("0 0 0");
	%client.setControlObject(%client.camera);
}

function serverCmdDropPetAtCam(%client) 
{
	commandToClient(%client, 'OnServerMessage', "DropPetAtCam not implemented");
}

function serverCmdDropAvaAtCam(%client) 
{
	if (!%client.isAdmin)
		return;

	%client.player.safeUnmount();
	%client.player.setTransform(%client.camera.getTransform());
	%client.player.setVelocity("0 0 0");
	%client.setControlObject(%client.player);
}

function serverCmdSetCameraMoveSpeed(%client, %speed) 
{
	if (!%client.isAdmin)
		return;
		
	%client.camera.setMovementSpeed(%speed);
}


function serverCmdMoveAvatarCustomAxis(%client, %axis, %value) 
{
	commandToClient(%client, 'OnServerMessage', "MoveAvatarCustomAxis not implemented");
}

function serverCmdAvatarAnimation(%client, %animationName, %blend) 
{
	if (%client.player.getObjectMount() != 0)
		return;

	%client.player.setActionThread(%animationName, %blend);
}

function serverCmdDismountObject(%client) 
{
	echo("serverCmdDismountObject");
	%client.player.getDataBlock().doDismount(%client.player);
}

function serverCmdToggleTravel(%client) 
{
	commandToClient(%client, 'OnServerMessage', "ToggleTravel not implemented");
}

// Mount pet to free slot on mount
function serverCmdMountPet(%client) 
{
	commandToClient(%client, 'OnServerMessage', "MountPet not implemented");
}

function serverCmdGameInventory_EquipTool(%client, %toolID) 
{
	commandToClient(%client, 'OnServerMessage', "EquipTool not implemented");
}

function serverCmdUpInter(%client, %dbID) 
{
	echo("Client interacted with datablock " @ %dbID);
	// TODO: do something?
	//%dbID.onInteract(%client);
}

function serverCmdIO_RPC_Command(%client, %datablock, %ghostID, %cmd, %arg1, %arg2, %arg3, %arg4, %arg5, %arg6, %arg7, %arg8, %arg9, %arg10) 
{
	%objID = %client.resolveObjectFromGhostIndex(%ghostID);
	echo("DO IO RPC" SPC %datablock @ "," @ %ghostID @ "," @ %cmd @ "," @ %arg1 @ "," @ %arg2 @ "," @ %arg3 @ %arg4 @ "," @ %arg5 @ "," @ %arg6 @ "," @ %arg7 @ "," @ %arg8 @ "," @ %arg9 @ "," @ %arg10);

	if (%objID != 0)
	{
		// Filter commands here in case people decide to be devious
		switch$(%cmd)
		{
			case mount:
				%datablock.doMount(%objID, %arg1, %client);
				return;
			case trick:
				%datablock.doTrick(%objID, %arg1);
				return;
			case rename:
				echo("TODO: rename pet to " @ %arg1);
				return;
			case toggle:
				%datablock.doToggle(%objID, %arg1);
				return;
			case ride:
				%datablock.doRide(%objID, %client);
				return;
			case pet:
				%datablock.doPet(%objID, %arg1);
				return;
			case pickupAllFurn:
				// TODO
				return;
			case open:
				%datablock.doOpen(%objID, %arg1);
				return;
		}
	}

	echo("TODO IO RPC" SPC %datablock @ "," @ %ghostID @ "," @ %cmd @ "," @ %arg1 @ "," @ %arg2 @ "," @ %arg3 @ %arg4 @ "," @ %arg5 @ "," @ %arg6 @ "," @ %arg7 @ "," @ %arg8 @ "," @ %arg9 @ "," @ %arg10);
}

function Player::sit(%this, %obj, %mountPoint)
{
	%this.safeMountTo(%obj, %mountPoint);
	%this.setActionThread("sit", true);
}

function serverCmdBoardTravel(%client, %ghostID, %node) 
{
	%objID = %client.resolveObjectFromGhostIndex(%ghostID);
	if (%objID != 0)
	{
		echo("Client " @ %client @ " BoardTravel " @ %objID SPC %mount);
		%client.player.safeMountTo(%objID, %node);
	}
}

function serverCmdGetCustomPurchaseArgument(%client, %ghostID, %customArgSeqID) 
{
	commandToClient(%client, 'OnServerMessage', "GetCustomPurchaseArgument not implemented");
}

function serverCmdPurchaseItem(%client, %ghostID, %type) 
{
	%objID = %client.resolveObjectFromGhostIndex(%ghostID);
	if (%objID != 0)
	{
		%objID.getDataBlock().onPurchase(%ghostID, %client, %type);
		echo("Client " @ %client @ " PurchaseItem " @ %objID SPC %type SPC %objID.getDataBlock());
	}
}

function serverCmdSellItem(%client, %ghostID, %type) 
{
	commandToClient(%client, 'OnServerMessage', "SellItem not implemented");
}

function serverCmdSellInvItem(%client, %itemID, %type, %uniqueid) 
{
	commandToClient(%client, 'OnServerMessage', "InvItem not implemented");
}


//$Permission::Mounts = 1;
//$Permission::Mounts::Public = 0;          00
//$Permission::Mounts::Friends = 1;         01
//$Permission::Mounts::Private = 2;         10
//$Permission::Mounts::Mask = 3;            11
//$Permission::Mounts::ShareFlag = 4;      100



function serverCmdEnterVehicle(%client, %ghostID, %mount) 
{
	%objID = %client.resolveObjectFromGhostIndex(%ghostID);
	if (%objID != 0)
	{
		echo("Client " @ %client @ " EnterVehicle " @ %objID SPC %mount);
		%client.player.safeMountTo(%objID, %node);
	}
}

function serverCmdSetMountPermissions(%client, %mounts) 
{
	commandToClient(%client, 'OnServerMessage', "SetMountPermissions not implemented");
}

function serverCmdperm_response(%client, %queryID, %response) 
{
	commandToClient(%client, 'OnServerMessage', "perm_response not implemented");
}

function serverCmdSimulatedNetParams(%client, %packetLoss, %latency) 
{
	if (!%client.isAdmin)
		return;

	%client.setSimulatedNetParams(0,0);//%packetLoss, %latency);
}



