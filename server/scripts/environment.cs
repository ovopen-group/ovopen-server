// core audio datablocks
datablock AudioProfile(DB_18) {
   canSaveDynamicFields = "1";
   fileName = "~/data/live_assets/sounds/ambient_nature/exterior/thunderclap.ogg";
   description = "AudioClosest3d";
   preload = "1";
};
datablock AudioProfile(DB_19) {
   canSaveDynamicFields = "1";
   fileName = "~/data/live_assets/sounds/objects/ammo_load.ogg";
   description = "AudioAbsoluteClosest3d";
   preload = "1";
};
datablock AudioProfile(DB_20) {
   canSaveDynamicFields = "1";
   fileName = "~/data/live_assets/sounds/avatar/voice/male/nice.ogg";
   description = "Audio2D";
   preload = "1";
};
datablock AudioProfile(DB_21) {
   canSaveDynamicFields = "1";
   fileName = "~/data/live_assets/sounds/avatar/voice/female/nice.ogg";
   description = "Audio2D";
   preload = "1";
};
datablock AudioProfile(DB_22) {
   canSaveDynamicFields = "1";
   fileName = "~/data/live_assets/sounds/ambient_nature/exterior/rain_light.ogg";
   description = "AudioLooping2d";
   preload = "0";
};
datablock AudioProfile(DB_23) {
   canSaveDynamicFields = "1";
   fileName = "~/data/live_assets/sounds/ambient_nature/exterior/rain_med.ogg";
   description = "AudioLooping2d";
   preload = "0";
};
datablock AudioProfile(DB_24) {
   canSaveDynamicFields = "1";
   fileName = "~/data/live_assets/sounds/ambient_nature/exterior/rain_heavy.ogg";
   description = "AudioLooping2d";
   preload = "0";
};
datablock PrecipitationData(DB_25) {
   canSaveDynamicFields = "1";
   className = "PrecipitationData";
   dropTexture = "~/data/live_assets/textures/weather/snow";
   dropSize = "0.07";
   splashSize = "0.07";
   splashMS = "50";
   useTrueBillboards = "0";
};
datablock PrecipitationData(DB_26) {
   canSaveDynamicFields = "1";
   className = "PrecipitationData";
   soundProfile = "22";
   dropTexture = "~/data/live_assets/textures/weather/rain";
   splashTexture = "~/data/live_assets/textures/weather/water_splash";
   dropSize = "0.25";
   splashSize = "0.25";
   splashMS = "500";
   useTrueBillboards = "0";
};
datablock PrecipitationData(DB_27) {
   canSaveDynamicFields = "1";
   className = "PrecipitationData";
   soundProfile = "23";
   dropTexture = "~/data/live_assets/textures/weather/rain";
   splashTexture = "~/data/live_assets/textures/weather/water_splash";
   dropSize = "0.25";
   splashSize = "0.25";
   splashMS = "500";
   useTrueBillboards = "0";
};
datablock PrecipitationData(DB_28) {
   canSaveDynamicFields = "1";
   className = "PrecipitationData";
   soundProfile = "24";
   dropTexture = "~/data/live_assets/textures/weather/rain";
   splashTexture = "~/data/live_assets/textures/weather/water_splash";
   dropSize = "0.25";
   splashSize = "0.25";
   splashMS = "500";
   useTrueBillboards = "0";
};
datablock PrecipitationData(DB_29) {
   canSaveDynamicFields = "1";
   className = "PrecipitationData";
   dropTexture = "~/data/live_assets/textures/weather/mist";
   splashTexture = "~/data/live_assets/textures/weather/water_splash";
   dropSize = "10";
   splashSize = "0.1";
   splashMS = "250";
   useTrueBillboards = "0";
};
datablock PrecipitationData(DB_30) {
   canSaveDynamicFields = "1";
   className = "PrecipitationData";
   dropTexture = "~/data/live_assets/textures/weather/shine";
   splashTexture = "~/data/live_assets/textures/weather/water_splash";
   dropSize = "20";
   splashSize = "0.1";
   splashMS = "250";
   useTrueBillboards = "0";
};
datablock AudioProfile(DB_31) {
   canSaveDynamicFields = "1";
   fileName = "~/data/live_assets/sounds/ambient_nature/exterior/lightning_zap.ogg";
   description = "AudioClosest3d";
   preload = "0";
};
datablock AudioProfile(DB_32) {
   canSaveDynamicFields = "1";
   fileName = "~/data/live_assets/sounds/ambient_nature/exterior/thunder1.ogg";
   description = "Audio2D";
   preload = "0";
};
datablock AudioProfile(DB_33) {
   canSaveDynamicFields = "1";
   fileName = "~/data/live_assets/sounds/ambient_nature/exterior/thunder2.ogg";
   description = "Audio2D";
   preload = "0";
};
datablock AudioProfile(DB_34) {
   canSaveDynamicFields = "1";
   fileName = "~/data/live_assets/sounds/ambient_nature/exterior/thunder3.ogg";
   description = "Audio2D";
   preload = "0";
};
datablock AudioProfile(DB_35) {
   canSaveDynamicFields = "1";
   fileName = "~/data/live_assets/sounds/ambient_nature/exterior/thunder4.ogg";
   description = "Audio2D";
   preload = "0";
};
datablock LightningData(DB_36) {
   canSaveDynamicFields = "1";
   className = "LightningData";
   strikeSound = "DB_31";
   dynamic = "0";
};
datablock LightningData(DB_37) {
   canSaveDynamicFields = "1";
   className = "LightningData";
   strikeSound = "DB_31";
   dynamic = "1";
};

