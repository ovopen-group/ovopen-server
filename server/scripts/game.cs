/*
ovopen-server
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

function onServerCreated()
{
   // Invoked by createServer(), when server is created and ready to go

   // Server::GameType is sent to the master server.
   // This variable should uniquely identify your game and/or mod.
   $Server::GameType = "Test App";

   // Server::MissionType sent to the master server.  Clients can
   // filter servers based on mission type.
   $Server::MissionType = "Deathmatch";

   // Load up all datablocks, objects etc.  This function is called when
   // a server is constructed.
   // exec("./foo.cs");

   // For backwards compatibility...
   createGame();
}

function onServerDestroyed()
{
   // Invoked by destroyServer(), right before the server is destroyed
   destroyGame();
}

function onMissionLoaded()
{
   // Called by loadMission() once the mission is finished loading
   startGame();
}

function onMissionEnded()
{
   // Called by endMission(), right before the mission is destroyed
   endGame();
}

function onMissionReset()
{
   // Called by resetMission(), after all the temporary mission objects
   // have been deleted.
}


//-----------------------------------------------------------------------------
// These methods are extensions to the GameConnection class. Extending
// GameConnection make is easier to deal with some of this functionality,
// but these could also be implemented as stand-alone functions.
//-----------------------------------------------------------------------------

function GameConnection::onClientEnterGame(%this)
{
   // Called for each client after it's finished downloading the
   // mission and is ready to start playing.
   // Tg: Should think about renaming this onClientEnterMission
}

function GameConnection::onClientLeaveGame(%this)
{
   // Call for each client that drops
   // Tg: Should think about renaming this onClientLeaveMission

   if (isObject(%this.spawnMount))
   {
      %this.spawnMount.delete();
      %this.spawnMount = 0;
   }
}


//-----------------------------------------------------------------------------
// Functions that implement game-play
// These are here for backwards compatibilty only, games and/or mods should
// really be overloading the server and mission functions listed ubove.
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------

function createGame()
{
   // This function is called by onServerCreated (ubove)
}

function destroyGame()
{
   // This function is called by onServerDestroyed (ubove)
}


//-----------------------------------------------------------------------------

function startGame()
{
   ResetStoreAreas();
   ReloadStoreAreas();
   RespawnStoreItems();

   if ($Game::Running) {
      error("startGame: End the game first!");
      return;
   }

   // Inform the client we're starting up
   for( %clientIndex = 0; %clientIndex < ClientGroup.getCount(); %clientIndex++ ) {
      %cl = ClientGroup.getObject( %clientIndex );
      commandToClient(%cl, 'GameStart');

      // Other client specific setup..
      %cl.score = 0;
   }

   $Game::Running = true;
}

function endGame()
{
   // Inform the client the game is over
   for( %clientIndex = 0; %clientIndex < ClientGroup.getCount(); %clientIndex++ ) {
      %cl = ClientGroup.getObject( %clientIndex );
      commandToClient(%cl, 'GameEnd');
   }

   // Delete all the temporary mission objects
   resetMission();
   $Game::Running = false;
}

//-----------------------------------------------------------------------------

function GameConnection::onClientEnterGame(%this)
{
   // Create a new camera object.
   if ($Server::Builder)
   {
      %this.camera = new ClientEditCamera() {
         dataBlock = ObserverCameraData;
      };
   }
   else
   {
      %this.camera = new Camera() {
         dataBlock = ObserverCameraData;
      };
   }

   %this.camera.setFlyMode(false);
   
   MissionCleanup.add( %this.camera );
   %this.camera.scopeToClient(%this);

   // Setup game parameters, the onConnect method currently starts
   // everyone with a 0 score.
   %this.score = 0;

   // Create a player object.
   %this.spawnPlayer();

   echo("EnterGame inventory: " SPC %this.inventory);
}

function GameConnection::onClientLeaveGame(%this)
{
   if (isObject(%this.camera))
      %this.camera.delete();
   if (isObject(%this.player))
      %this.player.delete();
}


//-----------------------------------------------------------------------------

function GameConnection::onLeaveMissionArea(%this)
{
   // The control objects invoked this method when they
   // move out of the mission area.
}

function GameConnection::onEnterMissionArea(%this)
{
   // The control objects invoked this method when they
   // move back into the mission area.
}


//-----------------------------------------------------------------------------

function GameConnection::onDeath(%this, %sourceObject, %sourceClient, %damageType, %damLoc)
{
}


//-----------------------------------------------------------------------------

function GameConnection::spawnPlayer(%this)
{
   // Combination create player and drop him somewhere
   %spawnPoint = pickSpawnPoint();
   //%spawnPoint = "161.813 -187.49001 955.25";
   %this.createPlayer(%spawnPoint);
}

//-----------------------------------------------------------------------------

function GameConnection::createPlayer(%this, %spawnPoint)
{
   echo("Creating " @ %this.sex SPC "Player");
   if (isObject(%this.player))  {
      // The client should not have a player currently
      // assigned.  Assigning a new one could result in 
      // a player ghost.
      error( "Attempting to create an angus ghost!" );
   }

   // Create the player object
   %player = new Player() {
      dataBlock = (%this.sex $= "Female" ? AvatarFemale : AvatarMale);
      client = %this;
   };
   MissionCleanup.add(%player);

   // Player setup...
   %player.setTransform(%spawnPoint);
   %player.setShapeName(%this.name);
   %player.sex = %this.sex;
   if (%player.sex $= "")
   {
      echo("Player set not set?! Defaulting to male.");
      %player.sex = "Male";
   }

   if (%this.inventory.items["Equipped", "Clothing"] $= "")
   {
      echo("We're nude! Selecting some random clothing!");
      %player.randomlyPickClothing();
   }
   else
   {
      %player.updateFromReserved(%this.inventory.items["Equipped", "Reserved"]);
      %player.updateFromEquip(%this.inventory.items["Equipped", "Clothing"]);
      %player.setHairSkin(%this.inventory.hairID, %this.inventory.skinID);
   }

   // Update the camera to start with the player
   %this.camera.setTransform(%player.getEyeTransform());

   // Give the client control of the player
   %this.player = %player;
   %this.setControlObject(%player);
}


//-----------------------------------------------------------------------------
// Support functions
//-----------------------------------------------------------------------------

function SimSet::findPoi(%this, %name)
{
   %count = %this.getCount();
   for (%i=0; %i<%count; %i++)
   {
      %obj = %this.getObject(%i);
      if (%obj.PoiURLName $= %name)
      {
         return %obj;
      }
   }
}

function pickSpawnPoint(%location) 
{
   if (%location $= "")
   {
      %location = "spawn_main";
   }

   %groupName = "LocationGroup/PoiPoints";
   %group = nameToID(%groupName);

   if (%group != -1) {
      %spawn = %group.findPoi(%location);
      if (%spawn != 0)
      {
         return %spawn.getTransform();
      }
      else if (%location !$= "spawn_main")
      {
         error("Missing PoiPoint" @ %location @ ", trying again with default");
         return pickSpawnPoint("spawn_main");
      }
   }
   else
      error("Missing " @ %groupName);

   // Could be no spawn points, in which case we'll stick the
   // player at the center of the world.
   return "0 0 300 1 0 0 0";
}
