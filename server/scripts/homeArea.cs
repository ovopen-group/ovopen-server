/*
ovopen-server
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

if (!isObject(HomeAreaSet))
{
	new SimSet(HomeAreaSet);
}

// HomeArea has:
//	objInArea
//	objFit
//	shiftObjToArea

function HomeArea::onAdd(%this)
{
	echo("Added normal home area");
	HomeAreaSet.add(%this);
}

function ClientEditHomeArea::onAdd(%this)
{
	echo("Added client home area");
	HomeAreaSet.add(%this);
}

function Player::getHomeArea(%this)
{
	%count = HomeAreaSet.getCount();
	for (%i=0; %i<%count; %i++)
	{
		%obj = HomeAreaSet.getObject(%i);
		if (%obj.objInArea(%this))
		{
			return %obj;
		}
	}
	return 0;
}

function InteractiveObjectData::ClientSetTransform(%this, %obj, %pos, %realPos)
{
	echo("Furniture moving:" SPC %this SPC %obj SPC %pos SPC %realPos);
	%obj.setTransform(%realPos);

	%obj.homeArea.shiftObjToArea(%obj);
}

function serverCmdPlaceFurnitureObject(%client, %datablock, %uniqueID) 
{
	// Determine what home area we are in...
	%area = %client.player.getHomeArea();
	if (%area == 0)
	{
		commandToClient(%client, 'FurniturePlacementError', "Not in Home Area");
		return;
	}

	//if (area.ownerClient != %client)
	//{
	//	commandToClient(%client, 'FurniturePlacementError', "Can't place furniture here");
	//}

	%inv = %client.inventory;
	%item = %client.getInventoryItem("Furniture", %uniqueID);
	if (%item $= "")
	{
		commandToClient(%client, 'FurniturePlacementError', "Item does not exist!");
	}

	%itemID = getField(%item, 0);
	%itemDB = getField(%item, 1);

	if (%datablock != %itemDB || !%datablock.isFurniture)
	{
		commandToClient(%client, 'FurniturePlacementError', "DB Mismatch!");
	}

	%obj = new InteractiveObject() {
		datablock = %datablock;
		ownerClient = %client;
		homeArea = %area;
		ownerID = %client.getUserAccID();
	};
	%obj.dump();

	%obj.setTransform(%client.player.position);
	%obj.schedule(250, delayedFurnitureGhostNotify, %client, 0);
}

function InteractiveObject::delayedFurnitureGhostNotify(%this, %client, %tryCount)
{
	%gid = %client.getGhostID(%this);
	if (%gid == -1)
	{
		if (%tryCount == 3)
		{
			echo("Aborted finding furniture ghost for oid " @ %newObj);
			return;
		}

		%this.setTransform(%client.player.position);
		%tryCount += 1;
		%this.schedule(250, delayedFurnitureGhostNotify, %client, %tryCount);
		return;
	}
	echo("delayedFurnitureGhostNotify to" SPC %client SPC "for " @ %gid SPC "oid=" @ %this);
	commandToClient(%client, 'FurniturePlacementBegin', %gid);
	//%oid.clearScopeToClient(%client);
}

function serverCmdMoveFurnitureObject(%client, %ghostID) 
{
	%oid = %client.resolveObjectFromGhostIndex(%ghostID);
	%clientUID = %client.getUserAccID();
	if (%oid != 0)
	{
		if (%oid.ownerID != %clientUID)
		{
			commandToClient(%client, 'OnServerMessage', "Can't move this furniture");
		}
		else
		{
			commandToClient(%client, 'FurnitureMovementBegin', %ghostID);
		}
	}
}

function serverCmdPickupFurnitureObject(%client, %ghostID) 
{
	%oid = %client.resolveObjectFromGhostIndex(%ghostID);
	%clientUID = %client.getUserAccID();
	echo("CLI UID = " @ %clientUID);
	if (%oid != 0)
	{
		if (%oid.ownerID == %clientUID)
		{
			%oid.schedule(1, delete);
		}
	}
}

