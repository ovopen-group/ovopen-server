// TODO: this should be customized per-location
datablock HomePointData(DB_7042)
{
   canSaveDynamicFields = "1";
   className = "HomePointData";
   shadowEnable = "0";
   shadowCanMove = "0";
   shadowCanAnimate = "1";
   shapeFile = "~/data/live_assets/shapes/dynamic/homepoints/homepoint_tuscan_wall.dts";
   emap = "0";
   planesort = "0";
   renderWhenDestroyed = "1";
   mass = "1";
   drag = "0";
   density = "1";
   maxEnergy = "0";
   maxDamage = "1";
   maxArmor = "0";
   disabledLevel = "1";
   destroyedLevel = "1";
   repairRate = "0.0033";
   inheritEnergyFromMount = "0";
   isInvincible = "0";
   cameraMaxDist = "15";
   cameraMinDist = "0";
   cameraDefaultFov = "90";
   cameraMinFov = "5";
   cameraMaxFov = "120";
   firstPersonOnly = "0";
   useEyePoint = "0";
   observeThroughObject = "0";
   aiAvoidThis = "0";
   computeCRC = "0";
   noIndividualDamage = "0";
   dynamicType = "0";
};
