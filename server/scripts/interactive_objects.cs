/*
ovopen-server
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

function GameConnection::cleanupMounts(%this)
{
   if (%this.spawnMount != 0)
   {
      if (%this.player.getControlObject() == %this.spawnMount)
      {
         if (%this.player.getMountObject() == %this.spawnMount)
         {
            %this.player.getDataBlock().doDismount(%this.player);
         }
         %this.player.clearControlObject();
      }
      %this.spawnMount.schedule(100, delete);
      %this.spawnMount = 0;
      %this.spawnMountID = 0;
   }
}

function MountablePlayerData::onEquip(%this, %client, %invID)
{
   echo("MountablePlayerDataData EQUIP");
   if (%client.waitEquip)
      return;

   if (isObject(%client.spawnMount))
   {
      echo("Putting away existing equip");
      %client.player.getDatablock().doDismount(%client.player, true);
      %client.spawnMount.schedule(100, delete);
      %client.spawnMount = 0;

      if (%client.spawnMountID == %invID)
      {
         %client.spawnMountID = 0;
         return;
      }
   }

   %player = %client.player;
   if (!isObject(%player))
      return;

   if (isObject(%client.mountedObject) && %client.mountedObject.owner != %client)
   {
      echo("Mounted to someone elses object");
      return;
   }

   echo("Spawning new playermountable equip");

   %mount = new MountablePlayer() {
      position = VectorAdd(%client.player.position, "0 0 2");
      rotation = %client.player.rotation;
      datablock = %this;
      client = %client;
      ownerID = %client.getUserAccID();
   };
   echo("MOUNT PLAYER ID=" @ %mount);

   %mount.scopeToClient(%client);
   %client.player.setControlObject(%mount);
   %client.spawnMount = %mount;
   %client.spawnMountID = %invID;

   %client.waitEquip = true;
   %client.schedule(1000, unsetWaitEquip);
   %client.add(%mount);

   if (isObject(%mount))
   {
      %client.player.safeMountTo(%mount, 0);
   }
}

function MountableVehicleData::onEquip(%this, %client, %invID)
{
   echo("MountableVehicleData EQUIP");
   if (%client.waitEquip)
      return;

   if (isObject(%client.spawnMount))
   {
      echo("Putting away existing equip");
      %client.player.getDatablock().doDismount(%client.player, true);
      echo("ControlObject now " @ %client.getControlObject());
      %client.spawnMount.schedule(100, delete);
      %client.spawnMount = 0;

      if (%client.spawnMountID == %invID)
      {
         %client.spawnMountID = 0;
         return;
      }
   }

   %player = %client.player;
   if (!isObject(%player))
      return;

   if (isObject(%client.mountedObject) && %client.mountedObject.owner != %client)
   {
      echo("Mounted to someone elses object");
      return;
   }

   echo("Spawning new player equip");

   %mount = new WheeledVehicle() {
      position = VectorAdd(%client.player.position, "0 0 2");
      rotation = %client.player.rotation;
      datablock = %this;
      client = %client;
      ownerID = %client.getUserAccID();
   };
   %mount.setRechargeRate(0.25);
   echo("MOUNT ID=" @ %mount);

   %mount.scopeToClient(%client);
   %client.player.setControlObject(%mount);
   %client.spawnMount = %mount;
   %client.spawnMountID = %invID;

   %client.waitEquip = true;
   %client.schedule(1000, unsetWaitEquip);
   %client.add(%mount);

   if (isObject(%mount))
   {
      %client.player.safeMountTo(%mount, 0);
   }
}

function AIPetData::onEquip(%this, %client, %invID)
{
   echo("AIPetData EQUIP" SPC %invID);
   if (%client.waitEquip)
      return;

   if (isObject(%client.spawnPet))
   {
      echo("Putting away existing equip");
      %client.spawnPet.schedule(100, delete);

      if (%client.spawnPetID == %invID)
      {
         %client.spawnPetID = 0;
         return;
      }

      %client.spawnPetID = 0;
   }

   %player = %client.player;
   if (!isObject(%player))
      return;

   echo("Spawning new playermountable pet");

   %colMask = $TypeMasks::WaterObjectType | 
              $TypeMasks::InteractiveObjectType | 
              $TypeMasks::StaticShapeObjectType | 
              $TypeMasks::TerrainObjectType | 
              $TypeMasks::InteriorObjectType;

   %inFront = containerRayCast(%player.position, VectorAdd(%player.position, VectorScale(%player.getForwardVector(), 2)), %colMask);

   echo("inFront == " SPC %inFront);
   if (%inFront != 0)
   {
      echo("Can't find a good place to spawn pet, aborting.");
      return;
   }

   %optimalPosition = VectorAdd(%player.position, VectorScale(%player.getForwardVector(), 2));

   %pet = new AIPet() {
      position = %optimalPosition;
      rotation = VectorSub("0 0 0", %client.player.rotation);
      datablock = %this;
      client = %client;
      ownerID = %client.getUserAccID();
   };

   %pet.scopeToClient(%client);
   %client.spawnPet = %pet;
   %client.spawnPetID = %invID;
   %client.add(%pet);

   %client.waitEquip = true;
   %client.schedule(1000, unsetWaitEquip);
}

function WheeledVehicleData::onAdd(%this,%obj)
{
   // Setup the car with some defaults tires & springs
   for (%i = %obj.getWheelCount() - 1; %i >= 0; %i--) {
      %obj.setWheelTire(%i,DB_235);
      %obj.setWheelSpring(%i,DB_236);
      %obj.setWheelPowered(%i,false);
   }
   
   // Steer front tires
   %obj.setWheelSteering(0,1);
   %obj.setWheelSteering(1,1);

   // Only power the two rear wheels...
   %obj.setWheelPowered(2,true);
   %obj.setWheelPowered(3,true);
}


function GameConnection::equipTool(%this, %itemID)
{
   %itemInfo = %this.getInventoryItem("Tools", %itemID);

   // actionList
   // mountPoint

   if (%this.player.activeToolID == %itemID)
   {
      %dbID = 0;
      %this.player.activeToolID = 0;
   }
   else
   {
      %dbID = getField(%itemInfo, 1);
      %this.player.activeToolID = %itemID;
   }

   %this.player.equipTool(%dbID);
}

/*
ShapeBaseImage classes

ShapeBaseImageData
PaintBallImage
SBF_ThrowSnowballClass
SBF_SnowBallImageClass

*/

/*
   Tools:

   Only 1 type: MeleeTool
*/
function Player::equipTool(%this, %dbID)
{
   if (%this.equippedTool != 0)
   {
      %this.unmountImage(%this.equippedTool.mountPoint);
   }

   %this.equippedTool = %dbID;
   if (%dbID != 0)
   {
      %this.mountImage(%this.equippedTool, %this.equippedTool.mountPoint);
   // nholdArmThread\tlook\nfireArmThread\tflag_wave\naction\t\nshapeBaseImage\tMeleeToolImage\nsound\t\nparticleID\t\n"
   }
}

function MeleeTool::onMount(%this,%obj,%slot)
{
   %obj.setImageAmmo(%slot, 1);
   %obj.setImageLoaded(%slot, 1);

   if (%this.holdArmThread !$= "")
   {
      %obj.setArmThread(%this.holdArmThread);
   }
   else
   {
      %obj.setArmThread("look");
   }
}

function MeleeTool::onUnmount(%this, %obj)
{
   %obj.setArmThread("look");
}

function InteractiveObject::matchesActionList(%this, %list)
{
   %db = %this.getDatablock();
   echo("DB " SPC %db);
   echo("ACTION LIST=" @ %list);

   %action[0] = %db.action[0];
   %action[1] = %db.action[1];
   %action[2] = %db.action[2];

   for (%i=0; %i<3; %i++)
   {
      %len = strlen(%action[%i]);
      if (%len == 0)
         continue;
      %actionName[%i] = getSubStr(%action[%i], 0, %len-1);
      %actionLevel[%i] = getSubStr(%action[%i], %len-1, 1);
   }

   %count = getFieldCount(%list);

   for (%i=0; %i<%count; %i++)
   {
      %value = getField(%list, %i);
      echo("FIELD VALUE FROM TOOL=" SPC %value);
      %len = strlen(%value);
      if (%len == 0)
         continue;

      %action = getSubStr(%value, 0, %len-1);
      %level = getSubStr(%value, %len-1, 1);

      for (%j=0; %j<3; %j++)
      {
         echo("MATCH? " SPC %j SPC "::" SPC %actionName[%j] SPC %actionLevel[%j]);
         echo("MATCH? VS " @ %action SPC %level);

         if ( (%action $= %actionName[%j]) && 
              (%actionLevel[%j] <= %level) )
         {
            return true;
         }
      }
   }

   echo("NO MATCH");

   return false;
}

// exec("onverse/server/scripts/inventory.cs");
function MeleeTool::onFire(%this, %obj)
{
   echo("MeleeTool fire " SPC %this SPC %obj);
   echo("FIRE THREAD" SPC %this.fireArmThread);
   %obj.setActionThread(%this.fireArmThread);


   // Cast ray in front
   %actionList = %this.actionList;
   for (%i=0; %i<%count; %i++)
   {
      %action = getField(%actionList, %i);
   }

   // Match the action list with what we got
   %startRay = VectorAdd(%obj.getPosition(), "0 0 0.5");
   %dist = 1;
   %endRay = VectorAdd(%startRay, VectorScale(%obj.getForwardVector(), %dist));
   %mask = $TypeMasks::StaticShapeObjectType | $TypeMasks::ShapeBaseObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::EnvironmentObjectType | $TypeMasks::InteractiveObjectType | $TypeMasks::DamageableItemObjectType;
   %forward = containerRayCast(%startRay, %endRay, %mask, %obj);

   if (%forward !$= "0")
   {
      %objStruck = getField(%forward, 0);
      %objPos = getWords(%forward, 1, 3);

      echo("STRUCK " @ %objStruck SPC "AT " @ %objPos);
   }
   else
   {
      echo("NOT STRUCK ANYTHING IN PARTICULAR");
      %objPos = %endRay;
   }

   //
   initContainerRadiusSearch(%objPos, %dist * 0.5, $TypeMasks::InteractiveObjectType | $TypeMasks::DamageableItemObjectType);
   echo("INIT CONTAINER SEARCH AT " @ %objPos);

   %scal = (%dist * 0.5) SPC (%dist * 0.5) SPC (%dist * 0.5);
   if (!isObject($dummyStorea))
   {
      echo("DUMMY CREATED");
      $dummyStorea = new ClientEditStoreArea() {
         position = %objPos;
         scale = %scal;
      };
   }
   else
   {
      echo("DUMMY CHANGED");
      $dummyStorea.setTransform(%objPos);
      $dummyStorea.setScale(%scal);
   }

   %toolActions = %this.actionList;
   
   while ((%next = containerSearchNext()) != 0)
   {
      echo("RADIUS SEARCH FOUND " @ %next SPC %next.getClassName() SPC %next.getDatablock().className);

      %klass = %next.getDataBlock().className;
      if (%klass $= DestructibleObject ||
          %klass $= ToolTarget)
      {
         if (!%next.matchesActionList(%toolActions))
         {
            echo("INCOMPATIBLE TOOL, SKIPPING");
            continue;
         }

         if (%next.getDamageLevel() < 1)
         {
            echo("DESTROYING OBJECT!!");
            %next.getDataBlock().kill(%next);
         }
         else
         {
            echo("OBJECT ALREADY DESTROYED!!");
         }
      }
   }


}

function SeymourTheDuck::onPurchase(%this, %object, %client, %type)
{
   if (%type == 1)
   {
      %ccChange = -100;
      %ppChange = %this.PPAmt;
      if (%client.hasEnoughCC(100))
      {
         %client.performTransaction("AdminBalanceChange", %type, %ccChange, %ppChange, %object.getId());
      }
      //function GameConnection::performTransaction(%client, %name, %itemid, %cc, %pp, %internal_id)
   }
}




