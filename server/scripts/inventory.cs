/*
ovopen-server
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

//-----------------------------------------------------------------------------
// Inventory server commands
//-----------------------------------------------------------------------------  

function GameConnection::setupInventory(%client, %lastInvID)
{
   %client.lastInvID = %lastInvID;
   if (isObject(%client.inventory))
      return;

   %user_inv = new SimObject() { };
   %client.inventory = %user_inv;
}

function INV_TranslateFromDB(%kind, %list)
{
   %count = getRecordCount(%list);
   %kind = InventoryItemMap.InvRMap[%kind];
   for (%i=0; %i<%count; %i++)
   {
      %record = getRecord(%list, %i);
      %dbID = getField(%record, 1);
      %mapObj = InventoryItemMap.map[%kind, %dbID];
      %record = setField(%record, 1, %mapObj.getId());
      %list = setRecord(%list, %i, %record);
   }
   return %list;
}

function INV_TranslateFromDB_Clothing(%kind, %list)
{
   %count = getRecordCount(%list);
   %kind = InventoryItemMap.InvRMap[%kind];
   for (%i=0; %i<%count; %i++)
   {
      %record = getRecord(%list, %i);
      if (%record $= "")
         continue;

      %ident = getField(%record, 0);
      %dbID = getField(%record, 1);
      %mapObj = InventoryItemMap.map[%kind, %dbID];
      %record = %dbID == 0 ? "0 0 0" : (%ident TAB 1 TAB %mapObj.getId());
      %list = setRecord(%list, %i, %record);
   }
   return %list;
}

function INV_TranslateFromDB_EquippedClothing(%list)
{
   %count = getRecordCount(%list);
   %kind = InventoryItemMap.InvRMap[%kind];

   %item[0] = "0\t0\t0";
   %item[1] = "0\t0\t0";
   %item[2] = "0\t0\t0";
   %item[3] = "0\t0\t0";
   %item[4] = "0\t0\t0";
   %item[5] = "0\t0\t0";
   %item[6] = "0\t0\t0";
   %item[7] = "0\t0\t0";
   %item[8] = "0\t0\t0";
   %item[9] = "0\t0\t0";

   %count = getRecordCount(%list);
   for (%i=0; %i<%count; %i++)
   {
      %record = getRecord(%list, %i);
      if (%record $= "")
         continue;

      %itemID = getField(%record, 0);
      %dbID = getField(%record, 1);
      // MAP DBID
      %mapObj = InventoryItemMap.map[Clothing, %dbID];
      %typeId = %mapObj.type-4;
      echo("Lookup clothing " @ %dbID);
      backtrace();
      %item[%typeId] = %itemID TAB 1 TAB %mapObj.getId();
   }

   return %item[0] NL %item[1] NL %item[2] NL %item[3] NL %item[4] NL %item[5] NL %item[6] NL %item[7] NL %item[8] NL %item[9];
}

function INV_TranslateFromDB_EquippedReserved(%list)
{
   %count = getRecordCount(%list);
   %kind = InventoryItemMap.InvRMap[%kind];

   %item[0] = "0\t0";
   %item[1] = "0\t0";
   %item[2] = "0\t0";

   // We only have a SET AMOUNT OF RESERVED ITEMS
   for (%i=0; %i<%count; %i++)
   {
      %record = getRecord(%list, %i);
      if (%record $= "")
         continue;

      %itemID = getField(%record, 0);
      %dbID = getField(%record, 1);
      // MAP DBID
      %mapObj = InventoryItemMap.map[Clothing, %dbID];
      %typeId = %mapObj.type;
      echo("Lookup reserved " @ %dbID @ "typeid " @ %typeID);
      backtrace();
      %item[%typeId-1] = %itemID TAB %mapObj.getId();
   }

   return %item[0] NL %item[1] NL %item[2];
}

function INV_TranslateToDB(%list)
{
   %count = getRecordCount(%list);
   for (%i=0; %i<%count; %i++)
   {
      %record = getRecord(%list, %i);
      //echo("RECORD:" @ %record);
      %ident = getField(%record, 0);
      %dbID = getField(%record, 1);
      //echo("Map is: " @ %kind SPC %dbID);
      %mapID = InventoryItemMap.map[Datablock, %dbID];
      //echo(%mapObj);
      %record = %ident TAB %mapID;
      echo("SYNC Item " @ %dbID @ "=" @ %mapID);
      %list = setRecord(%list, %i, %record);
   }
   return %list;
}

function INV_TranslateToDB_Clothing(%list)
{
   %count = getRecordCount(%list);
   %newlist = "";
   %nl_i = 0;
   for (%i=0; %i<%count; %i++)
   {
      %record = getRecord(%list, %i);
      %ident = getField(%record, 0);
      %dbID = getField(%record, 2);
      %mapID = %dbID == 0 ? 0 : InventoryItemMap.map[Datablock, %dbID];

      if (%mapID == 0)
      {
         continue;
      }

      %record = %ident TAB %mapID;
      %newlist = setRecord(%newlist, %nl_i, %record);
      %nl_i++;
   }


   echo("--INV_TranslateToDB_Clothing OUT--");
   echo(%newlist);
   echo("--");
   return %newlist;
}

// Gives us inventory equal to timbers
function GameConnection::setupTimberInventory(%this)
{
   %inv = %this.inventory;
   %inv.hairID = 1;
   %inv.skinID = 1;
   %inv.items["Pets"] = "101\t7183\n102\t7184\n";
   %inv.items["Travel"] = "168\t7324\n307\t7360\n308\t7359\n309\t7363\n310\t7361\n311\t7362\n312\t7332\n";
   %inv.items["Tools"] = "";
   %inv.items["Clothing"] = "21785\t1\t3279\n21786\t1\t3717\n21787\t1\t3666\n21788\t1\t3618\n21789\t1\t3616\n21790\t1\t3623\n21791\t1\t3624\n21792\t1\t3715\n21793\t1\t3602\n21794\t1\t3598\n21795\t1\t3604\n21796\t1\t3425\n21797\t1\t3422\n21798\t1\t3711\n21799\t1\t3609\n21800\t1\t1252\n21801\t1\t1250\n21802\t1\t3318\n21803\t1\t3688\n21804\t1\t3612\n21805\t1\t3713\n21806\t1\t3364\n21807\t1\t3607\n21808\t1\t3538\n21809\t1\t3046\n21810\t1\t2972\n21811\t1\t3697\n21812\t0\t3750\n21813\t1\t3749\n21814\t1\t3748\n21815\t1\t3790\n21816\t1\t3792\n21817\t1\t3788\n21818\t1\t3753\n21819\t1\t3765\n22764\t1\t4238\n22765\t1\t4242\n22766\t1\t4228\n22767\t1\t4235\n22768\t1\t4233\n22769\t1\t4230\n22770\t1\t4262\n22771\t1\t4226\n22772\t1\t4232\n22773\t1\t4274\n22774\t1\t4270\n22775\t1\t4252\n22776\t1\t2770\n22777\t1\t2776\n22849\t1\t4323\n22850\t1\t4315\n22851\t1\t4317\n22852\t1\t4301\n22853\t1\t4311\n22854\t1\t4326\n22855\t1\t4297\n22856\t1\t4321\n22857\t0\t4143\n22858\t1\t4154\n22859\t1\t4328\n22860\t0\t4146\n22861\t1\t4324\n22862\t1\t1742\n";
   %inv.items["Reserved"] = "251\t4339\n258\t4340\n301\t4351\n328\t4352\n333\t4353\n369\t4354\n587\t4356\n588\t4357\n589\t4358\n590\t4359\n592\t4360\n593\t4361\n779\t4363\n1119\t4367\n1374\t4368\n1375\t4369\n1376\t4370\n1377\t4371\n1378\t4372\n1394\t4380\n1473\t4382\n2075\t4383\n2076\t4384\n2077\t4385\n2078\t4386\n155\t4392\n253\t4394\n256\t4396\n272\t4398\n273\t4400\n274\t4402\n275\t4404\n276\t4406\n277\t4408\n278\t4410\n1062\t4412\n1098\t4414\n1102\t4416\n1105\t4418\n1106\t4420\n1109\t4422\n1110\t4424\n1112\t4426\n1115\t4428\n1116\t4430\n1117\t4432\n1118\t4434\n2498\t4436\n2499\t4438\n254\t4440\n257\t4441\n280\t4442\n281\t4443\n282\t4444\n283\t4445\n284\t4446\n285\t4447\n286\t4448\n287\t4449\n288\t4450\n289\t4451\n290\t4452\n628\t4459\n1469\t4467\n1470\t4468\n1471\t4469\n";
   %inv.items["Furniture"] = "";
   %inv.items["Equipped", "Reserved"] = "587\t4356\n155\t4392\n282\t4444\n"; // hair, eyes, lips
   %inv.items["Equipped", "Clothing"] = "22861\t1\t4324\n0\t0\t0\n22862\t1\t1742\n22852\t1\t4301\n22849\t1\t4323\n22856\t1\t4321\n22854\t1\t4326\n22851\t1\t4317\n0\t0\t0\n22850\t1\t4315\n";
}

function INV_lookupInvItemID(%list, %dbID)
{
   %count = getRecordCount(%list);
   for (%i=0; %i<%count; %i++)
   {
      %record = getRecord(%list, %i);
      if (getField(%record, 1) == $dbID)
      {
         return getField(%record, 0);
      }
   }
   return 0;
}

function serverCmdTimbernate(%client)
{
   %client.setupTimberInventory();
   %client.player.setHairSkin(%client.inventory.hairID, %client.inventory.skinID);
   %client.player.updateFromEquip(%client.inventory.items["Equipped", "Clothing"]);
   %client.player.updateFromReserved(%client.inventory.items["Equipped", "Reserved"]);
   %client.player.updateClothing();
   %client.transmitInventory();
   commandToClient(%client, 'OnInventoryMessage', 'Equipped', 'Clothing', %client.inventory.items["Equipped", "Clothing"]);
   commandToClient(%client, 'OnInventoryMessage', 'Equipped', 'Reserved', %client.inventory.items["Equipped", "Reserved"]);
}

// Give item based on db id (currently 1:1 with datablock id)
function GameConnection::addItem(%this, %type, %itemID)
{
   %dbID = InventoryItemMap.map[%type, %itemID];
   %inv = %this.inventory;
   if (%dbID != 0)
   {
      %realName = InventoryItemMap.InvMap[%type];
      if (%realName !$= "")
      {
         %this.lastInvID++;
         %ident = %this.lastInvID;
         %count = getRecordCount(%inv.items[%realName]);

         if (%realName $= "Clothing")
         {
            // id, canEquip, dbID
            %realStr = %ident TAB 1 TAB %dbID;
         }
         else
         {
            %realStr = %ident TAB %dbID;
         }

         %inv.items[%realName] = setRecord(%inv.items[%realName], %count, %itemStr);
         %this.call("transmitInv_" @ %realName, %itemStr, true);

         return %ident;
      }
   }
}

// Removes inventory item (using inventory ID)
function GameConnection::removeItem(%this, %type, %itemID)
{
   %inv = %this.inventory;
   %realName = InventoryItemMap.InvMap[%type];
   if (%realName !$= "")
   {
      %itemList = %inv.items[%realName];
      %count = getRecordCount(%itemList);

      %idx = %this.getInventoryItemIndex(%realName, %itemID);

      if (%idx >= 0)
      {
         %itemList = removeRecord(%itemList, %i);
         %inv.items[%realName] = %itemList;
         %this.call("transmitInv_" @ %realName, %record, false);
      }
   }
}

function GameConnection::transmitInv_Pets(%this, %itemStr, %is_add)
{
   commandToClient(%this, 'OnInventoryMessage', %is_add ? 'AddItems' : 'RemoveItems', 'Pets', %itemStr);
}

function GameConnection::transmitInv_Travel(%this, %itemStr, %is_add)
{
   commandToClient(%this, 'OnInventoryMessage', %is_add ? 'AddItems' : 'RemoveItems', 'Travel', %itemStr);
}

function GameConnection::transmitInv_Tools(%this, %itemStr, %is_add)
{
   commandToClient(%this, 'OnInventoryMessage', %is_add ? 'AddItems' : 'RemoveItems', 'Tools', %itemStr);
}

function GameConnection::transmitInv_Clothing(%this, %itemStr, %is_add)
{
   commandToClient(%this, 'OnInventoryMessage', %is_add ? 'AddItems' : 'RemoveItems', 'Clothing', %itemStr);
}

function GameConnection::transmitInv_Reserved(%this, %itemStr, %is_add)
{
   commandToClient(%this, 'OnInventoryMessage', %is_add ? 'AddItems' : 'RemoveItems', 'Reserved', %itemStr);
}

function GameConnection::transmitInv_Furniture(%this, %itemStr, %is_add)
{
   commandToClient(%this, 'OnInventoryMessage', %is_add ? 'AddItems' : 'RemoveItems', 'Furniture', %itemStr);
}

// Get inventory record
function GameConnection::getInventoryItem(%this, %type, %itemID)
{
   %inv = %this.inventory;
   %itemList = %inv.items[%type];
   %count = getRecordCount(%itemList);

   for (%i=0; %i<%count; %i++)
   {
      %record = getRecord(%itemList, %i);
      if (getField(%record, 0) == %itemID)
      {
         return %record;
      }
   }

   return "";
}

// Get inventory record
function GameConnection::getInventoryItemDB(%this, %type, %dbID)
{
   %inv = %this.inventory;
   %itemList = %inv.items[%type];
   %count = getRecordCount(%itemList);

   for (%i=0; %i<%count; %i++)
   {
      %record = getRecord(%itemList, %i);
      if (getField(%record, 1) == %dbID)
      {
         return %record;
      }
   }

   return "";
}

// Get inventory record index
function GameConnection::getInventoryItemIndex(%this, %type, %itemID)
{
   %inv = %this.inventory;
   %itemList = %inv.items[%type];
   %count = getRecordCount(%itemList);

   for (%i=0; %i<%count; %i++)
   {
      %record = getRecord(%itemList, %i);
      if (getField(%record, 0) == %itemID)
      {
         return %i;
      }
   }

   return -1;
}

// Change clothing slot
function GameConnection::changeClothingSlot(%this, %slotID, %itemID)
{
   echo("ChangeClothingSlot" SPC %slotID SPC %itemID);
   %inv = %this.inventory;
   %itemList = %inv.items["Equipped", "Clothing"];
   %count = getRecordCount(%itemList);
   %itemInfo = %this.getInventoryItem("Clothing", %itemID);

   for (%i=0; %i<%count; %i++)
   {
      %record = getRecord(%itemList, %i);
      %typeId = getField(%record, 1);
      %itemMap[%i] = %record;
      echo(%i SPC "=" SPC %record);
   }

   if (%itemInfo $= "")
   {
      echo("Clearing slotID " @ %slotID-1);
      %itemMap[%slotID-1] = "0\t0";
      echo(%slotID-1 SPC "=" SPC "0\t0");
   }
   else
   {
      %itemDB = getField(%itemInfo, 2);
      %itemMap[%itemDB.type - 4] = getField(%itemInfo, 0) TAB (%itemDB.type-3) TAB %itemDB;
   }

   %itemList = "";
   for (%i=0; %i<10; %i++)
   {
      if (%itemMap[%i] $= "")
         %addI = "0\t0";
      else
         %addI = %itemMap[%i];

      if (%itemList $= "")
         %itemList = %addI;
      else
         %itemList = %itemList NL %addI;
   }

   %inv.items["Equipped", "Clothing"] = %itemList;
   commandToClient(%this, 'OnInventoryMessage', 'Equipped', 'Clothing', %itemList);
}

// Change reserved clothing slot
// Can be any one of 1,2,3
function GameConnection::changeReservedSlot(%this, %slotID, %itemID)
{
   echo("changeReservedSlot" SPC %slotID SPC %itemID);
   %inv = %this.inventory;
   %itemList = %inv.items["Equipped", "Reserved"];
   %count = getRecordCount(%itemList);
   %itemInfo = %this.getInventoryItem("Reserved", %itemID);
   if (%itemInfo $= "")
      return;

   for (%i=0; %i<%count; %i++)
   {
      %record = getRecord(%itemList, %i);
      %typeId = getField(%record, 1).type-1;
      echo(%typeId);
      %itemMap[%typeId] = %record;
   }

   %itemDB = getField(%itemInfo, 1);
   %slotID = %itemDB.type-1;
   %itemMap[%slotID] = %itemInfo;

   %itemList = "";
   for (%i=0; %i<3; %i++)
   {
      if (%itemMap[%i] $= "")
         %addI = "0\t0";
      else
         %addI = %itemMap[%i];

      if (%itemList $= "")
         %itemList = %addI;
      else
         %itemList = %itemList NL %addI;
   }

   %inv.items["Equipped", "Reserved"] = %itemList;
   commandToClient(%this, 'OnInventoryMessage', 'Equipped', 'Reserved', %itemList);
}

// Transmits an inventory list
function GameConnection::transmitInventoryList(%this, %listName)
{
   %plainListName = getTaggedString(%listName);
   commandToClient(%this, 'OnInventoryMessage', 'StartItems', %listName, "");

   %list = %this.inventory.items[%plainListName];
   %numRecords = getRecordCount(%list);

   for (%i=0; %i<%numRecords; %i += 18)
   {
      %itemChunk = getRecords(%list, %i, %i+18);
      commandToClient(%this, 'OnInventoryMessage', 'AddItems', %listName, %itemChunk);
   }
}

// Transmits an equipped list
function GameConnection::transmitEquippedItemList(%this, %listName)
{
   %plainListName = getTaggedString(%listName);
   %inv = %this.inventory;
   %list = %inv.items["Equipped", %plainListName];
   if (%list !$= "")
   {
      commandToClient(%this, 'OnInventoryMessage', 'Equipped', %listName, %list);
   }
}

function GameConnection::transmitInventory(%this)
{
   %this.transmitInventoryList('Pets');
   %this.transmitInventoryList('Travel');
   %this.transmitInventoryList('Tools');
   %this.transmitInventoryList('Clothing');
   %this.transmitInventoryList('Reserved');
   %this.transmitInventoryList('Furniture');
}


$InvNameLookup[0] = "Pets";
$InvNameLookup[1] = "Travel";
$InvNameLookup[2] = "Tools";
$InvNameLookup[3] = "Clothing";
$InvNameLookup[4] = "Reserved";
$InvNameLookup[5] = "Furniture";
$InvNameLookup[6] = "Equipped_Reserved";
$InvNameLookup[7] = "Equipped_Clothing";
$InvNameLookup[8] = "Equipped_HairSkin";

function GameConnection::onInventoryFromServer(%client, %type, %data)
{
   %inventory = %client.inventory;
   echo("onInventoryFromServer " @ %type);
   switch$(%type)
   {
      case Pets:
         %inventory.items[Pets] = INV_TranslateFromDB(Pets, %data);
      case Travel:
         %inventory.items[Travel] = INV_TranslateFromDB(Travel, %data);
      case Tools:
         %inventory.items[Tools] = INV_TranslateFromDB(Tools, %data);
      case Clothing:
         %inventory.items[Clothing] = INV_TranslateFromDB_Clothing(Clothing, %data);
      case Reserved:
         %inventory.items[Reserved] = INV_TranslateFromDB(Reserved, %data);
      case Furniture:
         %inventory.items[Furniture] = INV_TranslateFromDB(Furniture, %data);
      case Equipped_Reserved:
         %inventory.items[Equipped_Reserved] = INV_TranslateFromDB_EquippedReserved(%data);
         if (isObject(%client.player))
         {
            %client.player.updateFromReserved(%client.inventory.items["Equipped", "Reserved"]);
            %client.player.updateClothing();
         }
      case Equipped_Clothing:
         %inventory.items[Equipped_Clothing] = INV_TranslateFromDB_EquippedClothing(%data);
         if (isObject(%client.player))
         {
            %client.player.updateFromEquip(%client.inventory.items["Equipped", "Clothing"]);
            %client.player.updateClothing();
         }
      case Equipped_HairSkin:
         %firstRecord = getRecord(%data, 0);
         %secondRecord = getRecord(%data, 0);

         if (getField(%firstRecord, %i) == 1)
         {
            %newHair = getField(%firstRecord, 1);
            %newSkin = getField(%secondRecord, 1);
         }
         else
         {
            %newHair = getField(%firstRecord, 0);
            %newSkin = getField(%secondRecord, 1);
         }

         %inventory.hairID = %newHair;
         %inventory.skinID = %newSkin;

         echo("Inventory hair=" @ %newHair SPC "skin=" @ %newSkin);
         echo("player=" @ %client.player);

         serverCmdChangeTextureSlot(%client, hair, %newHair);
         serverCmdChangeTextureSlot(%client, skin, %newSkin);

         return;
   }

   call(Meta_invSend @ %type, %client);
}

function GameConnection::syncInventory(%client)
{
   %accID = %client.getUserAccID();
   %inventory = %client.inventory;
   echo(%accID @ ": Doing full inventory sync -> DB");

   for (%i=0; %i<9; %i++)
   {
      %typeName = $InvNameLookup[%i];
      echo(%typeName);
      
      if (%typeName $= Clothing || %typeName $= Equipped_Clothing)
      {
         %items = INV_TranslateToDB_Clothing(%inventory.items[%typeName]);
         echo("Sending " @ %typename);
         echo(%items);
         MasterResetUserInventory(%accID, %i, %items);
      }
      else if (%typename $= Equipped_HairSkin)
      {
         %items = "1\t" @ %inventory.hairID NL "2\t" @ %inventory.skinID;
         echo("Sending HAIR AND SKIN!!! This is..." @ %items);
         MasterResetUserInventory(%accID, %i, %items);
      }
      else
      {
         %items = INV_TranslateToDB(%inventory.items[%typeName]);
         echo("Sending " @ %typename);
         echo(%items);
         MasterResetUserInventory(%accID, %i, %items);
      }
   }
   MasterUpdateLastInventoryID(%accID, %client.lastInvID);
}

function GameConnection::reloadInventoryLists(%client)
{
   echo("Reloading inventory from DB");
   for (%i=0; %i<9; %i++)
   {
      MasterGetUserInventory(%client.getUserAccID(), %i);
   }
}

function GameConnection::reloadInventory(%client)
{
   %client.reloadInventoryLists();
   MasterGetLastInventoryID(%client.getUserAccID());
}

function Meta_invSendPets(%client)
{
   %client.transmitInventoryList('Pets');
}

function Meta_invSendTravel(%client)
{
   %client.transmitInventoryList('Travel');
}

function Meta_invSendTools(%client)
{
   %client.transmitInventoryList('Tools');
}

function Meta_invSendClothing(%client)
{
   %client.transmitInventoryList('Clothing');
}

function Meta_invSendReserved(%client)
{
   %client.transmitInventoryList('Reserved');
}

function Meta_invSendFurniture(%client)
{
   %client.transmitInventoryList('Furniture');
}

function Meta_invSendEquipped_Reserved(%client)
{
   %client.transmitEquippedItemList('Reserved');
}

function Meta_invSendEquipped_Clothing(%client)
{
   %client.transmitEquippedItemList('Clothing');
}

//

function serverCmdGetToolsInventory(%client) 
{
   %client.transmitInventoryList('Tools');
}

function serverCmdGetPetsInventory(%client) 
{
   %client.transmitInventoryList('Pets');
}

function serverCmdGetTravelInventory(%client) 
{
   %client.transmitInventoryList('Travel');
}

function serverCmdGetFurnitureInventory(%client) 
{
   %client.transmitInventoryList('Furniture');
}

function serverCmdGetClothingInventory(%client) 
{
   %client.transmitInventoryList('Clothing');
   %client.transmitEquippedItemList('Clothing');
}

function serverCmdGetReservedClothingInventory(%client) 
{
   %client.transmitInventoryList('Reserved');
   %client.transmitEquippedItemList('Reserved');
}

function serverCmdChangeClothingSlot(%client, %slotID, %itemID) 
{
   %client.changeClothingSlot(%slotID, %itemID);
   %client.player.updateFromEquip(%client.inventory.items["Equipped", "Clothing"]);
   %client.player.updateClothing();
}

function serverCmdChangeReservedSlot(%client, %slotID, %itemID) 
{
   %client.changeReservedSlot(%slotID, %itemID);
   %client.player.updateFromReserved(%client.inventory.items["Equipped", "Reserved"]);
   %client.player.updateClothing();
}

function serverCmdDropInventoryItem(%client, %uniqueID) 
{
   commandToClient(%client, 'OnServerMessage', "DropInventoryItem not implemented");
}

function serverCmdChangeTextureSlot(%client, %type, %id) 
{
   echo("ChangeTextureSlot" SPC %type SPC %id);

   // NOTE: Skin and hair are defined in script, so no mapping is required

   if (%type $= "skin")
      %client.inventory.skinID = %id;
   else if (%type $= "hair")
      %client.inventory.hairID = %id;

   if (isObject(%client.player))
   {
      %client.player.setHairSkin(%client.inventory.hairID, %client.inventory.skinID);
      %client.player.updateFromReserved(%client.inventory.items["Equipped", "Reserved"]);
      %client.player.updateClothing();
   }

   commandToClient(%client, 'OnInventoryMessage', 'TextureChange', 'Reserved', %client.inventory.skinID TAB %client.inventory.hairID);
}

function serverCmdAcquireItem(%client, %type, %item) 
{
   if (!%client.isAdmin)
      return;

   // Pets, Travel, Tools, Clothing, Reserved, Furniture

   // Clothing
   // UID\tcanEquip\tdbID  ...  \n

   // Everything else
   // UID\t\tdbID  ...  \n


   // only seems to be 10 slots for clothing in inventory manager...

   // Clothing slot calls:
    // commandToServer('ChangeClothingSlot', %datablock.type - $clothing::numrestypes, %uniqueID);
   // To set clothing slot.

   // Each set of clothing seems to need a unique id? Seems to be like 22858 22862 etc



   
   //commandToClient('OnInventoryMessage', "\x0119 AddItems", "\x0121 Clothing","22857\t0\t4143\n22858\t1\t4154\n22859\t1\t4328\n22860\t0\t4146\n22861\t1\t4324\n22862\t1\t1742")

   commandToClient(%client, "AcquireItem not implemented");
}


function serverCmdEquipTool(%client, %uniqueID) 
{
   //echo("EquipTool" SPC %uniqueID);
   %client.equipTool(%uniqueID);
}


function serverCmdEquipPet(%client, %petID) 
{
   %item = %client.getInventoryItem("Pets", %petID);
   %dbID = getField(%item, 1);

   if (%petID == 0)
   {
      echo("Putting away pet");
      if (isObject(%client.spawnPet))
      {
         %client.spawnPet.schedule(100, delete);
         %client.spawnPet = 0;
         %client.spawnPetID = 0;
      }

      return;
   }

   if (!isObject(%dbID) || %dbID.getClassName() !$= AIPetData)
      return;

   %dbID.onEquip(%client, %petID);
}

function serverCmdEquipTravel(%client, %uniqueID) 
{
   %item = %client.getInventoryItem("Travel", %uniqueID);
   %dbID = getField(%item, 1);

   if (!isObject(%dbID))
      return;

   %dbID.onEquip(%client, %uniqueID);
}

function GameConnection::unsetWaitEquip(%this)
{
   %this.waitEquip = false;
}


//-----------------------------------------------------------------------------

// Random stuff



function INV_selectRandomClothingDatablocks(%sex, %count)
{
   %startCount = 0;
   %itemStr = "";

   for (%i=0; %i<%count; %i++)
   {
      %itemDB = (%sex @ "ClothingList_" @ getRandom(4, 10)).randomSelect().getId();
      %itemID = InventoryItemMap.map[Clothing, %itemDB].getId();
      //echo(%itemDB SPC %itemID);

      if (%itemID !$= "")
      {
         if (%itemStr $= "")
            %itemStr = (%startCount++) TAB 1 TAB %itemID;
         else
            %itemStr = %itemStr NL ((%startCount++) TAB 1 TAB %itemID);
      }
      else
      {
         %count--;
      }
   }

   return %itemStr;
}

function INV_wearRandomClothing(%list)
{
   %item[0] = %clothing[0] = "0\t0\t0";
   %item[1] = %clothing[1] = "0\t0\t0";
   %item[2] = %clothing[2] = "0\t0\t0";
   %item[3] = %clothing[3] = "0\t0\t0";
   %item[4] = %clothing[4] = "0\t0\t0";
   %item[5] = %clothing[5] = "0\t0\t0";
   %item[6] = %clothing[6] = "0\t0\t0";
   %item[7] = %clothing[7] = "0\t0\t0";
   %item[8] = %clothing[8] = "0\t0\t0";
   %item[9] = %clothing[9] = "0\t0\t0";

   %count = getRecordCount(%list);
   for (%i=0; %i<%count; %i++)
   {
      %record = getRecord(%list, %i);
      %num = getField(%record, 0);
      %dbID = getField(%record, 2);
      %typeId = %dbID.type-4;

      if (%clothing[%i] $= "")
         %clothing[%typeId] = %record;
      else
         %clothing[%typeId] = %clothing[%typeId] NL %record;
   }

   for (%i=0; %i<10; %i++)
   {
      %recordCount = getRecordCount(%clothing[%i]);
      %item[%i] = getRecord(%clothing[%i], getRandom(0, %recordCount-1));
   }

   return %item[0] NL %item[1] NL %item[2] NL %item[3] NL %item[4] NL %item[5] NL %item[6] NL %item[7] NL %item[8] NL %item[9];
}



function INV_wearRandomReserved(%sex, %list)
{
   %item[0] = "0\t0";
   %item[1] = "0\t0";
   %item[2] = "0\t0";

   %randomHair = (%sex @ "ClothingList_1").randomSelect();
   %randomEyes = (%sex @ "ClothingList_2").randomSelect();
   %randomLips = (%sex @ "ClothingList_3").randomSelect();

   %item[0] = INV_lookupInvItemID(%randomHair) TAB %randomHair;
   %item[1] = INV_lookupInvItemID(%randomEyes) TAB %randomEyes;
   %item[2] = INV_lookupInvItemID(%randomLips) TAB %randomLips;

   if (%sex $= "Male")
   {
      if (getRandom(0, 100) < 70)
      {
         %item[2] = "0\t0";
      }
   }

   return %item[0] NL %item[1] NL %item[2];
}



function GameConnection::setupRandomInventory(%this)
{
   %inv = %this.inventory;
   %inv.hairID = getRandom(1, 8);
   %inv.skinID = getRandom(1, 8);
   %inv.CC = getRandom(0,100);
   %inv.PP = getRandom(0,100);

   %inv.items["Pets"] = "7183\t7183\n7184\t7184\n7185\t7185\n7187\t7187\n7192\t7192\n7193\t7193\n7194\t7194\n7195\t7195\n7196\t7196\n7197\t7197\n7198\t7198\n7199\t7199\n7200\t7200\n7201\t7201\n7202\t7202\n7205\t7205\n7206\t7206\n7207\t7207\n7208\t7208\n7209\t7209\n7210\t7210\n7211\t7211\n7212\t7212\n7213\t7213\n7214\t7214\n7215\t7215\n7216\t7216\n7217\t7217\n7218\t7218\n7219\t7219\n7220\t7220\n7221\t7221\n7222\t7222\n7223\t7223\n7224\t7224\n7225\t7225\n7226\t7226\n7227\t7227\n7228\t7228\n7229\t7229\n7230\t7230\n7231\t7231\n7232\t7232\n7233\t7233\n7234\t7234\n7235\t7235\n7238\t7238\n7239\t7239\n7240\t7240\n7241\t7241\n7243\t7243\n7244\t7244\n7245\t7245\n7246\t7246\n7247\t7247\n7248\t7248\n7249\t7249\n7250\t7250\n7251\t7251\n7252\t7252\n7253\t7253\n7256\t7256\n7257\t7257\n7258\t7258\n7259\t7259\n7260\t7260\n7261\t7261\n7262\t7262\n";
   %inv.items["Travel"] = "7314\t7314\n7316\t7316\n7318\t7318\n7319\t7319\n7320\t7320\n7321\t7321\n7322\t7322\n7323\t7323\n7325\t7325\n7327\t7327\n7328\t7328\n7331\t7331\n7333\t7333\n7334\t7334\n7350\t7350\n7352\t7352\n7353\t7353\n7354\t7354\n7355\t7355\n7358\t7358\n7263\t7263\n7264\t7264\n7265\t7265\n7266\t7266\n7267\t7267\n7268\t7268\n7269\t7269\n7270\t7270\n7271\t7271\n7272\t7272\n7273\t7273\n7274\t7274\n7275\t7275\n7276\t7276\n7277\t7277\n7278\t7278\n7279\t7279\n7280\t7280\n7281\t7281\n7282\t7282\n7283\t7283\n7284\t7284\n7285\t7285\n7286\t7286\n7287\t7287\n7288\t7288\n7289\t7289\n7290\t7290\n7291\t7291\n7292\t7292\n7293\t7293\n7294\t7294\n7295\t7295\n7296\t7296\n7297\t7297\n7298\t7298\n7299\t7299\n7300\t7300\n7301\t7301\n7302\t7302\n7303\t7303\n7304\t7304\n7305\t7305\n7306\t7306\n7307\t7307\n7308\t7308\n7309\t7309\n7310\t7310\n7311\t7311\n7312\t7312\n7313\t7313\n7315\t7315\n7317\t7317\n7324\t7324\n7326\t7326\n7329\t7329\n7330\t7330\n7332\t7332\n7335\t7335\n7336\t7336\n7337\t7337\n7338\t7338\n7339\t7339\n7340\t7340\n7341\t7341\n7342\t7342\n7343\t7343\n7344\t7344\n7345\t7345\n7346\t7346\n7347\t7347\n7348\t7348\n7349\t7349\n7351\t7351\n7356\t7356\n7357\t7357\n7359\t7359\n7360\t7360\n7361\t7361\n7362\t7362\n7363\t7363\n";//INV_selectRandomItemDatablocks("TravelMount", 7263, 7363, 5);
   %inv.items["Tools"] = "7044\t7044\n7045\t7045\n7046\t7046\n7047\t7047\n7048\t7048\n7049\t7049\n7050\t7050\n7051\t7051\n7052\t7052\n7053\t7053\n7054\t7054\n7055\t7055\n7056\t7056\n7057\t7057\n7058\t7058\n7059\t7059\n7060\t7060\n7061\t7061\n7062\t7062\n7063\t7063\n7064\t7064\n7065\t7065\n7066\t7066\n7067\t7067\n7068\t7068\n7069\t7069\n7070\t7070\n7071\t7071\n7072\t7072\n7073\t7073\n7074\t7074\n7075\t7075\n7076\t7076\n7077\t7077\n7078\t7078\n7079\t7079\n7080\t7080\n7081\t7081\n7082\t7082\n7083\t7083\n7084\t7084\n7085\t7085\n7086\t7086\n7087\t7087\n7088\t7088\n7089\t7089\n7090\t7090\n7091\t7091\n7092\t7092\n7093\t7093\n7094\t7094\n7095\t7095\n7096\t7096\n7097\t7097\n7098\t7098\n7099\t7099\n7100\t7100\n7101\t7101\n7102\t7102\n7103\t7103\n7104\t7104\n7105\t7105\n7106\t7106\n7107\t7107\n7108\t7108\n7109\t7109\n7110\t7110\n7111\t7111\n7112\t7112\n7113\t7113\n7114\t7114\n7115\t7115\n7116\t7116\n7117\t7117\n7118\t7118\n7119\t7119\n7120\t7120\n7121\t7121\n7122\t7122\n7123\t7123\n7124\t7124\n7125\t7125\n7126\t7126\n7127\t7127\n7128\t7128\n7129\t7129\n7130\t7130\n7131\t7131\n7132\t7132\n7133\t7133\n7134\t7134\n7135\t7135\n7136\t7136\n7137\t7137\n7138\t7138\n7139\t7139\n7140\t7140\n7141\t7141\n7142\t7142\n7143\t7143\n7144\t7144\n7145\t7145\n7146\t7146\n7147\t7147\n7148\t7148\n7149\t7149\n7150\t7150\n7151\t7151\n7152\t7152\n7153\t7153\n7154\t7154\n7155\t7155\n7156\t7156\n7157\t7157\n7158\t7158\n7159\t7159\n7160\t7160\n7161\t7161\n7162\t7162\n7163\t7163\n7164\t7164\n7166\t7166\n7167\t7167\n7168\t7168\n7169\t7169\n7170\t7170\n7171\t7171\n7172\t7172\n7173\t7173\n7174\t7174\n7175\t7175\n7177\t7177\n7178\t7178\n7179\t7179\n7180\t7180\n7181\t7181\n7182\t7182\n";//INV_selectRandomItemDatablocks("Tool", 7263, 7363, 5);
   %inv.items["Clothing"] = INV_selectRandomClothingDatablocks(%this.sex, 20);

   if (%this.sex $= "Female")
      %inv.items["Reserved"] = "251\t4339\n258\t4340\n301\t4351\n328\t4352\n333\t4353\n369\t4354\n587\t4356\n588\t4357\n589\t4358\n590\t4359\n592\t4360\n593\t4361\n779\t4363\n1119\t4367\n1374\t4368\n1375\t4369\n1376\t4370\n1377\t4371\n1378\t4372\n1394\t4380\n1473\t4382\n2075\t4383\n2076\t4384\n2077\t4385\n2078\t4386\n155\t4392\n253\t4394\n256\t4396\n272\t4398\n273\t4400\n274\t4402\n275\t4404\n276\t4406\n277\t4408\n278\t4410\n1062\t4412\n1098\t4414\n1102\t4416\n1105\t4418\n1106\t4420\n1109\t4422\n1110\t4424\n1112\t4426\n1115\t4428\n1116\t4430\n1117\t4432\n1118\t4434\n2498\t4436\n2499\t4438\n254\t4440\n257\t4441\n280\t4442\n281\t4443\n282\t4444\n283\t4445\n284\t4446\n285\t4447\n286\t4448\n287\t4449\n288\t4450\n289\t4451\n290\t4452\n628\t4459\n1469\t4467\n1470\t4468\n1471\t4469\n";
   else
      %inv.items["Reserved"] = "4341\t4341\n4342\t4342\n4343\t4343\n4344\t4344\n4345\t4345\n4346\t4346\n4347\t4347\n4348\t4348\n4349\t4349\n4350\t4350\n4355\t4355\n4362\t4362\n4364\t4364\n4365\t4365\n4366\t4366\n4373\t4373\n4374\t4374\n4375\t4375\n4376\t4376\n4377\t4377\n4378\t4378\n4379\t4379\n4381\t4381\n4387\t4387\n4388\t4388\n4389\t4389\n4390\t4390\n4391\t4391\n4393\t4393\n4395\t4395\n4397\t4397\n4399\t4399\n4401\t4401\n4403\t4403\n4405\t4405\n4407\t4407\n4409\t4409\n4411\t4411\n4413\t4413\n4415\t4415\n4417\t4417\n4419\t4419\n4421\t4421\n4423\t4423\n4425\t4425\n4427\t4427\n4429\t4429\n4431\t4431\n4433\t4433\n4435\t4435\n4437\t4437\n4439\t4439\n4453\t4453\n4454\t4454\n4455\t4455\n4456\t4456\n4457\t4457\n4458\t4458\n4460\t4460\n4461\t4461\n4462\t4462\n4463\t4463\n4464\t4464\n4465\t4465\n4466\t4466\n";

   %inv.items["Furniture"] =    %inv.items["Furniture"] = "4794\t4794\n4795\t4795\n4796\t4796\n4797\t4797\n4798\t4798\n4803\t4803\n4804\t4804\n4805\t4805\n4807\t4807\n4808\t4808\n4809\t4809\n4810\t4810\n4811\t4811\n4812\t4812\n4813\t4813\n4814\t4814\n4815\t4815\n4816\t4816\n4817\t4817\n4818\t4818\n4819\t4819\n4820\t4820\n4821\t4821\n4822\t4822\n4823\t4823\n4824\t4824\n4825\t4825\n4826\t4826\n4827\t4827\n4828\t4828\n4829\t4829\n4830\t4830\n4831\t4831\n4832\t4832\n4833\t4833\n4834\t4834\n4835\t4835\n4836\t4836\n4837\t4837\n4838\t4838\n4839\t4839\n4840\t4840\n4841\t4841\n4842\t4842\n4843\t4843\n4844\t4844\n4845\t4845\n4846\t4846\n4847\t4847\n4848\t4848\n" @ 
                             "4849\t4849\n4850\t4850\n4851\t4851\n4852\t4852\n4853\t4853\n4856\t4856\n4857\t4857\n4858\t4858\n4859\t4859\n4860\t4860\n4861\t4861\n4862\t4862\n4863\t4863\n4864\t4864\n4865\t4865\n4866\t4866\n4867\t4867\n4868\t4868\n4869\t4869\n4870\t4870\n4871\t4871\n4872\t4872\n4873\t4873\n4874\t4874\n4875\t4875\n4876\t4876\n4877\t4877\n4878\t4878\n4879\t4879\n4880\t4880\n4881\t4881\n4882\t4882\n4883\t4883\n4884\t4884\n4885\t4885\n4886\t4886\n4887\t4887\n4888\t4888\n4889\t4889\n4890\t4890\n4891\t4891\n4892\t4892\n4893\t4893\n4894\t4894\n4895\t4895\n4896\t4896\n4897\t4897\n4898\t4898\n4899\t4899\n4900\t4900\n" @ 
                             "4901\t4901\n4902\t4902\n4903\t4903\n4904\t4904\n4905\t4905\n4906\t4906\n4907\t4907\n4908\t4908\n4909\t4909\n4910\t4910\n4911\t4911\n4912\t4912\n4913\t4913\n4914\t4914\n4915\t4915\n4916\t4916\n4917\t4917\n4918\t4918\n4919\t4919\n4920\t4920\n4921\t4921\n4922\t4922\n4923\t4923\n4924\t4924\n4925\t4925\n4926\t4926\n4927\t4927\n4928\t4928\n4929\t4929\n4930\t4930\n4931\t4931\n4932\t4932\n4933\t4933\n4934\t4934\n4935\t4935\n4936\t4936\n4937\t4937\n4938\t4938\n4939\t4939\n4940\t4940\n4941\t4941\n4942\t4942\n4943\t4943\n4944\t4944\n4945\t4945\n4946\t4946\n4947\t4947\n4948\t4948\n4949\t4949\n4950\t4950\n" @ 
                             "4951\t4951\n4952\t4952\n4953\t4953\n4954\t4954\n4955\t4955\n4956\t4956\n4957\t4957\n4958\t4958\n4959\t4959\n4960\t4960\n4961\t4961\n4962\t4962\n4963\t4963\n4964\t4964\n4965\t4965\n4966\t4966\n4967\t4967\n4968\t4968\n4969\t4969\n4970\t4970\n4971\t4971\n4972\t4972\n4973\t4973\n4974\t4974\n4975\t4975\n4976\t4976\n4977\t4977\n4978\t4978\n4979\t4979\n4980\t4980\n4981\t4981\n4982\t4982\n4983\t4983\n4984\t4984\n4985\t4985\n4986\t4986\n4987\t4987\n4988\t4988\n4989\t4989\n4990\t4990\n4991\t4991\n4992\t4992\n4993\t4993\n4994\t4994\n4995\t4995\n4996\t4996\n4997\t4997\n4998\t4998\n4999\t4999\n5000\t5000\n" @ 
                             "5001\t5001\n5002\t5002\n5003\t5003\n5004\t5004\n5005\t5005\n5006\t5006\n5007\t5007\n5008\t5008\n5009\t5009\n5010\t5010\n5011\t5011\n5012\t5012\n5013\t5013\n5014\t5014\n5015\t5015\n5016\t5016\n5017\t5017\n5018\t5018\n5019\t5019\n5020\t5020\n5021\t5021\n5022\t5022\n5023\t5023\n5024\t5024\n5025\t5025\n5026\t5026\n5027\t5027\n5028\t5028\n5029\t5029\n5030\t5030\n5031\t5031\n5032\t5032\n5033\t5033\n5034\t5034\n5035\t5035\n5036\t5036\n5037\t5037\n5038\t5038\n5039\t5039\n5040\t5040\n5041\t5041\n5042\t5042\n5043\t5043\n5044\t5044\n5045\t5045\n5046\t5046\n5047\t5047\n5048\t5048\n5049\t5049\n5050\t5050\n" @ 
                             "5051\t5051\n5052\t5052\n5053\t5053\n5054\t5054\n5055\t5055\n5056\t5056\n5057\t5057\n5058\t5058\n5059\t5059\n5060\t5060\n5061\t5061\n5062\t5062\n5063\t5063\n5064\t5064\n5065\t5065\n5066\t5066\n5067\t5067\n5068\t5068\n5069\t5069\n5070\t5070\n5071\t5071\n5072\t5072\n5073\t5073\n5074\t5074\n5075\t5075\n5076\t5076\n5077\t5077\n5078\t5078\n5079\t5079\n5080\t5080\n5081\t5081\n5082\t5082\n5084\t5084\n5085\t5085\n5086\t5086\n5087\t5087\n5088\t5088\n5093\t5093\n5094\t5094\n5095\t5095\n5096\t5096\n5097\t5097\n5098\t5098\n5099\t5099\n5100\t5100\n5101\t5101\n5102\t5102\n5103\t5103\n5104\t5104\n5105\t5105\n" @ 
                             "5106\t5106\n5107\t5107\n5108\t5108\n5109\t5109\n5110\t5110\n5111\t5111\n5112\t5112\n5113\t5113\n5114\t5114\n5135\t5135\n5137\t5137\n5138\t5138\n5139\t5139\n5140\t5140\n5142\t5142\n5143\t5143\n5144\t5144\n5145\t5145\n5146\t5146\n5147\t5147\n5148\t5148\n5149\t5149\n5150\t5150\n5151\t5151\n5152\t5152\n5157\t5157\n5158\t5158\n5159\t5159\n5160\t5160\n5161\t5161\n5162\t5162\n5163\t5163\n5164\t5164\n5165\t5165\n5166\t5166\n5167\t5167\n5168\t5168\n5169\t5169\n5170\t5170\n5171\t5171\n5172\t5172\n5173\t5173\n5174\t5174\n5175\t5175\n5176\t5176\n5177\t5177\n5178\t5178\n5179\t5179\n5180\t5180\n5181\t5181\n" @ 
                             "5182\t5182\n5183\t5183\n5184\t5184\n5185\t5185\n5186\t5186\n5187\t5187\n5188\t5188\n5189\t5189\n5190\t5190\n5191\t5191\n5192\t5192\n5193\t5193\n5194\t5194\n5195\t5195\n5196\t5196\n5197\t5197\n5198\t5198\n5203\t5203\n5205\t5205\n5206\t5206\n5208\t5208\n5209\t5209\n5210\t5210\n5212\t5212\n5213\t5213\n5215\t5215\n5218\t5218\n5219\t5219\n5220\t5220\n5221\t5221\n5222\t5222\n5223\t5223\n5224\t5224\n5225\t5225\n5226\t5226\n5227\t5227\n5228\t5228\n5229\t5229\n5230\t5230\n5231\t5231\n5232\t5232\n5233\t5233\n5234\t5234\n5235\t5235\n5236\t5236\n5237\t5237\n5238\t5238\n5241\t5241\n5243\t5243\n5247\t5247\n" @ 
                             "5248\t5248\n5249\t5249\n5250\t5250\n5251\t5251\n5252\t5252\n5253\t5253\n5254\t5254\n5255\t5255\n5256\t5256\n5257\t5257\n5259\t5259\n5260\t5260\n5261\t5261\n5262\t5262\n5263\t5263\n5268\t5268\n5270\t5270\n5271\t5271\n5272\t5272\n5273\t5273\n5274\t5274\n5282\t5282\n5283\t5283\n5284\t5284\n5285\t5285\n5286\t5286\n5287\t5287\n5288\t5288\n5289\t5289\n5290\t5290\n5291\t5291\n5292\t5292\n5293\t5293\n5294\t5294\n5295\t5295\n5296\t5296\n5298\t5298\n5299\t5299\n5300\t5300\n5301\t5301\n5302\t5302\n5303\t5303\n5304\t5304\n5305\t5305\n5306\t5306\n5307\t5307\n5308\t5308\n5309\t5309\n5310\t5310\n5311\t5311\n" @ 
                             "5312\t5312\n5313\t5313\n5314\t5314\n5315\t5315\n5316\t5316\n5317\t5317\n5318\t5318\n5319\t5319\n5320\t5320\n5321\t5321\n5322\t5322\n5323\t5323\n5324\t5324\n5325\t5325\n5326\t5326\n5327\t5327\n5328\t5328\n5329\t5329\n5330\t5330\n5331\t5331\n5332\t5332\n5333\t5333\n5334\t5334\n5335\t5335\n5336\t5336\n5337\t5337\n5338\t5338\n5339\t5339\n5341\t5341\n5342\t5342\n5343\t5343\n5344\t5344\n5345\t5345\n5346\t5346\n5347\t5347\n5348\t5348\n5349\t5349\n5350\t5350\n5351\t5351\n5352\t5352\n5353\t5353\n5354\t5354\n5355\t5355\n5356\t5356\n5357\t5357\n5358\t5358\n5359\t5359\n5360\t5360\n5361\t5361\n5362\t5362\n" @ 
                             "5363\t5363\n5364\t5364\n5365\t5365\n5366\t5366\n5367\t5367\n5368\t5368\n5369\t5369\n5370\t5370\n5371\t5371\n5372\t5372\n5373\t5373\n5374\t5374\n5375\t5375\n5376\t5376\n5377\t5377\n5378\t5378\n5379\t5379\n5380\t5380\n5381\t5381\n5382\t5382\n5383\t5383\n5384\t5384\n5385\t5385\n5386\t5386\n5387\t5387\n5388\t5388\n5389\t5389\n5390\t5390\n5391\t5391\n5392\t5392\n5393\t5393\n5394\t5394\n5395\t5395\n5396\t5396\n5397\t5397\n5398\t5398\n5399\t5399\n5400\t5400\n5401\t5401\n5402\t5402\n5404\t5404\n5408\t5408\n5409\t5409\n5410\t5410\n5411\t5411\n5412\t5412\n5413\t5413\n5414\t5414\n5415\t5415\n5416\t5416\n" @ 
                             "5417\t5417\n5418\t5418\n5419\t5419\n5420\t5420\n5423\t5423\n5424\t5424\n5425\t5425\n5426\t5426\n5427\t5427\n5428\t5428\n5429\t5429\n5430\t5430\n5431\t5431\n5432\t5432\n5433\t5433\n5434\t5434\n5435\t5435\n5436\t5436\n5437\t5437\n5438\t5438\n5439\t5439\n5440\t5440\n5441\t5441\n5442\t5442\n5443\t5443\n5444\t5444\n5445\t5445\n5446\t5446\n5447\t5447\n5448\t5448\n5449\t5449\n5450\t5450\n5451\t5451\n5452\t5452\n5453\t5453\n5454\t5454\n5455\t5455\n5456\t5456\n5457\t5457\n5458\t5458\n5459\t5459\n5460\t5460\n5461\t5461\n5462\t5462\n5463\t5463\n5464\t5464\n5465\t5465\n5466\t5466\n5467\t5467\n5468\t5468\n" @ 
                             "5469\t5469\n5470\t5470\n5471\t5471\n5472\t5472\n5473\t5473\n5474\t5474\n5475\t5475\n5476\t5476\n5477\t5477\n5478\t5478\n5479\t5479\n5480\t5480\n5481\t5481\n5482\t5482\n5483\t5483\n5484\t5484\n5485\t5485\n5486\t5486\n5487\t5487\n5488\t5488\n5489\t5489\n5490\t5490\n5491\t5491\n5492\t5492\n5493\t5493\n5494\t5494\n5495\t5495\n5496\t5496\n5497\t5497\n5498\t5498\n5499\t5499\n5500\t5500\n5501\t5501\n5502\t5502\n5503\t5503\n5504\t5504\n5505\t5505\n5506\t5506\n5507\t5507\n5508\t5508\n5509\t5509\n5510\t5510\n5511\t5511\n5512\t5512\n5513\t5513\n5514\t5514\n5515\t5515\n5516\t5516\n5517\t5517\n5518\t5518\n" @ 
                             "5519\t5519\n5520\t5520\n5521\t5521\n5522\t5522\n5523\t5523\n5524\t5524\n5525\t5525\n5526\t5526\n5527\t5527\n5528\t5528\n5529\t5529\n5530\t5530\n5531\t5531\n5532\t5532\n5533\t5533\n5534\t5534\n5535\t5535\n5536\t5536\n5537\t5537\n5538\t5538\n5539\t5539\n5540\t5540\n5541\t5541\n5542\t5542\n5543\t5543\n5544\t5544\n5545\t5545\n5546\t5546\n5547\t5547\n5548\t5548\n5549\t5549\n5550\t5550\n5551\t5551\n5552\t5552\n5553\t5553\n5554\t5554\n5555\t5555\n5556\t5556\n5557\t5557\n5558\t5558\n5559\t5559\n5560\t5560\n5561\t5561\n5562\t5562\n5563\t5563\n5564\t5564\n5565\t5565\n5566\t5566\n5567\t5567\n5568\t5568\n" @ 
                             "5569\t5569\n5570\t5570\n5571\t5571\n5572\t5572\n5573\t5573\n5574\t5574\n5575\t5575\n5576\t5576\n5577\t5577\n5578\t5578\n5579\t5579\n5580\t5580\n5581\t5581\n5582\t5582\n5583\t5583\n5584\t5584\n5585\t5585\n5586\t5586\n5587\t5587\n5588\t5588\n5589\t5589\n5590\t5590\n5591\t5591\n5592\t5592\n5593\t5593\n5594\t5594\n5595\t5595\n5596\t5596\n5597\t5597\n5598\t5598\n5599\t5599\n5600\t5600\n5601\t5601\n5602\t5602\n5603\t5603\n5604\t5604\n5605\t5605\n5606\t5606\n5607\t5607\n5608\t5608\n5609\t5609\n5610\t5610\n5611\t5611\n5612\t5612\n5613\t5613\n5614\t5614\n5615\t5615\n5616\t5616\n5617\t5617\n5618\t5618\n" @ 
                             "5619\t5619\n5620\t5620\n5621\t5621\n5622\t5622\n5623\t5623\n5624\t5624\n5625\t5625\n5626\t5626\n5627\t5627\n5628\t5628\n5629\t5629\n5630\t5630\n5631\t5631\n5632\t5632\n5633\t5633\n5634\t5634\n5635\t5635\n5636\t5636\n5637\t5637\n5638\t5638\n5639\t5639\n5640\t5640\n5641\t5641\n5642\t5642\n5643\t5643\n5644\t5644\n5645\t5645\n5646\t5646\n5647\t5647\n5648\t5648\n5649\t5649\n5650\t5650\n5651\t5651\n5652\t5652\n5653\t5653\n5654\t5654\n5655\t5655\n5656\t5656\n5657\t5657\n5658\t5658\n5659\t5659\n5660\t5660\n5661\t5661\n5662\t5662\n5663\t5663\n5664\t5664\n5665\t5665\n5666\t5666\n5667\t5667\n5668\t5668\n" @ 
                             "5669\t5669\n5670\t5670\n5671\t5671\n5672\t5672\n5673\t5673\n5674\t5674\n5675\t5675\n5676\t5676\n5677\t5677\n5678\t5678\n5679\t5679\n5680\t5680\n5681\t5681\n5682\t5682\n5683\t5683\n5684\t5684\n5685\t5685\n5686\t5686\n5687\t5687\n5688\t5688\n5689\t5689\n5690\t5690\n5691\t5691\n5692\t5692\n5694\t5694\n5695\t5695\n5696\t5696\n5697\t5697\n5698\t5698\n5699\t5699\n5700\t5700\n5701\t5701\n5702\t5702\n5703\t5703\n5704\t5704\n5705\t5705\n5706\t5706\n5707\t5707\n5708\t5708\n5709\t5709\n5710\t5710\n5711\t5711\n5712\t5712\n5713\t5713\n5714\t5714\n5715\t5715\n5716\t5716\n5717\t5717\n5718\t5718\n5719\t5719\n" @ 
                             "5720\t5720\n5721\t5721\n5722\t5722\n5723\t5723\n5724\t5724\n5725\t5725\n5726\t5726\n5727\t5727\n5728\t5728\n5729\t5729\n5730\t5730\n5731\t5731\n5732\t5732\n5733\t5733\n5734\t5734\n5735\t5735\n5736\t5736\n5737\t5737\n5738\t5738\n5739\t5739\n5740\t5740\n5741\t5741\n5742\t5742\n5743\t5743\n5744\t5744\n5745\t5745\n5747\t5747\n5748\t5748\n5749\t5749\n5750\t5750\n5751\t5751\n5752\t5752\n5753\t5753\n5754\t5754\n5755\t5755\n5756\t5756\n5757\t5757\n5758\t5758\n5759\t5759\n5760\t5760\n5761\t5761\n5762\t5762\n5763\t5763\n5764\t5764\n5765\t5765\n5766\t5766\n5767\t5767\n5768\t5768\n5769\t5769\n5770\t5770\n" @ 
                             "5771\t5771\n5772\t5772\n5773\t5773\n5774\t5774\n5775\t5775\n5776\t5776\n5777\t5777\n5778\t5778\n5779\t5779\n5780\t5780\n5781\t5781\n5782\t5782\n5783\t5783\n5784\t5784\n5785\t5785\n5786\t5786\n5787\t5787\n5788\t5788\n5789\t5789\n5790\t5790\n5791\t5791\n5792\t5792\n5793\t5793\n5794\t5794\n5795\t5795\n5796\t5796\n5797\t5797\n5798\t5798\n5799\t5799\n5800\t5800\n5801\t5801\n5802\t5802\n5803\t5803\n5804\t5804\n5805\t5805\n5806\t5806\n5807\t5807\n5808\t5808\n5809\t5809\n5810\t5810\n5811\t5811\n5812\t5812\n5813\t5813\n5814\t5814\n5815\t5815\n5816\t5816\n5817\t5817\n5818\t5818\n5819\t5819\n5820\t5820\n" @ 
                             "5821\t5821\n5822\t5822\n5823\t5823\n5824\t5824\n5825\t5825\n5826\t5826\n5827\t5827\n5828\t5828\n5829\t5829\n5830\t5830\n5831\t5831\n5832\t5832\n5833\t5833\n5834\t5834\n5835\t5835\n5836\t5836\n5837\t5837\n5838\t5838\n5839\t5839\n5840\t5840\n5841\t5841\n5842\t5842\n5843\t5843\n5844\t5844\n5845\t5845\n5846\t5846\n5847\t5847\n5848\t5848\n5849\t5849\n5850\t5850\n5851\t5851\n5852\t5852\n5853\t5853\n5854\t5854\n5855\t5855\n5856\t5856\n5857\t5857\n5858\t5858\n5859\t5859\n5860\t5860\n5861\t5861\n5862\t5862\n5863\t5863\n5864\t5864\n5865\t5865\n5866\t5866\n5867\t5867\n5868\t5868\n5870\t5870\n5871\t5871\n" @ 
                             "5872\t5872\n5873\t5873\n5874\t5874\n5875\t5875\n5876\t5876\n5877\t5877\n5878\t5878\n5879\t5879\n5880\t5880\n5881\t5881\n5882\t5882\n5883\t5883\n5884\t5884\n5885\t5885\n5886\t5886\n5887\t5887\n5888\t5888\n5889\t5889\n5890\t5890\n5891\t5891\n5892\t5892\n5893\t5893\n5894\t5894\n5895\t5895\n5896\t5896\n5897\t5897\n5898\t5898\n5899\t5899\n5900\t5900\n5901\t5901\n5902\t5902\n5903\t5903\n5904\t5904\n5905\t5905\n5906\t5906\n5907\t5907\n5908\t5908\n5909\t5909\n5910\t5910\n5911\t5911\n5912\t5912\n5913\t5913\n5914\t5914\n5915\t5915\n5916\t5916\n5917\t5917\n5918\t5918\n5919\t5919\n5920\t5920\n5921\t5921\n" @ 
                             "5922\t5922\n5923\t5923\n5924\t5924\n5925\t5925\n5926\t5926\n5927\t5927\n5928\t5928\n5929\t5929\n5930\t5930\n5931\t5931\n5932\t5932\n5933\t5933\n5934\t5934\n5935\t5935\n5936\t5936\n5937\t5937\n5938\t5938\n5939\t5939\n5940\t5940\n5941\t5941\n5942\t5942\n5943\t5943\n5944\t5944\n5945\t5945\n5946\t5946\n5947\t5947\n5948\t5948\n5949\t5949\n5950\t5950\n5951\t5951\n5952\t5952\n5953\t5953\n5954\t5954\n5955\t5955\n5956\t5956\n5957\t5957\n5958\t5958\n5959\t5959\n5960\t5960\n5961\t5961\n5962\t5962\n5963\t5963\n5964\t5964\n5965\t5965\n5966\t5966\n5967\t5967\n5968\t5968\n5969\t5969\n5970\t5970\n5971\t5971\n" @ 
                             "5972\t5972\n5973\t5973\n5974\t5974\n5975\t5975\n5976\t5976\n5977\t5977\n5978\t5978\n5979\t5979\n5980\t5980\n5981\t5981\n5982\t5982\n5983\t5983\n5984\t5984\n5985\t5985\n5986\t5986\n5987\t5987\n5988\t5988\n5989\t5989\n5990\t5990\n5991\t5991\n5992\t5992\n5993\t5993\n5994\t5994\n5995\t5995\n5996\t5996\n5997\t5997\n5998\t5998\n5999\t5999\n6000\t6000\n6001\t6001\n6002\t6002\n6003\t6003\n6004\t6004\n6005\t6005\n6006\t6006\n6008\t6008\n6009\t6009\n6010\t6010\n6013\t6013\n6014\t6014\n6015\t6015\n6016\t6016\n6017\t6017\n6018\t6018\n6019\t6019\n6020\t6020\n6021\t6021\n6022\t6022\n6023\t6023\n6024\t6024\n" @ 
                             "6025\t6025\n6026\t6026\n6027\t6027\n6028\t6028\n6029\t6029\n6030\t6030\n6031\t6031\n6032\t6032\n6033\t6033\n6034\t6034\n6035\t6035\n6036\t6036\n6037\t6037\n6038\t6038\n6039\t6039\n6040\t6040\n6041\t6041\n6042\t6042\n6043\t6043\n6044\t6044\n6045\t6045\n6046\t6046\n6047\t6047\n6048\t6048\n6049\t6049\n6050\t6050\n6051\t6051\n6052\t6052\n6053\t6053\n6054\t6054\n6055\t6055\n6056\t6056\n6057\t6057\n6058\t6058\n6059\t6059\n6060\t6060\n6061\t6061\n6062\t6062\n6063\t6063\n6064\t6064\n6065\t6065\n6066\t6066\n6067\t6067\n6068\t6068\n6069\t6069\n6070\t6070\n6071\t6071\n6072\t6072\n6073\t6073\n6074\t6074\n" @ 
                             "6076\t6076\n6077\t6077\n6078\t6078\n6079\t6079\n6080\t6080\n6081\t6081\n6082\t6082\n6083\t6083\n6084\t6084\n6085\t6085\n6086\t6086\n6087\t6087\n6088\t6088\n6089\t6089\n6090\t6090\n6091\t6091\n6092\t6092\n6093\t6093\n6094\t6094\n6095\t6095\n6096\t6096\n6097\t6097\n6098\t6098\n6099\t6099\n6100\t6100\n6101\t6101\n6102\t6102\n6103\t6103\n6104\t6104\n6105\t6105\n6106\t6106\n6107\t6107\n6108\t6108\n6109\t6109\n6110\t6110\n6111\t6111\n6112\t6112\n6113\t6113\n6114\t6114\n6115\t6115\n6116\t6116\n6117\t6117\n6118\t6118\n6120\t6120\n6121\t6121\n6122\t6122\n6123\t6123\n6124\t6124\n6125\t6125\n6126\t6126\n" @ 
                             "6127\t6127\n6128\t6128\n6129\t6129\n6130\t6130\n6131\t6131\n6132\t6132\n6133\t6133\n6134\t6134\n6135\t6135\n6136\t6136\n6137\t6137\n6138\t6138\n6139\t6139\n6140\t6140\n6141\t6141\n6142\t6142\n6143\t6143\n6144\t6144\n6145\t6145\n6146\t6146\n6147\t6147\n6148\t6148\n6149\t6149\n6150\t6150\n6151\t6151\n6152\t6152\n6153\t6153\n6154\t6154\n6155\t6155\n6156\t6156\n6157\t6157\n6158\t6158\n6159\t6159\n6160\t6160\n6161\t6161\n6162\t6162\n6163\t6163\n6164\t6164\n6165\t6165\n6166\t6166\n6167\t6167\n6168\t6168\n6169\t6169\n6170\t6170\n6171\t6171\n6172\t6172\n6173\t6173\n6174\t6174\n6175\t6175\n6176\t6176\n" @ 
                             "6177\t6177\n6178\t6178\n6179\t6179\n6180\t6180\n6181\t6181\n6182\t6182\n6183\t6183\n6184\t6184\n6185\t6185\n6186\t6186\n6187\t6187\n6188\t6188\n6189\t6189\n6190\t6190\n6191\t6191\n6192\t6192\n6193\t6193\n6194\t6194\n6195\t6195\n6196\t6196\n6197\t6197\n6198\t6198\n6199\t6199\n6200\t6200\n6201\t6201\n6202\t6202\n6203\t6203\n6204\t6204\n6205\t6205\n6206\t6206\n6207\t6207\n6208\t6208\n6209\t6209\n6210\t6210\n6211\t6211\n6212\t6212\n6213\t6213\n6214\t6214\n6215\t6215\n6216\t6216\n6217\t6217\n6218\t6218\n6219\t6219\n6220\t6220\n6221\t6221\n6222\t6222\n6223\t6223\n6224\t6224\n6225\t6225\n6226\t6226\n" @ 
                             "6227\t6227\n6228\t6228\n6229\t6229\n6230\t6230\n6231\t6231\n6232\t6232\n6233\t6233\n6234\t6234\n6235\t6235\n6236\t6236\n6237\t6237\n6238\t6238\n6239\t6239\n6240\t6240\n6241\t6241\n6242\t6242\n6243\t6243\n6244\t6244\n6245\t6245\n6246\t6246\n6247\t6247\n6248\t6248\n6249\t6249\n6250\t6250\n6251\t6251\n6252\t6252\n6253\t6253\n6254\t6254\n6255\t6255\n6256\t6256\n6257\t6257\n6258\t6258\n6259\t6259\n6260\t6260\n6261\t6261\n6262\t6262\n6263\t6263\n6264\t6264\n6265\t6265\n6266\t6266\n6267\t6267\n6268\t6268\n6269\t6269\n6270\t6270\n6271\t6271\n6272\t6272\n6273\t6273\n6274\t6274\n6275\t6275\n6276\t6276\n" @ 
                             "6277\t6277\n6278\t6278\n6279\t6279\n6280\t6280\n6281\t6281\n6282\t6282\n6283\t6283\n6284\t6284\n6285\t6285\n6286\t6286\n6287\t6287\n6288\t6288\n6289\t6289\n6290\t6290\n6291\t6291\n6292\t6292\n6293\t6293\n6294\t6294\n6295\t6295\n6296\t6296\n6297\t6297\n6298\t6298\n6299\t6299\n6300\t6300\n6301\t6301\n6302\t6302\n6303\t6303\n6304\t6304\n6305\t6305\n6306\t6306\n6307\t6307\n6308\t6308\n6309\t6309\n6310\t6310\n6312\t6312\n6313\t6313\n6314\t6314\n6315\t6315\n6316\t6316\n6317\t6317\n6318\t6318\n6319\t6319\n6320\t6320\n6321\t6321\n6322\t6322\n6323\t6323\n6324\t6324\n6325\t6325\n6326\t6326\n6327\t6327\n" @ 
                             "6328\t6328\n6329\t6329\n6330\t6330\n6331\t6331\n6332\t6332\n6333\t6333\n6334\t6334\n6335\t6335\n6336\t6336\n6337\t6337\n6338\t6338\n6339\t6339\n6341\t6341\n6342\t6342\n6343\t6343\n6345\t6345\n6346\t6346\n6347\t6347\n6348\t6348\n6349\t6349\n6350\t6350\n6351\t6351\n6352\t6352\n6353\t6353\n6354\t6354\n6355\t6355\n6356\t6356\n6361\t6361\n6362\t6362\n6363\t6363\n6364\t6364\n6370\t6370\n6371\t6371\n6372\t6372\n6373\t6373\n6374\t6374\n6375\t6375\n6376\t6376\n6377\t6377\n6378\t6378\n6379\t6379\n6380\t6380\n6381\t6381\n6382\t6382\n6383\t6383\n6384\t6384\n6385\t6385\n6388\t6388\n6389\t6389\n6390\t6390\n" @ 
                             "6391\t6391\n6392\t6392\n6393\t6393\n6394\t6394\n6395\t6395\n6396\t6396\n6397\t6397\n6398\t6398\n6399\t6399\n6400\t6400\n6401\t6401\n6402\t6402\n6404\t6404\n6405\t6405\n6406\t6406\n6407\t6407\n6411\t6411\n6412\t6412\n6413\t6413\n6414\t6414\n6415\t6415\n6416\t6416\n6417\t6417\n6418\t6418\n6419\t6419\n6420\t6420\n6421\t6421\n6422\t6422\n6423\t6423\n6424\t6424\n6425\t6425\n6426\t6426\n6427\t6427\n6428\t6428\n6429\t6429\n6430\t6430\n6431\t6431\n6432\t6432\n6433\t6433\n6434\t6434\n6435\t6435\n6437\t6437\n6438\t6438\n6439\t6439\n6440\t6440\n6441\t6441\n6442\t6442\n6443\t6443\n6444\t6444\n6445\t6445\n" @ 
                             "6446\t6446\n6447\t6447\n6452\t6452\n6453\t6453\n6454\t6454\n6455\t6455\n6467\t6467\n6468\t6468\n6469\t6469\n6470\t6470\n6473\t6473\n6474\t6474\n6475\t6475\n6476\t6476\n6477\t6477\n6478\t6478\n6479\t6479\n6480\t6480\n6481\t6481\n6482\t6482\n6483\t6483\n6484\t6484\n6485\t6485\n6486\t6486\n6487\t6487\n6488\t6488\n6489\t6489\n6490\t6490\n6491\t6491\n6492\t6492\n6493\t6493\n6494\t6494\n6495\t6495\n6496\t6496\n6497\t6497\n6498\t6498\n6499\t6499\n6500\t6500\n6501\t6501\n6502\t6502\n6503\t6503\n6504\t6504\n6505\t6505\n6506\t6506\n6507\t6507\n6508\t6508\n6509\t6509\n6510\t6510\n6511\t6511\n6512\t6512\n" @ 
                             "6513\t6513\n6514\t6514\n6515\t6515\n6516\t6516\n6517\t6517\n6518\t6518\n6519\t6519\n6520\t6520\n6521\t6521\n6522\t6522\n6523\t6523\n6524\t6524\n6525\t6525\n6526\t6526\n6527\t6527\n6528\t6528\n6529\t6529\n6530\t6530\n6531\t6531\n6532\t6532\n6533\t6533\n6534\t6534\n6535\t6535\n6536\t6536\n6537\t6537\n6538\t6538\n6539\t6539\n6540\t6540\n6541\t6541\n6542\t6542\n6543\t6543\n6544\t6544\n6545\t6545\n6546\t6546\n6547\t6547\n6548\t6548\n6549\t6549\n6550\t6550\n6551\t6551\n6552\t6552\n6553\t6553\n6554\t6554\n6555\t6555\n6556\t6556\n6557\t6557\n6558\t6558\n6559\t6559\n6560\t6560\n6561\t6561\n6562\t6562\n" @ 
                             "6563\t6563\n6564\t6564\n6565\t6565\n6566\t6566\n6567\t6567\n6568\t6568\n6569\t6569\n6570\t6570\n6571\t6571\n6572\t6572\n6573\t6573\n6574\t6574\n6575\t6575\n6576\t6576\n6577\t6577\n6578\t6578\n6579\t6579\n6580\t6580\n6581\t6581\n6583\t6583\n6584\t6584\n6585\t6585\n6586\t6586\n6587\t6587\n6588\t6588\n6589\t6589\n6590\t6590\n6591\t6591\n6592\t6592\n6593\t6593\n6594\t6594\n6595\t6595\n6596\t6596\n6597\t6597\n6598\t6598\n6599\t6599\n6600\t6600\n6601\t6601\n6602\t6602\n6603\t6603\n6604\t6604\n6605\t6605\n6606\t6606\n6607\t6607\n6608\t6608\n6609\t6609\n6610\t6610\n6611\t6611\n6612\t6612\n6613\t6613\n" @ 
                             "6614\t6614\n6615\t6615\n6617\t6617\n6618\t6618\n6619\t6619\n6620\t6620\n6621\t6621\n6622\t6622\n6623\t6623\n6624\t6624\n6625\t6625\n6626\t6626\n6627\t6627\n6628\t6628\n6629\t6629\n6630\t6630\n6631\t6631\n6632\t6632\n6633\t6633\n6634\t6634\n6635\t6635\n6636\t6636\n6637\t6637\n6638\t6638\n6639\t6639\n6640\t6640\n6641\t6641\n6642\t6642\n6643\t6643\n6644\t6644\n6645\t6645\n6646\t6646\n6647\t6647\n6648\t6648\n6649\t6649\n6650\t6650\n6651\t6651\n6652\t6652\n6653\t6653\n6654\t6654\n6655\t6655\n6656\t6656\n6657\t6657\n6658\t6658\n6659\t6659\n6660\t6660\n6661\t6661\n6662\t6662\n6663\t6663\n6664\t6664\n" @ 
                             "6665\t6665\n6666\t6666\n6667\t6667\n6668\t6668\n6669\t6669\n6670\t6670\n6671\t6671\n6673\t6673\n6674\t6674\n6675\t6675\n6676\t6676\n6677\t6677\n6678\t6678\n6679\t6679\n6680\t6680\n6681\t6681\n6682\t6682\n6683\t6683\n6684\t6684\n6685\t6685\n6686\t6686\n6687\t6687\n6688\t6688\n6689\t6689\n6690\t6690\n6691\t6691\n6692\t6692\n6693\t6693\n6694\t6694\n6695\t6695\n6696\t6696\n6697\t6697\n6698\t6698\n6700\t6700\n6701\t6701\n6702\t6702\n6703\t6703\n6704\t6704\n6705\t6705\n6706\t6706\n6707\t6707\n6708\t6708\n6709\t6709\n6710\t6710\n6711\t6711\n6712\t6712\n6713\t6713\n6714\t6714\n6715\t6715\n6716\t6716\n" @ 
                             "6717\t6717\n6718\t6718\n6719\t6719\n6720\t6720\n6721\t6721\n6722\t6722\n6723\t6723\n6724\t6724\n6725\t6725\n6726\t6726\n6727\t6727\n6728\t6728\n6729\t6729\n6730\t6730\n6731\t6731\n6732\t6732\n6733\t6733\n6734\t6734\n6735\t6735\n6736\t6736\n6737\t6737\n6738\t6738\n6739\t6739\n6740\t6740\n6741\t6741\n6742\t6742\n6743\t6743\n6744\t6744\n6745\t6745\n6746\t6746\n6747\t6747\n6748\t6748\n6749\t6749\n6750\t6750\n6751\t6751\n6752\t6752\n6753\t6753\n6754\t6754\n6755\t6755\n6756\t6756\n6757\t6757\n6758\t6758\n6759\t6759\n6760\t6760\n6761\t6761\n6762\t6762\n6763\t6763\n6764\t6764\n6765\t6765\n6766\t6766\n" @ 
                             "6767\t6767\n6768\t6768\n6769\t6769\n6770\t6770\n6771\t6771\n6772\t6772\n6773\t6773\n6774\t6774\n6775\t6775\n6776\t6776\n6777\t6777\n6778\t6778\n6779\t6779\n6780\t6780\n6781\t6781\n6782\t6782\n6783\t6783\n6784\t6784\n6785\t6785\n6786\t6786\n6787\t6787\n6788\t6788\n6789\t6789\n6790\t6790\n6791\t6791\n6792\t6792\n6793\t6793\n6794\t6794\n6795\t6795\n6796\t6796\n6797\t6797\n6798\t6798\n6799\t6799\n6800\t6800\n6801\t6801\n6802\t6802\n6803\t6803\n6805\t6805\n6806\t6806\n6807\t6807\n6808\t6808\n6809\t6809\n6810\t6810\n6811\t6811\n6812\t6812\n6813\t6813\n6814\t6814\n6815\t6815\n6816\t6816\n6817\t6817\n" @ 
                             "6818\t6818\n6819\t6819\n6820\t6820\n6821\t6821\n6822\t6822\n6823\t6823\n6824\t6824\n6825\t6825\n6826\t6826\n6827\t6827\n6828\t6828\n6829\t6829\n6830\t6830\n6831\t6831\n6832\t6832\n6833\t6833\n6834\t6834\n6835\t6835\n6836\t6836\n6837\t6837\n6838\t6838\n6839\t6839\n6840\t6840\n6841\t6841\n6842\t6842\n6843\t6843\n6844\t6844\n6845\t6845\n6846\t6846\n6847\t6847\n6848\t6848\n6849\t6849\n6850\t6850\n6851\t6851\n6852\t6852\n6853\t6853\n6854\t6854\n6857\t6857\n6858\t6858\n6859\t6859\n6860\t6860\n6866\t6866\n6869\t6869\n6870\t6870\n6871\t6871\n6872\t6872\n6873\t6873\n6874\t6874\n6875\t6875\n6876\t6876\n" @ 
                             "6877\t6877\n6878\t6878\n6879\t6879\n6881\t6881\n6894\t6894\n6895\t6895\n6896\t6896\n6897\t6897\n6898\t6898\n6899\t6899\n6900\t6900\n6901\t6901\n6902\t6902\n6903\t6903\n6904\t6904\n6905\t6905\n6906\t6906\n6907\t6907\n6908\t6908\n6909\t6909\n6910\t6910\n6911\t6911\n6912\t6912\n6913\t6913\n6914\t6914\n6915\t6915\n6916\t6916\n6917\t6917\n6918\t6918\n6919\t6919\n6920\t6920\n6921\t6921\n6922\t6922\n6923\t6923\n6924\t6924\n6925\t6925\n6926\t6926\n6927\t6927\n6928\t6928\n6929\t6929\n6930\t6930\n6931\t6931\n6932\t6932\n6933\t6933\n6934\t6934\n6935\t6935\n6936\t6936\n6937\t6937\n6938\t6938\n6939\t6939\n" @ 
                             "6940\t6940\n6941\t6941\n6942\t6942\n6944\t6944\n6945\t6945\n6946\t6946\n6947\t6947\n6948\t6948\n6949\t6949\n6950\t6950\n6951\t6951\n6952\t6952\n6953\t6953\n6954\t6954\n6955\t6955\n6956\t6956\n6957\t6957\n6958\t6958\n6959\t6959\n6960\t6960\n6961\t6961\n6965\t6965\n6966\t6966\n6967\t6967\n6968\t6968\n6969\t6969\n6970\t6970\n6971\t6971\n6972\t6972\n6973\t6973\n6981\t6981\n6985\t6985\n6986\t6986\n6987\t6987\n6988\t6988\n6989\t6989\n6990\t6990\n6991\t6991\n6993\t6993\n6994\t6994\n6995\t6995\n6997\t6997\n6998\t6998\n6999\t6999\n7000\t7000\n7001\t7001\n7002\t7002\n7003\t7003\n7004\t7004\n7005\t7005\n" @ 
                             "7006\t7006\n7007\t7007\n7008\t7008\n7009\t7009\n7010\t7010\n7011\t7011\n7012\t7012\n7013\t7013\n7015\t7015\n7016\t7016\n7018\t7018\n7019\t7019\n7020\t7020\n7021\t7021\n7022\t7022\n7023\t7023\n7024\t7024\n7025\t7025\n7026\t7026\n7027\t7027\n7028\t7028\n7029\t7029\n7030\t7030\n7032\t7032\n7033\t7033\n7034\t7034\n7035\t7035\n7036\t7036\n7037\t7037\n7038\t7038\n7039\t7039\n7040\t7040\n7041\t7041\n"; // Only "Reserved" and "Clothing" can be equipped

                             //INV_selectRandomItemDatablocks("Furniture", 4794, 7041, 30);
   // Only "Reserved" and "Clothing" can be equipped
   %inv.items["Equipped", "Reserved"] = INV_wearRandomReserved(%this.sex, %inv.items["Reserved"]);
   %inv.items["Equipped", "Clothing"] = INV_wearRandomClothing(%inv.items["Clothing"]);
}

function GameConnection::randomizeEverything(%this)
{
   %this.setupRandomInventory();
   %this.player.setHairSkin(%this.inventory.hairID, %this.inventory.skinID);
   %this.player.updateFromEquip(%this.inventory.items["Equipped", "Clothing"]);
   %this.player.updateFromReserved(%this.inventory.items["Equipped", "Reserved"]);
   %this.player.updateClothing();
   %this.transmitInventory();
   commandToClient(%this, 'OnInventoryMessage', 'Equipped', 'Clothing', %this.inventory.items["Equipped", "Clothing"]);
   commandToClient(%this, 'OnInventoryMessage', 'Equipped', 'Reserved', %this.inventory.items["Equipped", "Reserved"]);
}

function INV_selectRandomItemDatablocks(%type, %startID, %endID, %count)
{
   %startCount = 0;
   %itemStr = "";

   for (%i=0; %i<%count; %i++)
   {
      %num = getRandom(%startID, %endID);
      while (%picked[num])
      {
         %num = getRandom(%startID, %endID);
      }

      %itemID = InventoryItemMap.map[%type, %num];

      if (%itemID !$= "")
      {
         %itemId = %itemID.getId();
         if (%itemStr $= "")
            %itemStr = (%startCount++) TAB %itemID;
         else
            %itemStr = %itemStr NL (%startCount++) TAB %itemID;
      }
      else
      {
         %count--;
      }
   }
}

function serverCmdRandomnate(%client)
{
   %client.randomizeEverything();
}
