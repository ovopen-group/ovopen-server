/*
ovopen-server
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// Item handling

function ItemData::onPickup(%this,%obj,%player)
{
   echo("TODO: pickup unknown item");

   // All items are dynamic
   %obj.schedule(1, delete);
   return true;
}

function PlayerPointsObject::onPickup(%this,%obj,%player)
{
   %client = %player.client;
   %inv = %client.inventory;
   %inv.ppDelta += %obj.pp;
   %client.updatePoints();

   ServerPlay3D(DB_177, %obj.position);

   commandToClient(%client, 'ShowAcquiredPoints', %obj.pp, %obj.position);

   // All items are dynamic
   %obj.schedule(1, delete);
   return true;
}

function SpawnPP(%value, %origin)
{
   if (%value > 1000)
   {
      %value = 1000;
      %dataBlock = DB_185;
   }
   else if (%value > 100)
   {
      %value = 100;
      %dataBlock = DB_184;
   }
   else if (%value > 50)
   {
      %value = 50;
      %dataBlock = DB_183;
   }
   else if (%value > 25)
   {
      %value = 25;
      %dataBlock = DB_182;
   }
   else if (%value > 10)
   {
      %value = 10;
      %dataBlock = DB_181;
   }
   else if (%value > 5)
   {
      %value = 5;
      %dataBlock = DB_180;
   }
   else if (%value > 0)
   {
      %value = 1;
      %dataBlock = DB_179;
   }
   else
   {
      return "";
   }

   %item = new Item() {
      datablock = %dataBlock;
      position = %origin;
      PP = %value;
      rotate = true;
      isStatic = false;
      scale = "0.25 0.25 0.25";
   };
   %item.playThread(0, "root");
   return %item;
}

function RandomDropPP(%origin, %maxDist)
{
   ServerPlay3D(DB_176, %origin);

   for (%i=0; %i<5; %i++)
   {
      %value = getRandom(0, 100) SPC (getRandom(0, 1000) / 1000.0);

      %direction = getRandom(0, 360) SPC getRandom(0, 360) SPC getRandom(0, 360);
      %direction = MatrixCreateFromEuler(%direction);
      %dist = getRandom(0, %maxDist * 100.0) / 100.0;
      echo("RDIR=" @ %direction SPC "DIST=" @ %dist);
      %addVec = MatrixMulVector(%direction, "0" SPC %dist SPC "0");

      SpawnPP(%value, VectorAdd(%origin, %addVec));
   }
}

function serverCmdRainPP(%client)
{
   RandomDropPP(%client.getControlObject().position, 10);
}

