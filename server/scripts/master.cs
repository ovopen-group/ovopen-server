/*
ovopen-server
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

//$regAddress = "127.0.0.1";
//$regPort = 3725;

function OnMasterServerShouldKickUser(%connection)
{
	echo("TODO: kick user " @ %uid);
}

function OnMasterServerRegistrationFailed(%code)
{
	echo("Location server not registered, aborting.");
	quit();
}

// Callback when the server has been registered
function OnMasterServerRegistered(%error, %locationID, %instanceID, %url)
{
	echo("Location server registered as " @ %url);
}

function OnMasterUserBalance(%connection, %cc, %pp)
{
	echo("User balance: " @ %connection.getUserAccID() SPC %cc SPC %pp);

	if (isObject(%connection))
	{
		%connection.cc = %cc;
		%connection.pp = %pp;
		%connection.updatePoints();
	}
}

// 

$InvNameLookup[0] = "Pets";
$InvNameLookup[1] = "Travel";
$InvNameLookup[2] = "Tools";
$InvNameLookup[3] = "Clothing";
$InvNameLookup[4] = "Reserved";
$InvNameLookup[5] = "Furniture";
$InvNameLookup[6] = "Equipped_Reserved";
$InvNameLookup[7] = "Equipped_Clothing";
$InvNameLookup[8] = "Equipped_HairSkin";



function OnMasterUserInventory(%connection, %inv_id, %data)
{
	%ident = $InvNameLookup[%inv_id];
	echo("OnMasterUserInventory" SPC %inv_id SPC %ident SPC %data);
	
	if (%ident !$= "")
	{
		%connection.onInventoryFromServer(%ident, %data);
	}
}

function OnMasterUserInvId(%connection, %inv_id)
{
	// TODO
	echo("OnMasterUserInvId" SPC %inv_id);
	%connection.lastInvID = %inv_id;
}

function OnMasterProcessManagement(%code, %param)
{
	echo("OnMasterProcessManagement" SPC %code SPC %param);
}

function OnMasterHousePlotData(%deed_id, %plot_id, %owner_id, %type_id, %max_furniture, %cc, %pp)
{
	// TODO
	echo("OnMasterHousePlotData" SPC %deed_id SPC %plot_id);
}

function OnMasterAuthenticated()
{
	echo("Login success, registering server on port " @ $locationServerPort @  " with id " @ $regID);
	MasterSetUsersAllowed(0xFFFFF0);
	MasterRegisterServer($regID, $locationServerPort, 0xFFFFF0);
}

function OnMasterServerLoginFail()
{
	echo("Login failed, quitting...");
	quit();
}

function OnMasterServerError()
{
	echo("Unknown error, quitting...");
	quit();
}

function OnMasterServerDisconnect()
{
	echo("Disconnected from master, quitting...");
	quit();
}

function OnMasterTransaction(%connection, %uid, %ticketid, %txid)
{
	echo("User " @ %uid @ " made txn " @ %txid @ " ticket=" @ %ticketid SPC "connection=" @ %connection);
	%connection.onTransactionCompleted(%ticketid, %txid);
}

function initMasterConnection()
{
	if ($regID $= "")
	{
		echo("NOT registering server with master, provide -id <num> to do so.");
		MasterSetAcceptAnonymousLogins(1);
		return;
	}

	if ($regAddress $= "" || $regPort $= "")
	{
		echo("NOT registering server with master, provide -regip <address> -regport <port> to do so.");
		MasterSetAcceptAnonymousLogins(1);
		return;
	}
	echo("user=" @ $regUsername);
	echo("pass=" @ $regPassword);

	MasterSetUsersAllowed("255");

	MasterLoginServer($regAddress, $regPort, $regUsername, $regPassword);
}

// API:
// MasterSetAcceptAnonymousLogins(%value)
// MasterSetUserConnectionId(%uid, %value)
// MasterLoginServer(%address, %port, %username, %password)
// MasterRegisterServer(%ident, %port)
// MasterSetUserLoginDetails(%username, %password, %uid, %permissions, %invID)
// MasterClearUserLoginDetails(%uid)
// MasterSetUsersAllowed(%value)
// MasterAddUserInventory(%uid, %type, %list)
// MasterRemoveUserInventory(%uid, %type, %list)
// MasterResetUserInventory(%uid, %type, %list)
// MasterGetUserInventory(%uid, %type)
// MasterUpdateLastInventoryID(%uid, %lastID)
// MasterGetLastInventoryID(%uid)
// MasterSetPlotInfo(%owner_id, %plot_id, %list)
// MasterSetReadyState(%state)
// MasterNotifyUserPresence(%uid,%presence)
// MasterQueryBalance(%uid)
// MasterNotifyTransaction(uid,%objtype,%actionid,%ccdiff,%ppdiff,%serverid)
// MasterGetUserLoginDetails(%uid)

