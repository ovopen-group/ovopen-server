/*
ovopen-server
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

function serverCmdLocalMessage(%client, %message) {
   %count = ClientGroup.getCount();
   %username = %client.name;

   for(%cl= 0; %cl < %count; %cl++)
   {
     // TODO: restrict to clients in area
     %recipient = ClientGroup.getObject(%cl);
     if (%recipient == %client)
        return;
     commandToClient(%recipient, 'OnChatMessage', %username, %message);
   }
}

function serverCmdShoutMessage(%client, %message) {
   %count = ClientGroup.getCount();
   %username = %client.name;
   
   for(%cl= 0; %cl < %count; %cl++)
   {
     %recipient = ClientGroup.getObject(%cl);
     if (%recipient == %client)
        return;
     commandToClient(%recipient, 'OnChatMessage', %username,  %message);
   }
}
