/*
ovopen-server
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


function GameConnection::loadMission(%this)
{
   // Send over the information that will display the server info
   // when we learn it got there, we'll send the data blocks
   %this.currentPhase = 0;
   if (%this.isAIControlled())
   {
      // Cut to the chase...
      %this.onClientEnterGame();
   }
   else
   {
      commandToClient(%this, 'BeginLocationDownload', $Server::Builder, $Pref::Server::LocationId);
      echo("*** Sending mission load to client: " @ $Pref::Server::RootMission);
   }
}

function serverCmdLocationDownloadAck(%client)
{
   // Start with the CRC
   %client.setMissionCRC( $missionCRC );

   // Send over the datablocks...
   // OnDataBlocksDone will get called when have confirmation
   // that they've all been received.
   %client.transmitDataBlocks($missionSequence);
}

function GameConnection::onDataBlocksDone( %this, %missionSequence )
{
   // On to the next phase
   commandToClient(%this, 'LocationDownloadPhase2');
}

function serverCmdLocationDownloadPhase2Ack(%client, %seq)
{
   %client.currentPhase = 2;

   // Update mod paths, this needs to get there before the objects.
   %client.transmitPaths();

   // Start ghosting objects to the client
   %client.activateGhosting();

   // Setup inventory
   %client.updatePoints();
   echo("NOTE: Client inventory should be setup by now!");
}

function GameConnection::clientWantsGhostAlwaysRetry(%client)
{
   if($MissionRunning)
      %client.activateGhosting();
}


function GameConnection::onGhostAlwaysFailed(%client)
{

}

function GameConnection::onGhostAlwaysObjectsReceived(%client)
{
   // Ready for next phase.
   commandToClient(%client, 'LocationDownloadPhase3');
}

function serverCmdLocationDownloadPhase3Ack(%client)
{
   %client.currentPhase = 3;
   %client.setTurbo(0);
   %client.checkMaxRate();

   echo("Sending phase 4, builder=" @ $Server::Builder);
   commandToClient(%client, 'LocationDownloadPhase4', $Pref::Server::Builder, $Pref::Server::LocationId);
}

function serverCmdLocationDownloadPhase4Ack(%client, %poi)
{
   echo("phase 4 ack!!");
   // poi should be place we spawn at

   // Server is ready to drop into the game
   %client.startMission();
   %client.onClientEnterGame();
}
