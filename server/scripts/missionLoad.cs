/*
ovopen-server
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

$MissionLoadPause = 5000;

//-----------------------------------------------------------------------------

function loadMission( %missionName, %isFirstMission ) 
{
   endMission();
   echo("*** LOADING MISSION: " @ %missionName);
   echo("*** Stage 1 load");

   // increment the mission sequence (used for ghost sequencing)
   $missionRunning = false;
   $Server::MissionFile = %missionName;

   // if this isn't the first mission, allow some time for the server
   // to transmit information to the clients:
   if( %isFirstMission || $Server::ServerType $= "SinglePlayer" )
      loadMissionStage2();
   else
      schedule( $MissionLoadPause, ServerGroup, loadMissionStage2 );
}

//-----------------------------------------------------------------------------

function loadMissionStage2() 
{
   // Create the mission group off the ServerGroup
   echo("*** Stage 2 load");
   $instantGroup = ServerGroup;

   // Make sure the mission exists
   %file = $Pref::Server::RootMission;
   
   if( !isFile( %file ) ) {
      error( "Could not find mission " @ %file );
      return;
   }

   // Calculate the mission CRC.  The CRC is used by the clients
   // to caching mission lighting.
   $missionCRC = getFileCRC( %file );

   // Exec the mission, objects are added to the ServerGroup
   exec(%file);
   
   // If there was a problem with the load, let's try another mission
   if( !isObject(LocationGroup) ) {
      error( "No 'LocationGroup' found in mission \"" @ $missionName @ "\"." );
      schedule( 3000, ServerGroup, CycleMissions );
      return;
   }

   // Convert spawnpoints to PoiPoints
   if (!isObject(PoiPoints))
   {
      echo("Converting SpawnPoints to PoiPoints");
      new SimSet(PoiPoints);
      LocationGroup.add(PoiPoints);

      if (isObject(SpawnPoints))
      {
         %count = SpawnPoints.getCount();
         for (%i=0; %i<%count; %i++)
         {
            %obj = SpawnPoints.getObject(%i);
            %poi = new ClientEditPoiPoint() {
               canSaveDynamicFields = "1";
               position = %obj.position;
               rotation = "1 0 0 0";
            };
            if (%i == 0)
            {
               %poi.PoiURLName = "spawn_main";
            }
            PoiPoints.add(%poi);
            %obj.delete();
         }
      }
   }

   // Mission cleanup group
   new SimGroup( MissionCleanup );
   $instantGroup = MissionCleanup;
   
   // Construct MOD paths
   pathOnMissionLoadDone();

   // Mission loading done...
   echo("*** Mission loaded");
   
   // Start all the clients in the mission
   $missionRunning = true;
   for( %clientIndex = 0; %clientIndex < ClientGroup.getCount(); %clientIndex++ )
      ClientGroup.getObject(%clientIndex).loadMission();

   // Go ahead and launch the game
   onMissionLoaded();
   purgeResources();
}


//-----------------------------------------------------------------------------

function endMission()
{
   if (!isObject( LocationGroup ))
      return;

   echo("*** ENDING MISSION");
   
   // Inform the game code we're done.
   onMissionEnded();

   // Inform the clients
   for( %clientIndex = 0; %clientIndex < ClientGroup.getCount(); %clientIndex++ ) {
      // clear ghosts and paths from all clients
      %cl = ClientGroup.getObject( %clientIndex );
      %cl.endMission();
      %cl.resetGhosting();
      %cl.clearPaths();
   }
   
   // Delete everything
   LocationGroup.delete();
   MissionCleanup.delete();

   $ServerGroup.delete();
   $ServerGroup = new SimGroup(ServerGroup);
}


//-----------------------------------------------------------------------------

function resetMission()
{
   echo("*** MISSION RESET");

   // Remove any temporary mission objects
   MissionCleanup.delete();
   $instantGroup = ServerGroup;
   new SimGroup( MissionCleanup );
   $instantGroup = MissionCleanup;

   //
   onMissionReset();
}
