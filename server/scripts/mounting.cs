/*
ovopen-server
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// Controls mounting behavior


//----------------------------------------------------------------------------


function ObserverCameraData::onTrigger(%this,%obj,%trigger,%state)
{
   // state = 0 means that a trigger key was released
   if (%state == 0)
      return;

   echo("Trigger=" @ %trigger SPC "state=" @ %state);

   %obj.player.clearControlObject();
   %obj.player.getDatablock().doDismount(%obj.player);
   %obj.mountedObject = 0;
}

function AvatarData::onMount(%this,%obj,%vehicle,%node)
{
   %klass = %vehicle.getClassName();
   %driving = false;

   // Cancel previous callback
   if (%obj.mountCallbackSchedule)
   {
      cancel(%obj.mountCallbackSchedule);
      %obj.mountCallbackTimes = 0;
      %obj.mountCallbackSchedule = 0;
   }

   echo("onMount klass=" @ %klass);
   %isTravel = false;
   %isMounted = false;

   switch$(%klass)
   {
      case "WheeledVehicle":
         %isMounted = true;
         %this.onMountToVehicle(%obj,%vehicle,%node);
         %driving = %node == 0;
         %isTravel = true;
         echo("onMounted travel=" @ %isTravel SPC "driver=" @ %driving);
      case "FlyingVehicle":
         %isMounted = true;
         %isTravel = true;
         %this.onMountToVehicle(%obj,%vehicle,%node);
         %driving = %node == 0;
         %isTravel = %obj.client != %vehicle.getControllingClient();
      case "InteractiveObject":
         %isMounted = true;
         %isTravel = true;
         echo("InteractiveObject");
         %this.onMountToInteractiveObject(%obj,%vehicle,%node);
      case "ClientEditInteractiveObject":
         %isMounted = true;
         echo("InteractiveObject");
         %this.onMountToInteractiveObject(%obj,%vehicle,%node);
      case "MountablePlayer":
         %isMounted = true;
         %isTravel = true;
         %this.onMountToMountablePlayer(%obj,%vehicle,%node);
   }

   %vehicle.scopeToClient(%obj.client);
   %gid = %obj.client.getGhostID(%vehicle);

   echo(">>CONTROLLING CLIENT " @ %vehicle.getControllingClient());
   echo(">>PLAYER CLIENT " @ %obj.client);
   echo(">>GID" SPC %gid);
   echo("MOUNTED=" @ %isMounted);

   // Cancel previous callback
   %obj.doMountCallback(%vehicle, %isMounted, %isTravel, %driving);
}

function Player::doMountCallback(%this, %vehicle, %mounted, %isTravel, %driving)
{
   %gid = %this.client.getGhostID(%vehicle);
   if (%gid == -1)
   {
      if (%obj.mountCallbackTimes > 10)
      {
         echo("Aborting doMountCallback for " @ %this);
      }
      else
      {
         %obj.mountCallbackSchedule = %this.schedule(10, doMountCallback, %vehicle, %mounted, %isTravel, %driving);
         %obj.mountCallbackTimes++;
      }
   }
   else
   {
      // (%ghostId, %mounted, %isTravel, %driving)
      echo("OnMountedObject" SPC %gid SPC %mounted SPC "trv=" @ %isTravel SPC %driving);
      echo("Current control client for vehicle is: " @ %vehicle.getControllingClient());
      %this.client.setupUI();
      // (%ghostId, %mounted, %isTravel, %driving)
      commandToClient(%this.client, 'OnMountedObject', %gid, %mounted, %isTravel, %driving);
   }
}

function AvatarData::onMountToVehicle(%this, %obj, %vehicle, %node)
{
   %obj.setTransform("0 0 0 0 0 1 0");
   if (%node == 0)
   {
      %obj.setControlObject(%vehicle);
   }
   else
   {
      // TODO
   }
   %obj.setActionThread("root", false);
   %obj.schedule(100, setActionThread, "mount_driving_auto", true, true);
   %obj.mountedObject = %vehicle;
}

function AvatarData::onMountToMountablePlayer(%this, %obj, %vehicle, %node)
{
	echo("onMountToMountablePlayer" SPC %obj);
   %obj.setTransform("0 0 0 0 0 1 0");
   if (%node == 0)
   {
      %obj.setControlObject(%vehicle);
   }
   else
   {
      // TODO
   }
   %obj.setActionThread("root", false);
   %obj.schedule(100, setActionThread, "mount_riding", true, true);
   %obj.mountedObject = %vehicle;


}

function AvatarData::onMountToInteractiveObject(%this,%obj,%vehicle,%node)
{
   echo("Mounted to " @ %vehicle @ ", prev=" @ %obj.mountedObject);
   %oldPlayerRot = getWords(%obj.getTransform(), 3, 6);
   //%oldPlayerForward = %obj.getForwardVector();
   %obj.setTransform("0 0 0 0 0 1 0");
   echo(%obj.client.camera.position);
   %camera = %obj.client.camera;

   if (%obj.mountedObject != %vehicle)
   {
      echo("New mount to " @ %vehicle @ " on " @ %obj);
      %center = %vehicle.getWorldBoxCenter();

      echo("Shifted mount camera transform to " @ %center);
      if (%obj.mountedObject == 0 || !isObject(%obj.mountedObject)) // new mount session
      {
         %camera.setOrbitMode(%vehicle, %center SPC %oldPlayerRot , 1.5, 4.5, 1.5, false);
      }
      else
      {
         %oldCameraRot = getWords(%camera.getTransform(), 3, 6);
         echo("fff=" @ %camera.getTransform());
         echo("oldRot = " @ %oldCameraRot);
         echo("mo=@"  @ %obj.mountedObject.position);
         echo("cam=" @ %camera.position);
         %dist = VectorDist(%camera.position, %obj.mountedObject.position);
         echo("oldDist = " @ %dist);
         %camera.setOrbitMode(%vehicle, %center SPC %oldCameraRot, 1.5, 4.5, %dist, false);
      }
      %camera.player = %obj;
   }

   %obj.client.setControlObject(%camera);
   %obj.mountedObject = %vehicle;
}

function AvatarData::onUnmount( %this, %obj, %vehicle, %node )
{
   //if (%node == 0)
   //   %obj.mountImage(%obj.lastWeapon, $WeaponSlot);
   //echo("unmounted from object");
   %driving = false;
   if (%vehicle.getClassName() $= "WheeledVehicle" || %vehicle.getClassName() $= "FlyingVehicle")
   {
      echo("Was driving vehicle");
      %driving = true;
   }
   %obj.client.setControlObject(%obj);
   %obj.doMountCallback(%vehicle, false, true, %driving);
}

function AvatarData::doDismount(%this, %obj, %forced)
{
   // This function is called by player.cc when the jump trigger
   // is true while mounted
   %obj.clearControlObject();
   if (!%obj.isMounted())
      return;

   // Cancel callback
   if (%obj.mountCallbackSchedule)
   {
      cancel(%obj.mountCallbackSchedule);
      %obj.mountCallbackTimes = 0;
      %obj.mountCallbackSchedule = 0;
   }

   // Position above dismount point
   %pos    = getWords(%obj.getTransform(), 0, 2);
   %oldPos = %pos;
   %vec[0] = " 0  0  1";
   %vec[1] = " 0  0  1";
   %vec[2] = " 0  0 -1";
   %vec[3] = " 1  0  0";
   %vec[4] = "-1  0  0";
   %impulseVec  = "0 0 0";
   %vec[0] = MatrixMulVector( %obj.getTransform(), %vec[0]);

   // Make sure the point is valid
   %pos = "0 0 0";
   %numAttempts = 5;
   %success     = -1;
   for (%i = 0; %i < %numAttempts; %i++) {
      %pos = VectorAdd(%oldPos, VectorScale(%vec[%i], 3));
      if (%obj.checkDismountPoint(%oldPos, %pos)) {
         %success = %i;
         %impulseVec = %vec[%i];
         break;
      }
   }
   if (%forced && %success == -1)
      %pos = %oldPos;

   %obj.unmount();
   %obj.mountVehicle = false;
   %obj.mountedObject = 0;
   %obj.schedule(4000, "mountVehicles", true);

   // Position above dismount point
   %obj.setTransform(%pos);
   %obj.applyImpulse(%pos, VectorScale(%impulseVec, 1));
}


