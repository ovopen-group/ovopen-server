/*
ovopen-server
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// Pet functions

function AIPet::onAdd(%this)
{
	%this.setFollowObject(%this.owner.player);
}

function AIPet::clearJump(%this)
{
	%this.setImageTrigger(2, 0);
	%this.jumpSchedule = 0;
}

function AIPet::startJumping(%this)
{
	if (%this.jumpSchedule != 0)
		return;

	%this.setImageTrigger(2, 1);
	%this.jumpSchedule = %this.schedule(100, clearJump);
}

function AIPetData::onMoveStuck(%this, %obj)
{
	%obj.startJumping();
}

function AIPetData::onCollision(%this, %obj, %otherObj, %vec, %vecl)
{
	if (%otherObj == %obj.owner.player)
	{
		echo("Bumped into owner, stopping.");
		%obj.stop();
		%obj.startCheckingOwnerDistance();
	}
}

function AIPetData::onReachDestination(%this,%obj)
{
	echo("Pet: REACHED DESTINATION <-" SPC %obj);
	%obj.startCheckingOwnerDistance();
}

function AIPetData::doTrick(%this, %obj, %trickNum)
{
	if (%trickNum >= 0 && %trickNum < %this.numTrickAnimations)
	{
		echo("AIPet trick " @ %trickNum);
		%obj.stop();
		%obj.setActionThread("trick" @ %trickNum, true);
		%obj.cancelCheckingOwnerDistance();
		%obj.startCheckingOwnerDistance();
	}
}

function AIPetData::doPet(%this, %obj)
{
	%obj.stop();
	%obj.cancelCheckingOwnerDistance();
	%obj.startCheckingOwnerDistance();
}

function AIPet::startCheckingOwnerDistance(%this)
{
	if (%this.checkOwnerDistaceSchedule != 0)
	{
		cancel(%this.checkOwnerDistaceSchedule);
	}

	%this.checkOwnerDistaceSchedule = %this.schedule(1000, checkOwnerDistace);
}

function AIPet::cancelCheckingOwnerDistance(%this)
{
	if (%this.checkOwnerDistaceSchedule == 0)
		return;

	cancel(%this.checkOwnerDistaceSchedule);
	%this.checkOwnerDistaceSchedule = 0;
}

function AIPet::checkOwnerDistace(%this)
{
	%player = %this.owner.player;
	%dist = VectorDist(%player.position, %this.position);
	if (%dist > 2)
	{
		echo("PET: Player too far away " @ %dist @ " starting to follow again...");
		%this.setFollowObject(%player);
		return;
	}

	%this.checkOwnerDistaceSchedule = %this.schedule(1000, checkOwnerDistace);
}

// onMoveStuck
// onReachDestination
// onTargetExitLOS
// onTargetEnterLOS

// fields
// moveTolerance
// numTrickAnimations
// numIdleAnimations
// triggerSound
// triggerSound2
// walkRate


// ConsoleConstructor::ConsoleConstructor(0x520c40, "AIPlayer", "stop", 0x9b0b0, "()Stop moving.", 0x2);
// ConsoleConstructor::ConsoleConstructor(0x520c00, "AIPlayer", "clearAim", 0x98ed0, "()Stop aiming at anything.", 0x2);
// ConsoleConstructor::ConsoleConstructor(0x520bc0, "AIPlayer", "setMoveSpeed", 0x98c20, "( float speed )Sets the move speed for an AI object.", 0x3);
// ConsoleConstructor::ConsoleConstructor(0x520b80, "AIPlayer", "setMoveDestination", 0x9b000, "(Point3F goal, bool slowDown=true)Tells the AI to move to the location provided.", 0x3);
// ConsoleConstructor::ConsoleConstructor(0x520b40, "AIPlayer", "getMoveDestination", 0x99190, "()Returns the point the AI is set to move to.", 0x2);
// ConsoleConstructor::ConsoleConstructor(0x520b00, "AIPlayer", "setAimLocation", 0x98ee0, "( Point3F target )Tells the AI to aim at the location provided.", 0x3);
// ConsoleConstructor::ConsoleConstructor(0x520ac0, "AIPlayer", "getAimLocation", 0x98c50, "()Returns the point the AI is aiming at.", 0x2);
// ConsoleConstructor::ConsoleConstructor(0x520a80, "AIPlayer", "setAimObject", 0x9b7c0, "( GameBase obj, [Point3F offset] )Sets the bot's target object. Optionally set an offset from target location.", 0x3);
// ConsoleConstructor::ConsoleConstructor(0x520a40, "AIPlayer", "getAimObject", 0x98cd0, "()Gets the object the AI is targeting.", 0x2);
// ConsoleConstructor::ConsoleConstructor(0x520a00, "AIPlayer", "setFollowObject", 0x9b170, "( GameBase obj )Sets the bot's follow object.", 0x3);
// ConsoleConstructor::ConsoleConstructor(0x5209c0, "AIPlayer", "clearFollow", 0x9b1e0, "()Stop following anything.", 0x2);
// ConsoleConstructor::ConsoleConstructor(0x520980, "AIPlayer", "setLookObject", 0x99050, "(Obj)Set the object that this AIPlayer is looking at.", 0x3);
// ConsoleConstructor::ConsoleConstructor(0x520940, "AIPlayer", "setLookMask", 0x98d10, "(radius, mask)Set the mask used to search for objects to look at.", 0x4);

