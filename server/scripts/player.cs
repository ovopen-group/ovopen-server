/*
ovopen-server
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

function AvatarData::onAdd(%this,%obj)
{
   // Vehicle timeout
   %obj.mountVehicle = true;

   // Default dynamic armor stats
   %obj.setRechargeRate(%this.rechargeRate);
   %obj.setRepairRate(0);
}

function AvatarData::onRemove(%this, %obj)
{
   if (%obj.client.player == %obj)
      %obj.client.player = 0;
}

function AvatarData::onNewDataBlock(%this,%obj)
{
}

function Player::onStartSwim(%this, %obj)
{
   echo("Start swim");
}

function Player::onStopSwim(%this, %obj)
{
   echo("Stop swim");
}

function Player::safeMountTo(%this, %obj, %node)
{
   // TODO: check client permissions
   if (%obj.getMountNodeObject(%node) == 0)
   {
      %client = %this.client;

      if (%client.spawnMount != %obj)
      {
         %this.clearControlObject();
         %client.cleanupMounts();
      }

      %obj.mountObject(%this, %node);
      return true;
   }
   else
   {
      return false;
   }
}

function Player::safeUnmount(%this)
{
   if (%this.isMounted())
   {
      %client = %this.client;
      %this.getDataBlock().doDismount(%this);
      %this.clearControlObject();

      %mount = %client.spawnMount;

      if (%mount != 0)
      {
         %client.cleanupMounts();
      }

      return true;
   }
}

//----------------------------------------------------------------------------

function AvatarData::onCollision(%this,%obj,%col)
{
   if (%obj.getState() $= "Dead")
      return;

   // Try and pickup all items
   if (%col.getClassName() $= "Item")
      %col.getDataBlock().onPickup(%col, %obj);
}

function AvatarData::onImpact(%this, %obj, %collidedObject, %vec, %vecLen)
{
}


//----------------------------------------------------------------------------

function AvatarData::damage(%this, %obj, %sourceObject, %position, %damage, %damageType)
{
}

function AvatarData::onDamage(%this, %obj, %delta)
{
}

function AvatarData::onDisabled(%this,%obj,%state)
{
}


//-----------------------------------------------------------------------------

function AvatarData::onLeaveMissionArea(%this, %obj)
{
   // Inform the client
   %obj.client.onLeaveMissionArea();
}

function AvatarData::onEnterMissionArea(%this, %obj)
{
   // Inform the client
   %obj.client.onEnterMissionArea();
}

//-----------------------------------------------------------------------------

function AvatarData::onEnterLiquid(%this, %obj, %coverage, %type)
{
}

function AvatarData::onLeaveLiquid(%this, %obj, %type)
{
}


//-----------------------------------------------------------------------------

function AvatarData::onTrigger(%this, %obj, %triggerNum, %val)
{
   // This method is invoked when the player receives a trigger
   // move event.  The player automatically triggers slot 0 and
   // slot one off of triggers # 0 & 1.  Trigger # 2 is also used
   // as the jump key.
   //echo("AVATAR TRIGGER NUM " @ %triggerNum);
}


function WheeledVehicleData::onTrigger(%this, %obj, %triggerNum, %val)
{
   // This method is invoked when the player receives a trigger
   // move event.  The player automatically triggers slot 0 and
   // slot one off of triggers # 0 & 1.  Trigger # 2 is also used
   // as the jump key.
   //echo("VEHICLE TRIGGER NUM " @ %triggerNum);
}


//-----------------------------------------------------------------------------
// Player methods
//-----------------------------------------------------------------------------

//----------------------------------------------------------------------------

function Player::mountVehicles(%this,%bool)
{
   // If set to false, this variable disables vehicle mounting.
   %this.mountVehicle = %bool;
}

function Player::isPilot(%this)
{
   %vehicle = %this.getObjectMount();
   // There are two "if" statements to avoid a script warning.
   if (%vehicle)
      if (%vehicle.getMountNodeObject(0) == %this)
         return true;
   return false;
}

function Player::setControl(%this, %isControl)
{
   echo("Player::setControl called " SPC %isControl);
   if (%isControl)
   {
      %this.client.setupUI();
   }
}

function Player::setHairSkin(%this, %hair, %skin)
{
   %this.hairID = %hair;
   %this.skinID = %skin;
}

//----------------------------------------------------------------------------

function Player::playCelAnimation(%this,%anim)
{
   %this.setActionThread("cel"@%anim);
}

//----------------------------------------------------------------------------

function PaintBallImage::onFire(%this)
{
   echo("PAINTBALL FIRE");
}

function SBF_ThrowSnowballClass::onFire(%this)
{
   echo("SNOWBALL FIRE");
}

function ShapeBaseImageData::onDecInv(%this)
{
   echo("SHAPE DECINV");
}

function SBF_SnowBallImageClass::onFire(%this)
{

   echo("SBF_SnowBallImageClass FIRE");
}
