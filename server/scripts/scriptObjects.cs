/*
ovopen-server
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// Test script objects

function TestScriptObject::_declareBaseClass(%this)
{
	echo("Declaring base class for TestScriptObject");

	return "TestBaseScriptObject";
}

function TestScriptObject::bark(%this)
{
	echo("FUDGE");
}

function TestBaseScriptObject::scream(%this)
{
	echo("SCREEEEAM!!!!");
}

function TestScriptObject::onAdd(%this)
{
	%this.addPersistField("LEMONLIME");
	echo("We should scream.");
	return %this.scream();
}


function TestGameTrigger::meow(%this)
{
	echo("MEOW");
	%this.dump();
}

function TestGameTrigger::onAdd(%this)
{
	echo("GAME TRIGGER ON ADD");
	%this.dump();
}

function TestGameTrigger::onEnterTrigger(%this, %obj, %arg)
{
	echo("GAME TRIGGER ENTER: " @ %obj);
	echo("GAME TRIGGER ARG: " @ %arg);

	if (%obj.getClassName() $= "Player")
	{
		%obj.setTransform(PoiPoints.getObject(0).getTransform());
	}
}


function OVTrigger::onEnterTrigger(%this)
{
	echo("GAME TRIGGER ENTER OVTRIGGER");
}

// Also on AIPlayer
function GameScriptObject::persistFieldUpdated(%this, %field)
{
	//echo("PERSIST FIELD UPDATED: " @ %field);
}

function SimSet::findNamedObject(%this, %name)
{
	%count = %this.getCount();
	for (%i=0; %i<%count; %i++)
	{
		%obj = %this.getObject(%i);
		if (%obj.isMemberOfClass("SimSet"))
		{
			%nestObj = %obj.findNamedObject(%name);
			if (%nestObj != 0)
				return %nestObj;
		}

		if (%obj.editorName $= %name)
		{
			return %obj;
		}
	}
	return 0;
}

function SimSet::findNamedObjects(%this, %baseName)
{
	%count = %this.getCount();
	%len = strlen(%baseName);
	for (%i=0; %i<%count; %i++)
	{
		%obj = %this.getObject(%i);
		if (%obj.isMemberOfClass("SimSet"))
		{
			%nestObj = %obj.findNamedObjects(%baseName);
			if (%nestObj != 0)
				return %nestObj;
		}

		if (getSubStr(%obj.editorName, 0, %len) $= %baseName)
		{
			%list = %list @ %obj @ " ";
		}
	}
	return %list;
}

function SimSet::nudgeTriggers(%this)
{
	echo("Nudge simset triggers");
	%count = %this.getCount();
	for (%i=0; %i<%count; %i++)
	{
		%obj = %this.getObject(%i);
		if (%obj.isMemberOfClass("SimSet"))
		{
			%nestObj = %obj.nudgeTriggers();
			if (%nestObj != 0)
				return %nestObj;
		}

		if (%obj.isMemberOfClass("Trigger") || %obj.isMemberOfClass("GameScriptObject"))
		{
			echo("FOUND TRIGGER/GAMESCRIPT");
			%obj.refresh();
		}
	}
	return 0;
}

function SimSet::forceAllVisible(%this)
{
	echo("Force Visibility");
	%count = %this.getCount();
	for (%i=0; %i<%count; %i++)
	{
		%obj = %this.getObject(%i);
		if (%obj.isMemberOfClass("SimSet"))
		{
			%obj.forceAllVisible();
		}

		if (%obj.isMemberOfClass("InteractiveObject"))
		{
			%obj.setHidden(false);
		}

		if (%obj.isMemberOfClass("sgLightObject"))
		{
			%obj.setEnable(1);
		}
	}
}

function SimSet::toggleEditScriptObjects(%this, %value)
{
	echo("Toggle Edit Scripts");
	%count = %this.getCount();
	for (%i=0; %i<%count; %i++)
	{
		%obj = %this.getObject(%i);
		if (%obj.isMemberOfClass("SimSet"))
		{
			%obj.toggleEditScriptObjects(%value);
		}

		if (%obj.isMemberOfClass("GameScriptObject"))
		{
			%obj.setEditorState(%value);
		}
	}
}

// TELEPORTERS

function FuncTeleport::onEnterTrigger(%this, %obj, %arg)
{
	if (%obj.client != 0)
	{
		%poi = PoiPoints.findPoi(%this.arg[0]);
		if (!isObject(%poi))
			return;
		%obj.setTransform(%poi.getTransform());
	}
}

function FuncItemTeleport::onEnterTrigger(%this, %obj, %arg)
{
	if (%obj.client != 0)
	{
		%itemRecord = %obj.client.getInventoryItemDB(%this.arg[1], %this.arg[2]);
		if (%itemRecord $= "")
			return;

		%poi = PoiPoints.findPoi(%this.arg[0]);
		if (!isObject(%poi))
			return;
		%obj.setTransform(%poi.getTransform());
	}
}

function FuncRelativeTeleporter::onEnterTrigger(%this, %obj, %arg)
{
	if (%obj.client != 0)
	{
		%xfm = %this.getTransform();
		%forwardVec = VectorNormalize(MatrixMulVector(%xfm, "0 1 0"));
		%relPos = VectorSub(%obj.position, %this.position);
		%dot = VectorDot(%relPos, %forwardVec);

		%isValid = %this.arg[1] ? (%dot > 0) : (%dot < 0);
		if (%isValid)
		{
			%invRot = MatrixInverse("0 0 0 " @ %this.rotation);
			%coordInRotSpace = MatrixMulPoint(MatrixInverse(%this.getTransform()), %obj.position);

			// Place object inside equivalent opposite position in destination teleporter
			%destTeleport = LocationGroup.findNamedObject(%this.arg[0]);
			if (isObject(%destTeleport))
			{
				%newPos = MatrixMulPoint(%destTeleport.getTransform(), %coordInRotSpace);
				%obj.setTransform(%newPos SPC getWords(%obj.getTransform(), 3, 6));
			}
		}
	}
}

function FuncRelativeTeleporter::onTickTrigger(%this)
{
	return;
	//echo("TELEPORT TRIGGER TICK");
	%count = %this.getNumObjects();
	for (%i=0; %i<%this.getNumObjects(); %i++)
	{
		%obj = %this.getObject(%i);
		%this.onEnterTrigger(%obj);
		//echo("OBJ " @ %obj SPC "Still in teleporter");
	}
	return;
}

/*
function FuncRelativeTeleporter::onLeaveTrigger(%this, %obj)
{
}*/

function FuncAnnounceArea::onEnterTrigger(%this, %obj)
{
	if (%obj.getClassName() !$= "Player")
	{
		return;
	}

	echo("Player entered area, doing announcement for " @ %this.arg[0]);
	//%this.dump();

	%obj.client.lastAreaTrigger = %this;
	%obj.client.doEnterAreaTriggerEvent(%this.arg[0], %this.arg[1]);

	%musicTrack = %this.arg[2];
	if (%musicTrack !$= "")
	{
		%obj.client.doFadeInMusic(%musicTrack);
	}

	%message = %this.arg[3];
	if (%message !$= "")
	{
		echo("Show tutorial " @ %message);
		%obj.client.doShowTutorialMessage(%message);
	}
}

function FuncAnnounceArea::onLeaveTrigger(%this, %obj)
{
	if (%obj.getClassName() !$= "Player")
	{
		return;
	}

	if (%obj.client.lastAreaTrigger != %this)
		return;

	echo("Player exited area, stopping music");

	%obj.client.doFadeOutMusic();
}

function AvatarCannonObject::onAdd(%this, %obj)
{
	%obj.addPersistField("ejectionForce");
}

function AvatarCannonObject::doRide(%this, %obj, %client)
{
	%player = %client.player;
	if (%client.spawnMount && %player.getControlObject() == %client.spawnMount)
	{
		%toFire = %client.spawnMount;
	}
	else
	{
		%toFire = %player;
	}

	%finish = %obj.getSlotTransform(0);
	%start = %obj.getSlotTransform(1);

	%cannonVec = VectorNormalize(VectorSub(%finish, %start));

	%toFire.setTransform(%finish);
	%force = %obj.ejectionForce;
	if (%toFire.getClassName() $= WheeledVehicle)
	{
		%force *= 10;
	}
	%toFire.applyImpulse(%finish, VectorScale(%cannonVec, %force));
	ServerPlay3D(DB_174, %obj.getTransform());
}

/*
function FuncTeleport::onTickTrigger(%this)
{
	echo("TELEPORT TRIGGER TICK");
	return;
}*/

/*
function Trigger::TESTonTickTrigger(%this)
{
	if (%this.targetClass $= "")
		return;

	if (%this.checkTarget !$= %this.targetClass)
	{
		echo("Basic trigger class changed, refreshing!!!...");
		%this.checkTarget = %obj.targetClass;
		%this.refresh();
	}
	else
	{
		echo("Please implement a tick method for " @ %this.targetClass);
	}

	return;
}*/

function serverCmdNudgeTriggers(%this)
{
	if (!%this.isAdmin)
		return;

	LocationGroup.nudgeTriggers();
}

function serverCmdToggleEditScriptObjects(%this, %value)
{
	if (!%this.isAdmin)
		return;

	LocationGroup.toggleEditScriptObjects(%value);
}

function serverCmdForceAllVisible(%this)
{
	if (!%this.isAdmin)
		return;

	LocationGroup.forceAllVisible();
}

function sgLightObject::doToggle(%this, %value)
{
	%this.toggled = %value;
	%this.setEnable(%value);
}

function StaticSwitchObject::onAdd(%this, %obj)
{
	if ($Server::Builder)
	{
		%obj.addPersistField("targetObject");
		%obj.addPersistField("targetValue");
	}

	if (%obj.targetValue $= "") %obj.targetValue = 1;
}

function StaticSwitchObject::doToggle(%this, %obj, %value)
{
	echo("StaticSwitchObject toggle " @ %value);
	%target = LocationGroup.findNamedObject(%obj.targetObject);
	%obj.toggled = %value;
	if (isObject(%target))
	{
		echo("StaticSwitchObject toggling object" SPC %target);
		%target.doToggle(%value ? %obj.targetValue : !%obj.targetValue);
	}
}

function TimedSwitchObject::onAdd(%this, %obj)
{
	if ($Server::Builder)
	{
		%obj.addPersistField("targetObject");
		%obj.addPersistField("targetValue");
		%obj.addPersistField("timeLimitMS");
	}

	%obj.addPersistField("dispMode");
	%obj.addPersistField("toggled");

	if (%obj.targetValue $= "") %obj.targetValue = 1;
	if (%obj.dispMode $= "") %obj.dispMode = 0;
	if (%obj.toggled $= "") %obj.toggled = 0;
}

function TimedSwitchObject::doToggle(%this, %obj, %value)
{
	if (%obj.limitSchedule !$= "")
	{
		cancel(%obj.limitSchedule);
		%obj.limitSchedule = "";
	}
	%this.setState(%obj, %value);
	%obj.limitSchedule = %this.schedule(%obj.timeLimitMS, onTimeLimitReached, %obj);
}

function TimedSwitchObject::onTimeLimitReached(%this, %obj)
{
	echo("TimedSwitchObject time limit reached, resetting...");
	%this.setState(%obj, 0);
}

function TimedSwitchObject::setState(%this, %obj, %state)
{
	echo("TimedSwitchObject setState " @ %state SPC "cur=" @ %obj.toggled);
	if (%obj.toggled == %state)
		return;

	echo("TimedSwitchObject " @ %obj @ " set to state " @ %state);
	%target = LocationGroup.findNamedObject(%obj.targetObject);

	if (%state)
	{
		%obj.stopThread(0);
		%obj.playThread(0, close_animation);
	}
	else
	{
		%obj.stopThread(0);
		%obj.playThread(0, open_animation);
	}

	%obj.toggled = %state;

	if (isObject(%target))
	{
		%target.doToggle(%state ? %obj.targetValue : !%obj.targetValue);
	}
}

function StaticCloseObject::onAdd(%this, %obj)
{
	if ($Server::Builder)
	{
		%obj.addPersistField("defaultState");
	}

	if (%obj.defaultState $= "") %obj.defaultState = 0;
}

function StaticCloseObject::doToggle(%this, %obj, %value)
{
	echo("StaticCloseObject::doToggle" SPC %value);
	%obj.stopThread(0);

	%obj.toggled = %value;

	if (%value)
	{
		%obj.playThread(0, open_animation);
	}
	else
	{
		%obj.playThread(0, close_animation);
	}
}

// mainly walls
function ToggleDisplayObject::onAdd(%this, %obj)
{
}

function ToggleDisplayObject::doToggle(%this, %obj, %value)
{
	%obj.toggled = %value;
	%obj.setHidden(%value);
}

// platform_space, forest_lillypad_flower
function TrigPlatform::onAdd(%this, %obj)
{
	%obj.addPersistField("targetObject");
	%obj.addPersistField("targetValue");

	//%obj.enableCollisionTrigger(true);
}

function InteractiveObjectData::onTrigger(%this, %obj, %triggerNum, %val)
{
	echo("IO TRIGGER " SPC %this SPC %obj SPC %triggerNum SPC %val);
}

function TrigPlatform::onTrigger(%this, %obj, %triggerNum, %val)
{
	echo("PLAT TRIGGER " SPC %this SPC %obj SPC %triggerNum SPC %val);
}

function InteractiveObjectData::onPlatformON(%this, %obj)
{
	echo("!!onPlatformON");
}

function InteractiveObjectData::onPlatformOFF(%this, %obj)
{
	echo("!!onPlatformOFF");
}

function TrigPlatform::onCollision(%this, %obj, %c1, %c2)
{
	echo("TrigPlatform onCOLLISION");
}

function TimedDisplayObject::onAdd(%this, %obj)
{
	if ($Server::Builder)
	{
		%obj.addPersistField("visibleMS");
		%obj.addPersistField("invisibleMS");
	}

	%this.flipVisibility(%obj, false);
}

function TimedDisplayObject::flipVisibility(%this, %obj, %hidden)
{
	//echo("flipVisibility TimedDisplayObject " @ %obj SPC %hidden);
	if (!isObject(%obj))
		return;

	if (%hidden)
	{
		%obj.setHidden(true);
		if (%obj.invisibleMS != 0) %this.schedule(%obj.invisibleMS, flipVisibility, %obj, false);
	}
	else
	{
		%obj.setHidden(false);
		if (%obj.visibleMS != 0) %this.schedule(%obj.visibleMS, flipVisibility, %obj, true);
	}
}


/*
function OVTrigger::onTickTrigger(%this)
{
	echo("DEFAULT TRIGGER TICK");
	return;

	if (%this.targetClass $= "")
		return;

	if (%this.checkTarget !$= %this.targetClass)
	{
		echo("Basic trigger class changed, refreshing!!!...");
		%this.checkTarget = %obj.targetClass;
		%this.refresh();
	}
}
*/

$interactiveRange = 20;

function SceneObject::verifyPlayerInRange(%this, %player)
{
    %vector = VectorSub(%this.getPosition(), %player.getPosition());
    return VectorLen(%vector) < $interactiveRange;
}

if (!isObject(ToolTargetSet))
{
	new SimSet(ToolTargetSet) {};
}

function ToolTarget::ensureTargetsPresent(%this)
{

}

function ToolTarget::onAdd(%this, %obj)
{
	%obj.addPersistField("minRespawnDelay");
	%obj.addPersistField("maxRespawnDelay");

	%obj.minRespawnDelay = 3000;
	%obj.maxRespawnDelay = 6000;

	%obj.playThread(0, "root");

	// Set some default values on DB if unset
	%block = %obj.getDatablock();
	if (%block.hideAfterDestroyMS $= "")
	{
		%block.hideAfterDestroyMS = 2000;
	}
	if (%block.fadeAfterDestroyMS $= "")
	{
		%block.fadeAfterDestroyMS = 1000;
	}

	ToolTargetSet.add(%obj);
}

function ToolTarget::kill(%this, %obj)
{
	%obj.playThread(0, "death");
	%obj.hideSchedule = %obj.schedule(hideAfterDestroyMS, onHide);
	%obj.startFade(%this.fadeAfterDestroyMS, %this.hideAfterDestroyMS, 1);
	%obj.setDamageLevel(1);

	%this.ensureTargetsPresent();
	RandomDropPP(%obj.position, 1);

	// Schedule respawn
	if (%obj.minRespawnDelay >= 0)
	{
		%randomRate = getRandom(%obj.minRespawnDelay, %obj.maxRespawnDelay);
		echo("Will respawn in " @ %randomRate);
		%obj.respawnSchedule = %obj.schedule(%randomRate, triggerRespawn);
	}
}

function InteractiveObject::triggerRespawn(%this)
{
	echo("TRIGGERED");
	%this.getDatablock().respawn(%this);
}

function ToolTarget::onHide(%this)
{
	%obj.hideSchedule = "";
	%obj.setHidden(1);
}

function ToolTarget::respawn(%this, %obj)
{
	if (!isObject(%obj))
		return;

	if (%obj.hideSchedule !$= "")
	{
		cancel(%obj.hideSchedule);
		%obj.hideSchedule = "";
	}

	if (%obj.respawnSchedule !$= "")
	{
		cancel(%obj.respawnSchedule);
		%obj.respawnSchedule = "";
	}

	%obj.setHidden(false);
	%obj.playThread(0, "root");
	%obj.startFade(0, 0, 0);
	%obj.setDamageLevel(0);
}

function SeymourTheDuck::onAdd(%this, %obj)
{
	%obj.playThread(0, "root");
}

function StaticToggleObject::onAdd(%this, %obj)
{
	%this.addPersistField("toggled");
	%obj.playThread(0, "root");
}

function StaticToggleObject::doToggle(%this, %obj, %value)
{
	echo("TODO: toggle object " SPC %obj SPC %this SPC %value);

	%obj.toggled = %value;
	%obj.enableParticles(%value);
}

function serverCmdResetToolTargets(%this)
{
	%count = ToolTargetSet.getCount();
	for (%i=0; %i<%count; %i++)
	{
		%obj = ToolTargetSet.getObject(%i);
		%obj.getDataBlock().respawn(%obj);
	}
}

function GiveToolObject::doOpen(%this, %obj)
{
	echo("TODO: give tool with description:" @ %this.desc);
}

function StaticBedObject::doMount(%this, %obj, %arg1, %client)
{
	%client.player.sit(%obj, %arg1);
}

function StaticBenchObject::onAdd(%this)
{
	%this.addPersistField("verb");
}

function StaticBenchObject::doMount(%this, %obj, %arg1, %client)
{
	echo("OBJ=" @ %obj);
	echo("PARAM=" @ %param);
	echo("CLIENT=" @ %client);
	%client.player.sit(%obj, %arg1);
}

function InteractiveObjectData::doMount(%this, %obj, %arg1, %client)
{
	%client.player.sit(%obj, %arg1);
}

// stingray_swim only
function TimedToggleObject::onAdd(%this, %obj)
{
	%obj.playThread(0, "root");
}

function FuncMultiToggleScript::onAdd(%this)
{
	%this.addPersistField("pattern0");
	%this.addPersistField("pattern1");
	%this.addPersistField("pattern2");
	%this.addPersistField("patternTime");
	%this.addPersistField("objectNameToToggle");

	%this.patternID = 0;
	%this.timerSchedule = "";

	// Find all objects with the relevant name
	if (%this.objectNameToToggle !$= "")
	{
		%this.objList = LocationGroup.findNamedObjects(%this.objectNameToToggle);
	}

	if (%this.patternTime != 0)
	{
		%this.nudgeC = $nudgeC++;
		%this.onTimer(%this.nudgeC);
	}
}

function FuncMultiToggleScript::setEditorState(%this, %state)
{
	if (!%state)
	{
		%this.nudgeC = $nudgeC++;
		%this.onTimer(%this.nudgeC);
	}
	else
	{
		if (%this.timerSchedule !$= "")
		{
			cancel(%this.timerSchedule);
			%this.timerSchedule = "";
		}
	}
}

function FuncMultiToggleScript::onTimer(%this, %nudgeC)
{
	if (%this.nudgeC != %nudgeC)
		return;

	%this.enablePattern(%this.pattern[%this.patternID]);
	%this.patternID++;
	if (%this.patternID > 2)
		%this.patternID = 0;

	%this.timerSchedule = %this.schedule(%this.patternTime, onTimer, %nudgeC);
}

function FuncMultiToggleScript::enablePattern(%this, %pattern)
{
	//echo("FuncMultiToggleScript: Enabling pattern " SPC %pattern SPC "objlist" SPC %this.objList);
	%count = getWordCount(%this.objList);
	for (%i=0; %i<%count; %i++)
	{
		%field = getWord(%this.objList, %i);
		if (isObject(%field))
		{
			%list[%i] = %field;
		}
	}

	%count = strlen(%pattern);
	%baseName = %this.objectNameToToggle;
	for (%i=0; %i<%count; %i++)
	{
		%vis = getSubStr(%pattern, %i, 1);
		%obj = %list[%i];
		if (isObject(%obj))
		{
			%obj.doToggle(%vis);
		}
	}
}

function FuncMultiToggleSpecific::onAdd(%this)
{
	%this.addPersistField("trigger0");
	%this.addPersistField("trigger1");
	%this.addPersistField("trigger2");
	%this.addPersistField("trigger3");
	%this.addPersistField("invertSignal");
}

function FuncMultiToggleSpecific::doToggle(%this, %value)
{
	if (%invertSignal)
	{
		%value = !%value;
	}

	%this.toggled = %value;

	for (%i=0; %i<4; %i++)
	{
		if (%this.trigger[%i] $= "")
			continue;

		%obj = LocationGroup.findNamedObjects(%this.trigger[%i]);
		if (isObject(%obj))
		{
			%obj.doToggle(%value);
		}
	}
}

// Allows IOs to be triggered in a generic way
function InteractiveObject::doToggle(%this, %val)
{
	%this.getDatablock().doToggle(%this, %val);
}

function FuncAndToggle::onAdd(%this)
{
	%this.addPersistField("arg0");
	%this.addPersistField("arg1");
	%this.addPersistField("targetObject");
	%this.addPersistField("targetValue");

	if (%this.targetValue $= "") %this.targetValue = 1;
	%this.toggled = -1;
}

function FuncAndToggle::doToggle(%this, %value)
{
	%targetObject = LocationGroup.findNamedObject(%this.targetObject);
	if (isObject(%targetObject))
	{
		%obj1 = LocationGroup.findNamedObject(%this.arg[0]);
		%obj2 = LocationGroup.findNamedObject(%this.arg[1]);

		%value = (%obj1.toggled && %obj2.toggled);
		if (%this.toggled != %value)
		{
			%this.toggled = %value;
			%targetObject.doToggle(%value ? %this.targetValue : !%this.targetValue);
		}
	}
}

function FuncXorToggle::onAdd(%this)
{
	%this.addPersistField("arg0");
	%this.addPersistField("arg1");
	%this.addPersistField("targetObject");
	%this.addPersistField("targetValue");

	if (%this.targetValue $= "") %this.targetValue = 1;
	%this.toggled = -1;
}

function FuncXorToggle::doToggle(%this, %value)
{
	%targetObject = LocationGroup.findNamedObject(%this.targetObject);
	if (isObject(%targetObject))
	{
		%obj1 = LocationGroup.findNamedObject(%this.arg[0]);
		%obj2 = LocationGroup.findNamedObject(%this.arg[1]);

		%value = (%obj1.toggled ^ %obj2.toggled);
		if (%this.toggled != %value)
		{
			%this.toggled = %value;
			%targetObject.doToggle(%value ? %this.targetValue : !%this.targetValue);
		}
	}
}

function FuncNotToggle::onAdd(%this)
{
	%this.addPersistField("arg0");
	%this.addPersistField("arg1");
	%this.addPersistField("targetObject");
	%this.addPersistField("targetValue");

	if (%this.targetValue $= "") %this.targetValue = 1;
	%this.toggled = -1;
}

function FuncNotToggle::doToggle(%this, %value)
{
	%targetObject = LocationGroup.findNamedObject(%this.targetObject);
	if (isObject(%targetObject))
	{
		%obj1 = LocationGroup.findNamedObject(%this.arg[0]);
		%obj2 = LocationGroup.findNamedObject(%this.arg[1]);

		%value = !(%obj1.toggled && %obj2.toggled);
		if (%this.toggled != %value)
		{
			%this.toggled = %value;
			%targetObject.doToggle(%value ? %this.targetValue : !%this.targetValue);
		}
	}
}

function FuncToggleFrequently::onAdd(%this)
{
	%this.addPersistField("timeLimitMS");
	%this.addPersistField("targetObject");
	%this.addPersistField("targetValue");
	%this.addPersistField("startOn");

	if (%this.targetValue $= "") %this.targetValue = 1;
	if (%this.toggled $= "") %this.toggled = -1;

	%this.nudgeC = $nudgeC++;

	if (%this.startOn)
	{
		%this.toggled = false;
		%this.doToggle(true);
	}
}

function FuncToggleFrequently::onTimeLimitReached(%this, %nudgeC)
{
	if (%this.nudgeC != %nudgeC)
		return;

	%this.limitSchedule = "";

	if (%this.toggled && %this.timeLimitMS != 0)
	{
		%obj = LocationGroup.findNamedObject(%this.targetObject);
		%obj.doToggle(%this.targetValue);
		%this.limitSchedule = %this.schedule(%this.timeLimitMS, onTimeLimitReached, %nudgeC);
	}
}

function FuncToggleFrequently::doToggle(%this, %value)
{
	%this.toggled = %value;

	if (%value)
	{
		if (%this.limitSchedule $= "")
		{
			%this.limitSchedule = %this.schedule(%this.timeLimitMS, onTimeLimitReached, %this.nudgeC);
		}
	}
	else
	{
		if (%this.limitSchedule !$= "")
		{
			cancel(%this.limitSchedule);
			%this.limitSchedule = "";
		}
	}
}

function Trigger::getRelMimicPos(%this, %obj)
{
	%xfm = %this.getTransform();
	%forwardVec = VectorNormalize(MatrixMulVector(%xfm, "0 1 0"));
	%relPos = VectorSub(%obj.position, %this.position);
	%invRot = MatrixInverse("0 0 0 " @ %this.rotation);
	%coordInRotSpace = MatrixMulPoint(MatrixInverse(%this.getTransform()), %obj.position);
	return %coordInRotSpace;
}

function FuncAvatarMimicTrigger::onAdd(%this)
{
	%this.followPlayerID = 0;
}

function FuncAvatarMimicTrigger::onTickTrigger(%this)
{
	if (%this.followPlayerID == 0)
		return;

	%targetAI = LocationGroup.findNamedObject(%this.arg[0]);
	%targetAITrigger = LocationGroup.findNamedObject(%this.arg[1]);

	if (!isObject(%targetAI) || !isObject(%targetAITrigger))
		return;

	%relPos = %this.getRelMimicPos(%this.followPlayerID);
	%destPos = MatrixMulPoint(%targetAITrigger.getTransform(), %relPos);
	%targetAI.setMoveDestination(%destPos);
}

function FuncAvatarMimicTrigger::onEnterTrigger(%this, %obj, %arg)
{
	echo("FuncAvatarMimicTrigger ENTER" SPC %obj);
	if (%obj.getClassName() !$= "Player")
		return;

	if (%this.followPlayerID == 0)
	{
		%this.followPlayerID = %obj;
	}
}

function FuncAvatarMimicTrigger::onExitTrigger(%this, %obj, %arg)
{
	echo("FuncAvatarMimicTrigger EXIT" SPC %obj);
	if (%this.followPlayerID == %obj)
	{
		%this.followPlayerID = 0;
	}
}

function FuncTriggerIfPresent::onEnterTrigger(%this, %obj, %arg)
{
	//echo("FuncTriggerIfPresent OBJS ENTER " SPC %this.getNumObjects());

	if (%this.getNumObjects() == 1)
	{
		%this.toggled = true;
		%targetObject = LocationGroup.findNamedObject(%this.arg[0]);
		if (!isObject(%targetObject))
			return;

		echo("FuncTriggerIfPresent START");
		%targetObject.doToggle(true);
	}
}

function FuncTriggerIfPresent::onLeaveTrigger(%this, %obj, %arg)
{
	//echo("FuncTriggerIfPresent OBJS EXIT " SPC %this.getNumObjects());

	if (%this.getNumObjects() == 0)
	{
		%this.toggled = false;
		%targetObject = LocationGroup.findNamedObject(%this.arg[0]);
		if (!isObject(%targetObject))
			return;
		
		echo("FuncTriggerIfPresent END");
		%targetObject.doToggle(false);
	}
}


