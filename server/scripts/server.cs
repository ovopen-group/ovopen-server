/*
ovopen-server
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

function createServer()
{
   //
   new SimGroup(MissionGroup); // compat with paths
   
   $missionSequence = 0;
   $Server::PlayerCount = 0;
   $Server::ServerType = %serverType;

   // Make sure the network port is set to the correct pref.
   if ($forceNetPort)
   {
      setNetPort($forceNetPort);
      $locationServerPort = $forceNetPort;
   }
   else
   {
      setNetPort($Pref::Server::Port);
      $locationServerPort = $Pref::Server::Port;
   }

   allowConnections(true);

   // Load the mission
   $ServerGroup = new SimGroup(ServerGroup);
   onServerCreated();

   echo("MISSION in createServer=" @ $Pref::Server::RootMission);
   loadMission($pref::Server::RootMission, true);

   setupClothingSystem();
}


//-----------------------------------------------------------------------------

function destroyServer()
{
   $Server::ServerType = "";
   allowConnections(false);
   $missionRunning = false;
   
   // End any running mission
   endMission();
   onServerDestroyed();

   // Delete all the server objects
   if (isObject(LocationGroup))
      LocationGroup.delete();
   if (isObject(MissionCleanup))
      MissionCleanup.delete();
   if (isObject($ServerGroup))
      $ServerGroup.delete();

   // Delete all the connections:
   while (ClientGroup.getCount())
   {
      %client = ClientGroup.getObject(0);
      %client.delete();
   }

   // Delete all the data blocks...
   echo("DELETING DATABLOCKS");
   deleteDataBlocks();
   
   // Save any server settings
   echo( "Exporting server prefs..." );
   savePreferences();

   // Dump anything we're not using
   purgeResources();
}


//--------------------------------------------------------------------------

function resetServerDefaults()
{
   echo( "Resetting server defaults..." );
   
   // Override server defaults with prefs:   
   exec( "onverse/server/defaults.cs" );
   loadPreferences();

   loadMission( $Server::MissionFile );
}

//-----------------------------------------------------------------------------

function onNeedRelight()
{

}

