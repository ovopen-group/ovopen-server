/*
ovopen-server
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

if (!isObject(RandomItemLists_clothing_objects))
{
	new SimSet(RandomItemLists_clothing_objects) {};
	new SimSet(RandomItemLists_interactive_objects) {};
	new SimSet(RandomItemLists_tool_objects) {};
	new SimSet(RandomItemLists_pet_objects) {};
	new SimSet(RandomItemLists_travelmount_objects) {};
}

/*
	There are several types of purchase object:

		PurchaseInteractiveObject
		PurchaseClothingObject
		PurchaseToolObject
		PurchaseAIPet
		PurchaseShoulderPet
		PurchaseMountablePlayer
		PurchaseWheeledVehicle

	Purchase objects should not be placed in the builder. Instead a StoreArea 
	object should be used to designate where the purchase object should be.

	The StoreArea should then list up to 10 store folders which will be used to select an appropriate item to
	randomly spawn in the location.

	StoreArea funcs:
		clearRandomObjects - clears random object list
		addRandomObject(tier, object_id, type_id) - adds random object
		getTierCount(int(tierID)) - gets amount of objects belonging to a tier
		getRandomObject(int(tier)) - selects a random object, returns "<int> <int>"  (objectId typeId)

	StoreArea stores a vector of random objects per tier. Each entry is (StoreArea::ObjectEntry) = (objectId = <int>, type = <int>)

	0x260 - object vector
		0x26c - object pointer list

	The RPC command, RPC_StoreFolders can be used to get an item list. This will return a list of <entry_id> <type_id> <resource_id> <name>
	typeID can be used to select the correct object. We can keep a cache of item -> db ids like with the inventory code.


	NOTE: InteractiveObjectData also has clearRandomObjects, addRandomObject, getTierCount, getRandomObject, addPersistField - maybe for drops?

	PurchaseToolObject has "toolDatablock" also presumably fields are assigned for item id?

	example with PurchaseInteractiveObject

	PurchaseInteractiveObject::constructContextMenu -> PurchaseInteractiveObject::onPurchase -> PurchaseItem(%this); (target = local obj id)

	Custom purchases send GetCustomPurchaseArgument to server which must return <text> <sequence>
	Eventually when you click purchase it sends serverCmdPurchaseItem(gid, type(CC=1, PP=2))

*/

function LoadRandomItemLists()
{
	RandomItemLists_clothing_objects.clear();
	RandomItemLists_clothing_objects.typeID = $objects::tableID[clothing_objects];
	RandomItemLists_interactive_objects.clear();
	RandomItemLists_interactive_objects.typeID = $objects::tableID[interactive_objects];
	RandomItemLists_tool_objects.clear();
	RandomItemLists_tool_objects.typeID = $objects::tableID[tool_objects];
	RandomItemLists_pet_objects.clear();
	RandomItemLists_pet_objects.typeID = $objects::tableID[pet_objects];
	RandomItemLists_travelmount_objects.clear();
	RandomItemLists_travelmount_objects.typeID = $objects::tableID[travelmount_objects];

	%db = DataBlockGroup.getId();
	%count = %db.getCount();

	for (%i=0; %i<%count; %i++)
	{
		%obj = DataBlockGroup.getObject(%i);
		switch$(%obj.getClassName())
		{
			case ClothingData:
				if (%obj.type > 3)
					RandomItemLists_clothing_objects.add(%obj);
			case ClientEditInteractiveObjectData:
				RandomItemLists_interactive_objects.add(%obj);
			case ToolImageData:
				RandomItemLists_tool_objects.add(%obj);
			case AIPetData:
				RandomItemLists_pet_objects.add(%obj);
			case ShoulderPetData:
				RandomItemLists_pet_objects.add(%obj);
			case MountablePlayerData:
				if (%obj.shapeFile !$= "")
					RandomItemLists_travelmount_objects.add(%obj);
			case WheeledVehicleData:
				//if (%obj.shapeFile !$= "")
				//	RandomItemLists_travelmount_objects.add(%obj);
		}
	}
}

function PickRandomStoreItemList()
{
	%idx = getRandom(0, 4);
	switch(%idx)
	{
		case 0:
			return RandomItemLists_clothing_objects;
		case 1:
			return RandomItemLists_interactive_objects;
		case 2:
			return RandomItemLists_tool_objects;
		case 3:
			return RandomItemLists_pet_objects;
		case 4:
			return RandomItemLists_travelmount_objects;
		default:
			echo("WTF SOMETHING WENT WRONG");
	}
}

LoadRandomItemLists();

function ResetStoreAreas()
{
	%count = StoreAreasGroup.getCount();
	for (%i=0; %i<%count; %i++)
	{
		%obj = StoreAreasGroup.getObject(%i);
		%obj.item = "";
	}
}

function ReloadStoreAreas()
{
	%count = StoreAreasGroup.getCount();
	for (%i=0; %i<%count; %i++)
	{
		%obj = StoreAreasGroup.getObject(%i);
		%obj.clearRandomObjects();
		//echo("SETTING UP SA " @ %obj SPC %obj.editorName);

		// Make a random list for now
		%list = PickRandomStoreItemList();
		%items = %list.randomSelectUnique(5);
		%list_count = getRecordCount(%items);

		//echo("RAND ITEMS: " @ %list_count @ " FROM LIST: " @ %list);

		for (%j=0; %j<%list_count; %j++)
		{
			%dbID = getRecord(%items, %j);
			%obj.addRandomObject(%dbID.tier, %dbID, %list.typeID);
			//echo("  SA: ADDING OBJ " @ %dbID.tier @ "<< TIER" SPC %dbID SPC %list.typeID);
		}

		// Select base tier
		for (%j=0; %j<4; %j++)
		{
			//echo("  SA: TIERC " @ %j SPC "=" SPC %obj.getTierCount(%j));
			if (%obj.getTierCount(%j) > 0)
			{
				%obj.selectTier = %j;
				break;
			}
		}
		//echo("TIER " @ %obj.selectTier @ " SELECTED!");
	}
}

function RespawnStoreItems()
{
	%count = StoreAreasGroup.getCount();
	for (%i=0; %i<%count; %i++)
	{
		StoreAreasGroup.getObject(%i).spawnItem();
	}
}

function ClearStoreItems()
{
	%count = StoreAreasGroup.getCount();
	for (%i=0; %i<%count; %i++)
	{
		StoreAreasGroup.getObject(%i).clearItem();
	}
}

function StoreArea::clearItem(%this)
{
	if (%this.item !$= "" && isObject(%this.item))
	{
		echo("StoreArea DELETING" SPC %this.item SPC %this.item.getClassName());
		%this.dump();
		%this.item.dump();
		%this.item.delete();
	}
}

function StoreArea::spawnItem(%this)
{
	echo("StoreArea" SPC %this SPC "spawnItem");
	return;

	%this.clearItem();
	%randomItem = %this.getRandomObject(%this.selectTier);
	echo("SA " @ %this.editorName SPC "selected" SPC %randomItem);
	if (%randomItem $= "")
		return;

	%dbID = getWord(%randomItem, 0);
	%typeId = getWord(%randomItem, 1);

	%pos = %this.position;
	%rot = %this.rotation;


	echo("DB=" @ %dbID);
	echo("TYPE=" @ %typeId);
	echo("REV NAME: " @ $objects::reverseName[%typeId]);

	%desc = %this.description $= "" ? %dbID.description : %this.description;
	%display = "";

	switch$ ($objects::reverseName[%typeId])
	{
		case clothing_objects:
			if (%dbID.dualSex) // means there are actually two datablocks
			{
				if (%dbID.sex $= "Male")
				{
					%mcDB = %dbID;
					%fcDB = %dbID+1;
				}
				else
				{
					%mcDB = %dbID-1;
					%fcDB = %dbID;
				}
			}
			else if (%dbID.sex $= "Male") // male only
			{
				%mcDB = %fcDB = %dbID;
			}
			else // female only
			{
				%fcDB = %fcDB = %dbID;
			}
			%display = new PurchaseClothingObject() {
				datablock = %dbID;
				rotate = true;
				description = %desc;
				maleClothing = %mcDB;
				femaleClothing = %fcDB;
				basePosition = %pos;
				baseRotation = %rot;
				//scale = %this.scale;
			};
			%display.setTransform(%pos);
		case interactive_objects:
			echo("MAKING IO DISPLAY");
			%display = new PurchaseInteractiveObject() {
				datablock = %dbID;
				rotate = true;
				description = %desc;
				basePosition = %pos;
				baseRotation = %rot;
				//scale = %this.scale;
			};
			%display.setTransform(%pos);
			echo("IO DISPLAY = " @ %display);
		case tool_objects:
			%display = new PurchaseToolObject() {
				toolDatablock = %dbID;
				rotate = true;
				description = %desc;
				basePosition = %pos;
				baseRotation = %rot;
				//scale = %this.scale;
			};
			%display.setTransform(%pos);
		case pet_objects:
			if (%dbID.getClassName() $= ShoulderPetData)
			{
				%display = new PurchaseShoulderPet() {
					datablock = %dbID;
					rotate = true;
					description = %desc;
					basePosition = %pos;
					baseRotation = %rot;
					//scale = %this.scale;
				};
				%display.setTransform(%pos);
			}
			else
			{
				%display = new PurchaseAIPet() {
					datablock = %dbID;
					rotate = true;
					description = %desc;
					basePosition = %pos;
					baseRotation = %rot;
					//scale = %this.scale;
				};
				%display.setTransform(%pos);
			}
		case travelmount_objects:

			if (%dbID.getClassName() $= WheeledVehicleData)
			{
				%display = new PurchaseWheeledVehicle() {
					datablock = %dbID;
					rotate = true;
					description = %desc;
					basePosition = %pos;
					baseRotation = %rot;
					//scale = %this.scale;
				};
				%display.setTransform(%pos);
			}
			else
			{
				%display = new PurchaseMountablePlayer() {
					datablock = %dbID;
					rotate = true;
					description = %desc;
					basePosition = %pos;
					baseRotation = %rot;
					//scale = %this.scale;
				};
				%display.setTransform(%pos);
			}
	}

	%this.item = %display;
}

function serverCmdRespawnTestStores(%client)
{
	ReloadStoreAreas();
	RespawnStoreItems();
}
