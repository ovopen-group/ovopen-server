/*
ovopen-server
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// Transactions file
// Transactions should describe steps required to recreate the users inventory.

function RegisterTransactionType(%typename, %id)
{
   $TransactionType[%id] = %typename;
   $TransactionTypeID[%typename] = %id;
}

// Perform transaction with associated cost
function GameConnection::performTransaction(%client, %name, %itemid, %cc, %pp, %internal_id)
{
   echo("Running txn " @ %name @ %cc @ %pp);
   call("Transaction_" @ %name, %itemid, %cc, %pp, %internal_id);
   %txid = $TransactionTypeID[%name];

   if (%txid $= "")
   {
   	error("Unknown txn " @ %name);
   	return;
   }

	%token = MasterNotifyTransaction(%client.getUserAccID(),%itemid,%txid,%cc,%pp,%internal_id);
   $txnWait[%token] = 0;

	echo("token=" @ %token);
}

   //handler->paramQueryBalance = false;
   //handler->paramUID = strtoul(argv[1], NULL, 0);
   //handler->paramObjectType = strtoul(argv[2], NULL, 0);
   //handler->paramActionID = strtoul(argv[3], NULL, 0);
   //handler->paramCCDiff = strtol(argv[4], NULL, 0);
   //handler->paramPPDiff = strtol(argv[5], NULL, 0);
   //handler->paramServerID = strtoul(argv[6], NULL, 0);

function GameConnection::onTransactionCompleted(%connection, %ticketid, %txid)
{
   echo("Server ack transaction " @ %ticketid SPC %txid);
   $txnWait[%ticketid] = 0;
   MasterQueryBalance(%accID);
   commandToClient(%connection, 'OnClientActionMessage', "Completed transaction " @ %txid);
}

// TODO: MasterReplayTransaction
function GameConnection::replayTransaction(%client, %name, %cc, %pp)
{

}

RegisterTransactionType(NOP, 0);

function Transaction_NOP(%connection, %item_id, %cc, %pp, %internal_id)
{

}

RegisterTransactionType(CommitDeltaBalance, 1);
// %client.performTransaction(CommitDeltaBalance, 0, %client.ccDelta, %client.ppDelta, 0);
function Transaction_CommitDeltaBalance(%client, %item_id, %cc, %pp, %internal_id)
{
   %client.cc += %cc;
   %client.pp += %pp;
   %client.ccDelta = 0;
   %client.ppDelta = 0;
}

RegisterTransactionType(AdminBalanceChange, 2);

function Transaction_AdminBalanceChange(%client, %item_id, %cc, %pp, %internal_id)
{
   %client.cc += %cc + %client.ccDelta;
   %client.pp += %pp + %client.ppDelta;
   %client.ccDelta = 0;
   %client.ppDelta = 0;
   %client.updatePoints();
}

RegisterTransactionType(GivePet, 3);

function Transaction_GivePet(%client, %item_id, %cc, %pp, %internal_id)
{
   echo("TODO: add pet " @ %item_id);
	//%connection.addItem(%type, %id);
}

