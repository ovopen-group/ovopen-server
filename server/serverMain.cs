/*
ovopen-server
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

$Relight::mod = 1;
$Relight::modIdx = 0;
$Relight::force = 0;
$MasterServer::address = "onverseworld.onverse.local";
$MasterServer::port = 65355;
$Player::name = "";
$Player::password = "Onverse123";
$debug::enabled = 0;
$debug::port = 6060;
$debug::password = "password";
$prefFile = "prefs";

$journalActive = 0;
setLogMode(5);

package OnverseLocationServer
{
    function processArguments()
    {
        for (%i=1; %i < $Game::argc; %i++)
        {
            %arg = $Game::argv[%i];
            %nextArg = $Game::argv[%i + 1];
            %hasNextArg = ($Game::argc - %i) > 1;
            if (%arg $= "-config")
            {
                if (%hasNextArg)
                {
                    $prefFile = %nextArg;
                }
                else
                {
                    error("Error: Missing Command Line argument. Usage: -config <pref name>");
                }
            }
            else if (%arg $= "-port")
            {
                if (%hasNextArg)
                {
                    $forceNetPort = %nextArg;
                    %i++;
                }
                else
                {
                    error("Error: Missing Command Line argument. Usage: -port <port number>");
                }
            }
            else if (%arg $= "-id")
            {
                if (%hasNextArg)
                {
                    $regID = %nextArg;
                    echo("Will use id " @ $regID);
                    %i++;
                }
                else
                {
                    error("Error: Missing Command Line argument. Usage: -id <token id>");
                }
            }
            else if (%arg $= "-user")
            {
                if (%hasNextArg)
                {
                    $regUsername = %nextArg;
                    %i++;
                }
                else
                {
                    error("Error: Missing Command Line argument. Usage: -user <uid>");
                }
            }
            else if (%arg $= "-regip")
            {
                if (%hasNextArg)
                {
                    $regAddress = %nextArg;
                    %i++;
                }
                else
                {
                    error("Error: Missing Command Line argument. Usage: -regip <uid>");
                }
            }
            else if (%arg $= "-regport")
            {
                if (%hasNextArg)
                {
                    $regPort = %nextArg;
                    %i++;
                }
                else
                {
                    error("Error: Missing Command Line argument. Usage: -regport <port>");
                }
            }
            else if (%arg $= "-pass")
            {
                if (%hasNextArg)
                {
                    $regPassword = %nextArg;
                    %i++;
                }
                else
                {
                    error("Error: Missing Command Line argument. Usage: -pass <pass>");
                }
            }
            else if (%arg $= "-jSave")
            {
                if (%hasNextArg)
                {
                    echo("Saving event log to journal: " @ %nextArg);
                    saveJournal(%nextArg @ ".jf");
                    %i++;
                    $journal::Active = 1;
                    $journal::Writing = 1;
                }
                else
                {
                    error("Error: Missing Command Line argument. Usage: -jSave <journal_name>");
                }
            }
            else
            {
                if (%arg $= "-jPlay")
                {
                    if (%hasNextArg)
                    {
                        playJournal(%nextArg @ ".jf", 0);
                        $journal::Active = 1;
                        $journal::File = %nextArg;
                        $journal::Reading = 1;
                    }
                    else
                    {
                        error("Error: Missing Command Line argument. Usage: -jPlay <journal_name>");
                    }
                }
                else
                {
                    if (%arg $= "-jDebug")
                    {
                        if (%hasNextArg)
                        {
                            playJournal(%nextArg @ ".jf", 1);
                            $journal::File = %nextArg;
                            $journal::Active = 1;
                            $journal::Reading = 1;
                        }
                        else
                        {
                            error("Error: Missing Command Line argument. Usage: -jDebug <journal_name>");
                        }
                    }
                    else
                    {
                        if (%arg $= "-WriteDoc")
                        {
                            setLogMode(1);
                            dumpConsoleClasses();
                            dumpConsoleFunctions();
                            return 0;
                        }
                    }
                }
            }
        }
        return 1;
    }

    function LoadCommonDatablocks()
    {
        exec("onverse/common/avatarAnimation.cs");
        exec("onverse/common/audio.cs");
    }

    function savePreferences()
    {
        echo("Exporting server preferences");
        export("$pref::*", "onverse/server/prefs.cs", False);
    }

    function loadPreferences()
    {
        %filename = "onverse/server/cfg/" @ $prefFile @ ".cs";
        if (!isFile(%filename))
        {
            error("Preferences file " @ %filename @ "doesn't exist, use location_tool to manage.");
            quit();
        }
        else
        {
            exec(%filename);
        }
    }

    function includeFiles()
    {
        echo("Including clientMain Files...");
        exec("onverse/common/TScriptArray.cs");
        exec("onverse/common/categoryID.cs");
        exec("onverse/common/onverseServer.cs");
        exec("onverse/common/materialMaps.cs");
        exec("onverse/common/skins.cs");
        exec("onverse/common/definitions.cs");

        LoadCommonDatablocks();

        // Need this BEFORE datablocks
        exec("onverse/server/scripts/scriptObjects.cs");

        // Following things define datablocks and must be in order
        exec("onverse/server/scripts/environment.cs");
        exec("onverse/server/scripts/player.cs");
        exec("onverse/server/scripts/playerData.cs");
        exec("onverse/server/scripts/pet.cs");
        exec("onverse/server/scripts/vehicles.cs");

        exec("onverse/server/datablocks/clothing.cs");
        exec("onverse/server/datablocks/particle_objects.cs");
        exec("onverse/server/datablocks/light_objects.cs");

        if ($Server::Builder)
            exec("onverse/server/datablocks/interactiveObjects.builder.cs");
        else
            exec("onverse/server/datablocks/interactiveObjects.cs");

        exec("onverse/server/scripts/homePoint.cs");
        exec("onverse/server/scripts/homeArea.cs");
        exec("onverse/server/scripts/triggers.cs");
        exec("onverse/server/scripts/mounting.cs");
        exec("onverse/server/datablocks/tool_objects.cs");
        exec("onverse/server/datablocks/pet_objects.cs");
        exec("onverse/server/datablocks/travelmount_objects.cs");
        exec("onverse/server/datablocks/npc_objects.cs");
        exec("onverse/server/datablocks/inventory_datablock_map.cs");

        // Other stuff
        exec("onverse/server/scripts/audio.cs");
        exec("onverse/server/scripts/camera.cs");
        exec("onverse/server/scripts/clientConnection.cs");
        exec("onverse/server/scripts/clothing.cs");
        exec("onverse/server/scripts/commands.cs");
        exec("onverse/server/scripts/game.cs");
        exec("onverse/server/scripts/inventory.cs");
        exec("onverse/server/scripts/interactive_objects.cs");
        exec("onverse/server/scripts/item.cs");
        exec("onverse/server/scripts/kickban.cs");
        exec("onverse/server/scripts/message.cs");
        exec("onverse/server/scripts/missionDownload.cs");
        exec("onverse/server/scripts/missionLoad.cs");
        exec("onverse/server/scripts/server.cs");
        exec("onverse/server/scripts/staticShape.cs");
        exec("onverse/server/scripts/builder.cs");
        exec("onverse/server/scripts/storeAreas.cs");
        exec("onverse/server/datablocks/builder_templates.cs");
        exec("onverse/server/datablocks/builder_templates_custom.cs");
        exec("onverse/server/scripts/master.cs");
        exec("onverse/server/scripts/transactions.cs");

        setupToolTargetBuilderGroup();
        setupInteractiveObjectDatablockProperties();

        // Temp for spawnsphere
        datablock MissionMarkerData(SpawnSphereMarker)
        {
           category = "Misc";
           shapeFile = "~/data/live_assets/shapes/dynamic/help.dts";
        };
    }

    function onStart()
    {
    	setLogMode(5);
    	enableWinConsole(1);
    	headlessDisplay();
    	dbgSetParameters($debug::port, $debug::password, $debug::enabled);
        initASIO();

        echo("\n--------- Initializing Onverse Location Server ---------");
        echo("Version: " @ getVersionString() @ "\n");

        if (processArguments() == 0)
        {
            quit();
        }

        exec("./defaultPrefs.cs");
        setDefaults();
        %defaultsNum = $pref::defaultsNum;
        $pref::defaultsNum = "";
        loadPreferences();

        // Set builder mode based on prefs
        $Server::Builder = $Pref::Server::IsBuilder;
        echo("SERVER BUILDER MODE = " @ $Server::Builder);

        if (%defaultsNum != $pref::defaultsNum)
        {
            setDefaults();
        }
        $pref::backgroundSleepTime = "0";
        $pref::timeManagerProcessInterval = "10";

        includeFiles();
        setRandomSeed();
        $loadingDone = 1;

        createServer();
        initMasterConnection();
    }

    function onExit()
    {
        saveModulePreferences();
        savePreferences();
        OpenALShutdown();
    }
};

activatePackage(OnverseLocationServer);

onStart();


