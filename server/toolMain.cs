/*
ovopen-server
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
// Tool only
function OnToolServerCompile()
{

}

$journalActive = 0;
$gToolPort = 61234;
setLogMode(5);

package OnverseToolServer
{
    function processArguments()
    {
        for (%i=1; %i < $Game::argc; %i++)
        {
            %arg = $Game::argv[%i];
            %nextArg = $Game::argv[%i + 1];
            %hasNextArg = ($Game::argc - %i) > 1;
            if (%arg $= "-port")
            {
                if (%hasNextArg)
                {
                    $gToolPort = %nextArg;
                    %i++;
                }
                else
                {
                    error("Error: Missing Command Line argument. Usage: -port <port number>");
                }
            }
        }
        return 1;
    }

    function LoadCommonDatablocks()
    {
    }

    function savePreferences()
    {
    }

    function loadPreferences()
    {
    }

    function includeFiles()
    {
    }

    function onStart()
    {
    	setLogMode(5);
    	enableWinConsole(1);
    	headlessDisplay();
    	dbgSetParameters($debug::port, $debug::password, $debug::enabled);
        initASIO();

        echo("\n--------- Initializing Onverse Tool Server ---------");
        echo("Version: " @ getVersionString() @ "\n");

        if (processArguments() == 0)
        {
            quit();
        }

        StartToolServer($gToolPort);
    }

    function onExit()
    {
    }
};

function OnToolServerCompile(%scriptName)
{
	echo("OnToolServerCompile " @ %scriptName);

	%built = ToolServerWriteHeadScript("HMM");

	// Update
	ToolServerSendResponse(%built ? 1 : 0);
}

function OnMasterServerDisconnect()
{

}

activatePackage(OnverseToolServer);

onStart();


